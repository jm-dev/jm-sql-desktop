/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.jmsql.options.pages.events;

import java.util.EventListener;

/**
 *
 * @author Michael L.R. Marques
 */
public interface OptionChangeListener extends EventListener {

    void optionChanged(OptionChangeEvent e);

}
