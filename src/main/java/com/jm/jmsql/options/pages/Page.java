/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.jmsql.options.pages;

import com.jm.jmsql.options.pages.events.OptionChangeEvent;
import com.jm.jmsql.options.pages.events.OptionChangeListener;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

/**
 *
 * @author Michael L.R. Marques
 */
public abstract class Page extends JPanel {

    /**
     * Creates new form Page
     */
    public Page() {
        initComponents();
    }

    /**
     *
     */
    public abstract void load();

    /**
     * 
     */
    public abstract void save();

    /**
     * 
     */
    public abstract void loadDefaults();

    /**
     *
     * @return
     */
    public abstract ImageIcon getIcon();

    /**
     * 
     * @return
     */
    public abstract String getDescription();

    /**
     * 
     * @param listener
     */
    public void addOptionChangedListener(OptionChangeListener listener) {
        this.listenerList.add(OptionChangeListener.class, listener);
    }

    /**
     * 
     * @param listener
     */
    public void removeOptionChangedListener(OptionChangeListener listener) {
        this.listenerList.remove(OptionChangeListener.class, listener);
    }

    /**
     *
     * @param <T>
     * @param type
     * @param source
     * @param option
     * @param oldValue
     * @param newValue
     * @return
     */
    public <T> boolean fireOptionChanged(Class<T> type, Object source, String option, T oldValue, T newValue) {
        return fireOptionChanged(new OptionChangeEvent<>(source, option, oldValue, newValue));
    }

    /**
     *
     * @param <T>
     * @param type
     * @param source
     * @param option
     * @param oldValue
     * @param newValue
     * @param restartRequired
     * @return
     */
    public <T> boolean fireOptionChanged(Class<T> type, Object source, String option, T oldValue, T newValue, boolean restartRequired) {
        return fireOptionChanged(new OptionChangeEvent<>(source, option, oldValue, newValue, restartRequired));
    }

    /**
     *
     * @param e
     * @return
     */
    public boolean fireOptionChanged(OptionChangeEvent<?> e) {
        if (e.getOldValue().equals(e.getNewValue())) {
            return false;
        }
        for (OptionChangeListener listener : this.listenerList.getListeners(OptionChangeListener.class)) {
            listener.optionChanged(e);
        }
        return true;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setOpaque(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
