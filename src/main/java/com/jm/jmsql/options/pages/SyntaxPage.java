/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.jmsql.options.pages;

import com.jm.jmsql.utils.Settings;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.ImageIcon;

/**
 *
 * @author Michael L.R. Marques
 */
public class SyntaxPage extends Page {

    private boolean oldPredicting;

    /**
     * Creates new form SyntaxPage
     */
    public SyntaxPage() {
        initComponents();
    }

    /**
     *
     */
    @Override
    public void load() {
        this.chbPredictiveText.setSelected(Settings.isPredicting());
        this.chbPredictiveText.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (fireOptionChanged(Boolean.class, e.getSource(), "predict", oldPredicting, chbPredictiveText.isSelected())) {
                    oldPredicting = chbPredictiveText.isSelected();
                }
            }
        });
        this.oldPredicting = this.chbPredictiveText.isSelected();
    }

    /**
     *
     */
    @Override
    public void save() {
        Settings.setPredicting(this.chbPredictiveText.isSelected());
    }

    /**
     *
     */
    @Override
    public void loadDefaults() {
        this.chbPredictiveText.setSelected(Settings.isPredictingDefault());
    }

    /**
     *
     * @return
     */
    @Override
    public ImageIcon getIcon() {
        return new ImageIcon(getClass().getResource("/options_syntax.png"));
    }

    /**
     * 
     * @return
     */
    @Override
    public String getDescription() {
        return "Syntax Settings";
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        chbPredictiveText = new javax.swing.JCheckBox();

        chbPredictiveText.setText("Predictive Text");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(chbPredictiveText)
                .addContainerGap(297, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(chbPredictiveText)
                .addContainerGap(270, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox chbPredictiveText;
    // End of variables declaration//GEN-END:variables

}
