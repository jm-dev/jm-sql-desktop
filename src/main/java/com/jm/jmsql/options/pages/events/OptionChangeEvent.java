/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.jmsql.options.pages.events;

import java.awt.Event;

/**
 *
 * @author Michael L.R. Marques
 */
public class OptionChangeEvent<T> extends Event {

    private final String option;
    private final T newValue;
    private final T oldValue;
    private final boolean restartRequired;

    /**
     *
     * @param source
     * @param option
     * @param newValue
     * @param oldValue
     */
    public OptionChangeEvent(Object source, String option, T newValue, T oldValue) {
        this(source, option, newValue, oldValue, false);
    }

    /**
     *
     * @param source
     * @param option
     * @param newValue
     * @param oldValue
     * @param restartRequired
     */
    public OptionChangeEvent(Object source, String option, T newValue, T oldValue, boolean restartRequired) {
        super(source, 0, null);
        this.option = option;
        this.newValue = newValue;
        this.oldValue = oldValue;
        this.restartRequired = restartRequired;
    }

    /**
     * @return the option
     */
    public String getOption() {
        return option;
    }

    /**
     * @return the newValue
     */
    public T getNewValue() {
        return newValue;
    }

    /**
     * @return the oldValue
     */
    public T getOldValue() {
        return oldValue;
    }

    /**
     * @return the restartRequired
     */
    public boolean isRestartRequired() {
        return restartRequired;
    }
    
}
