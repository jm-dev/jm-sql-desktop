/*
 * Copyright (C) 2012-2013 Michael L.R. Marques
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Contact: michaellrmarques@gmail.com
 */

package com.jm.jmsql.options;

import com.jm.commons.components.JMDialog;
import com.jm.commons.settings.SettingsException;
import com.jm.jmsql.options.pages.GeneralPage;
import com.jm.jmsql.options.pages.Page;
import com.jm.jmsql.options.pages.ResultSetPage;
import com.jm.jmsql.options.pages.SyntaxPage;
import com.jm.jmsql.options.pages.events.OptionChangeEvent;
import com.jm.jmsql.options.pages.events.OptionChangeListener;
import com.jm.jmsql.utils.Settings;
import java.awt.Frame;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Michael L.R. Marques
 */
public class Options extends JMDialog implements OptionChangeListener {
    
    private static final Logger logger = LogManager.getLogger(Options.class);
    
    private Page[] pages;
    private boolean changes;
    private boolean restart;

    /**
     * Creates new form Options
     * @param parent
     */
    public Options(Frame parent) {
        super(parent);
        initComponents();
        setLocationRelativeTo(parent);
        this.mainPanel.setImage(new File("./images/optionslogo.png"));
        // Set the icon image
        try {
            setIconImage(ImageIO.read(getClass().getResource("/options.png")));
        } catch (IOException ioe) {
            logger.error(ioe.getMessage(), ioe);
        }
        // 
//        addComponentListener(new java.awt.event.ComponentAdapter() {
//            @Override
//            public void componentResized(ComponentEvent event) {
//                // Force the width
//                if (event.getComponent().getWidth() >= event.getComponent().getMaximumSize().width) {
//                    Dimension d = new Dimension(event.getComponent().getMaximumSize().width, event.getComponent().getHeight());
//                    event.getComponent().setPreferredSize(d);
//                    event.getComponent().setSize(d);
//                }
//                // Force the height
//                if (event.getComponent().getHeight() >= event.getComponent().getMaximumSize().height) {
//                    Dimension d = new Dimension(event.getComponent().getWidth(), event.getComponent().getMaximumSize().height);
//                    event.getComponent().setPreferredSize(d);
//                    event.getComponent().setSize(d);
//                }
//            }
//        });
        // Load Pges
        load(new GeneralPage(),
                new SyntaxPage(),
                    new ResultSetPage());
    }
    
    /**
     * 
     * @param changes 
     */
    private void setChanged(boolean changes) {
        this.changes = changes;
        this.btnApply.setEnabled(changes);
    }
    
    /**
     * 
     * @param restart 
     */
    private void setRestart(boolean restart) {
        this.restart = restart;
    }

    /**
     * 
     * @param e
     */
    @Override
    public void optionChanged(OptionChangeEvent e) {
        setChanged(true);
        if (!this.restart &&
                e.isRestartRequired()) {
            setRestart(true);
        }
        logger.debug("Option changed: " + e.getOption() + " Old Value: " + e.getOldValue() + " New Value: " + e.getNewValue());
    }
    
    /**
     * 
     */
    private void load(Page... pages) {
        for (final Page page : (this.pages = pages)) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    page.load();
                }
            }).start();
            this.tabSettings.addTab(null, page.getIcon(), page, page.getDescription());
            page.addOptionChangedListener(this);
        }
    }
    
    /**
     *
     */
    private void save() {
        if (this.changes) {
            if (this.restart) {
                JOptionPane.showMessageDialog(this, "A restart is required for some changes to take effect.", "Restart Required", JOptionPane.INFORMATION_MESSAGE);
            }
            for (Page page : this.pages) {
                page.save();
            }
            
            try {
                // Save settings changes
                Settings.save();
            } catch (SettingsException se) {
                logger.error(se.getMessage(), se);
            }
            // Finalize changes
            setChanged(false);
            setRestart(false);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainPanel = new com.jm.commons.components.panel.ImagePanel();
        tabSettings = new javax.swing.JTabbedPane();
        btnApply = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        btnOk = new javax.swing.JButton();
        btnDefault = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Options...");
        setMinimumSize(new java.awt.Dimension(500, 350));
        setModal(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closing(evt);
            }
        });

        mainPanel.setImage(null);
        mainPanel.setMaximumSize(new java.awt.Dimension(800, 600));
        mainPanel.setMinimumSize(new java.awt.Dimension(550, 400));

        btnApply.setText("Apply");
        btnApply.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnApplyActionPerformed(evt);
            }
        });

        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        btnOk.setText("Ok");
        btnOk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOkActionPerformed(evt);
            }
        });

        btnDefault.setText("Default");
        btnDefault.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDefaultActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(mainPanelLayout.createSequentialGroup()
                        .addComponent(btnDefault)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 256, Short.MAX_VALUE)
                        .addComponent(btnOk)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCancel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnApply))
                    .addComponent(tabSettings))
                .addContainerGap())
        );

        mainPanelLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnApply, btnCancel, btnOk});

        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addGap(89, 89, 89)
                .addComponent(tabSettings, javax.swing.GroupLayout.DEFAULT_SIZE, 271, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnApply)
                    .addComponent(btnCancel)
                    .addComponent(btnOk)
                    .addComponent(btnDefault))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnOkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOkActionPerformed
        save();
        setVisible(false);
        dispose();
    }//GEN-LAST:event_btnOkActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        setVisible(false);
        dispose();
    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnApplyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnApplyActionPerformed
        save();
    }//GEN-LAST:event_btnApplyActionPerformed

    private void closing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closing
        setVisible(false);
        dispose();
    }//GEN-LAST:event_closing

    private void btnDefaultActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDefaultActionPerformed
        for (Page page : this.pages) {
            if (page.isShowing()) {
                page.loadDefaults();
            }
        }
    }//GEN-LAST:event_btnDefaultActionPerformed
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnApply;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnDefault;
    private javax.swing.JButton btnOk;
    private com.jm.commons.components.panel.ImagePanel mainPanel;
    private javax.swing.JTabbedPane tabSettings;
    // End of variables declaration//GEN-END:variables
    
}
