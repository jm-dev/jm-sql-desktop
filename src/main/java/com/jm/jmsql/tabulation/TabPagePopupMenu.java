/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jm.jmsql.tabulation;

import com.jm.jmsql.JMSqlApp;
import com.jm.jmsql.sql.SQLEditor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;

/**
 *
 * @created Dec 18, 2012
 * @author Michael
 */
public class TabPagePopupMenu extends JPopupMenu {
    
    /**
     * 
     * @param app
     * @param tabPane
     * @param component
     * @param x
     * @param y 
     */
    public static void show(final JMSqlApp app, final JTabbedPane tabPane, JComponent component, int x, int y) {
        TabPagePopupMenu popup = new TabPagePopupMenu();
        //
        JMenuItem close = new JMenuItem("Close");
        close.addActionListener(new ActionListener() {
            @Override public void actionPerformed(ActionEvent e) {
                tabPane.removeTabAt(tabPane.getSelectedIndex());
            }
        });
        // 
        popup.add(close);
        //
        if (tabPane.getTabCount() > 1) {
            JMenuItem closeOther = new JMenuItem("Close Other");
            closeOther.addActionListener(new ActionListener() {
                @Override public void actionPerformed(ActionEvent e) {
                    while (tabPane.getTabCount() > 1) {
                        tabPane.removeTabAt(tabPane.getSelectedIndex() == 0 ? 1 : 0);
                    }
                }
            });
            popup.add(closeOther);
            //
            JMenuItem closeAll = new JMenuItem("Close All");
            closeAll.addActionListener(new ActionListener() {
                @Override public void actionPerformed(ActionEvent e) {
                    tabPane.removeAll();
                }
            });
            popup.add(closeAll);
        }
        //
        if (tabPane.getSelectedComponent() instanceof SQLEditor) {
            //
            if (popup.getComponentCount() > 0) {
                popup.add(new JSeparator());
            }
            //
            JMenuItem save = new JMenuItem("Save");
            save.addActionListener(new ActionListener() {
                @Override public void actionPerformed(ActionEvent e) {
                    app.saveSQL();
                }
            });
            //
            JMenuItem saveAs = new JMenuItem("Save As");
            saveAs.addActionListener(new ActionListener() {
                @Override public void actionPerformed(ActionEvent e) {
                    app.saveAsSQL();
                }
            });
            //
            JMenuItem saveAll = new JMenuItem("Save All");
            saveAll.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    app.saveAllSQL();
                }
            });
            //
            JMenuItem clonePage = new JMenuItem("Clone Page");
            clonePage.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        SQLEditor editor = app.getSelectedEditor().clone();
                        tabPane.add(tabPane.getTitleAt(tabPane.getSelectedIndex()), editor);
                        tabPane.setToolTipTextAt(tabPane.getSelectedIndex()+1, tabPane.getToolTipTextAt(tabPane.getSelectedIndex()));
                        app.setSelectedEditor(editor);
                    } catch (CloneNotSupportedException cnse) {
                        System.out.println(cnse.getMessage());
                    }
                }
            });
            // 
            popup.add(save);
            popup.add(saveAs);
            popup.add(saveAll);
            popup.add(new JSeparator());
            popup.add(clonePage);
        }
        //
        if (popup.getComponentCount() > 0) {
            popup.show(component, x, y);
        }
    }

}
