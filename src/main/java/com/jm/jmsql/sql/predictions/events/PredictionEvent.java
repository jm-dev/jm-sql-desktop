/*
 * Copyright (C) 2012-2013 Michael L.R. Marques
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Contact: michaellrmarques@gmail.com
 */
package com.jm.jmsql.sql.predictions.events;

import com.jm.jmsql.xplora.Item;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;

/**
 *
 * @author Michael L.R. Marques
 */
public class PredictionEvent extends EventObject {
    
    private final List<Item> predictionList;
    private final int index;
    private final boolean predictionSelected;
    private final String current;
    
    /**
     * 
     * @param source
     */
    public PredictionEvent(Object source) {
        this(source, new ArrayList());
    }
    
    /**
     * 
     * @param source
     * @param predictionList 
     */
    public PredictionEvent(Object source, List<Item> predictionList) {
        this(source, predictionList, -1);
    }

    /**
     * 
     * @param source
     * @param predictionList
     * @param index
     */
    public PredictionEvent(Object source, List<Item> predictionList, int index) {
        this(source, predictionList, index, null);
    }
    
    /**
     * 
     * @param source
     * @param predictionList
     * @param index 
     * @param current
     */
    public PredictionEvent(Object source, List<Item> predictionList, int index, String current) {
        super(source);
        this.predictionList = predictionList;
        this.index = index;
        this.predictionSelected = predictionList != null && index >= 0;
        this.current = current;
    }
    
    /**
     * 
     * @return 
     */
    public Item getPrediction() {
        if (!this.predictionSelected) {
            return null;
        }
        return this.predictionList.get(index);
    }
    
    /**
     * 
     * @return 
     */
    public List<Item> getPredictionList() {
        return this.predictionList;
    }
    
    /**
     * 
     * @return 
     */
    public int getPredictionIndex() {
        if (!this.predictionSelected) {
            return -1;
        }
        return this.index;
    }
    
    /**
     * 
     * @return 
     */
    public boolean isPredictionListEmpty() {
        return this.predictionList == null ||
                this.predictionList.isEmpty();
    }
    
    /**
     * 
     * @return 
     */
    public boolean isPredictionSelected() {
        return this.predictionSelected;
    }

    /**
     * @return the current
     */
    public String getCurrent() {
        return current;
    }
    
}
