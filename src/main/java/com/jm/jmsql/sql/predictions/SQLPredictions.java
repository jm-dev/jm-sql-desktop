/*
 * Copyright (C) 2013 Michael
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.jm.jmsql.sql.predictions;

import com.jm.commons.components.JMDialog;
import com.jm.jmsql.models.SQLPredictionsListModel;
import com.jm.jmsql.xplora.Column;
import com.jm.jmsql.xplora.Definition;
import com.jm.jmsql.xplora.Item;
import com.jm.jmsql.xplora.Library;
import com.jm.jmsql.xplora.Schema;
import com.jm.jmsql.xplora.Table;
import com.jm.jmsql.sql.predictions.events.PredictionEvent;
import com.jm.jmsql.sql.predictions.events.PredictionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.EventListener;
import java.util.List;
import java.util.regex.Pattern;
import javax.swing.event.EventListenerList;
import javax.swing.text.JTextComponent;

/**
 * 
 * @author Michael L.R. Marques
 */
public class SQLPredictions extends JMDialog  {

    protected static final Pattern SQL_TABLE_PATTERN = Pattern.compile("(FROM|JOIN)", Pattern.CASE_INSENSITIVE);
    protected static final Pattern SQL_COLUMN_PATTERN = Pattern.compile("(SELECT|WHERE|AND|OR|BY|INTO)", Pattern.CASE_INSENSITIVE);
    
    private final EventListenerList listeners;
    private String current;
    
    /**
     * Creates new form SQLPredictions
     */
    public SQLPredictions() {
        initComponents();
        
        this.listeners = new EventListenerList();
        
        this.lstPredictions.setCellRenderer(new SQLPredictionsListModel.SQLPredictionsCellRenderer());
        this.lstPredictions.setModel(new SQLPredictionsListModel());
        this.lstPredictions.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if (KeyEvent.VK_ENTER == e.getKeyCode()) {
                    firePredictionSelected();
                } else if (KeyEvent.VK_ESCAPE == e.getKeyCode()) {
                    setVisible(false);
                }
            }
        });
        this.lstPredictions.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (MouseEvent.BUTTON1 == e.getButton() &&
                        e.getClickCount() == 2) {
                    firePredictionSelected();
                }
            }
        });
        if (this.lstPredictions.getSelectedIndex() >= 0) {
            this.lstPredictions.setSelectedIndex(0);
            this.lstPredictions.ensureIndexIsVisible(0);
        }
    }
    
    /**
     * 
     * @param textComponent
     * @param definition 
     * @param library
     */
    public void predict(final JTextComponent textComponent, final Definition definition, final Library library) {
        Thread predicitonThread = new Thread(new Runnable() {
            @Override
            public void run() {
                firePredicting();
                current = "";
                for (int i = textComponent.getCaretPosition(); i >= 0; i--) {
                    char character = textComponent.getText().charAt(i);
                    if (Character.isWhitespace(character)) {
                        break;
                    }
                    current = character + current;
                }

                if (SQL_TABLE_PATTERN.matcher(current).find() ||
                        SQL_COLUMN_PATTERN.matcher(current).find()) {
                    current = "";
                }

                String pattern = "[" + "." + (definition.getParent().getLibraryDelimeter() != null ? ("|" + definition.getParent().getLibraryDelimeter()) : "") + "]";

                List<Item> predictions = buildPredictionList(current, pattern, textComponent.getText(), textComponent.getCaretPosition(), definition, library);
                lstPredictions.setModel(new SQLPredictionsListModel(predictions));

                if (predictions.size() > 0) {
                    for (Item item : predictions) {
                        if (item.getName().equals(current)) {
                            lstPredictions.setSelectedValue(item, true);
                            firePredicted();
                            firePredictionSelected();
                            return;
                        } else if (item.getName().startsWith(current)) {
                            lstPredictions.setSelectedValue(item, true);
                        }
                    }
                }

                firePredicted();
                pack();
            }
        });
        predicitonThread.start();
    }
    
    /**
     * 
     * @param current
     * @param pattern
     * @param caretPosition
     * @param sql
     * @param definition
     * @param library
     * @return 
     */
    protected static List<Item> buildPredictionList(String current, String pattern, String sql, int caretPosition, Definition definition, Library library) {
        String[] values = new String[0];
        if (current != null) {
            values = current.split(pattern);
        }
        
        Class<? extends Item> required = getRequired(sql, caretPosition);
        Library lib = library == null || definition.isLibrary() ? definition : library;

        List<Item> predictions = new ArrayList();
        switch (values.length) {
            case 1: {
                List<? extends Item> subList = lib.subList(required);
                if (!values[0].isEmpty()) {
                    for (Item item : subList) {
                        if (item.getName().toLowerCase().startsWith(values[0].toLowerCase())) {
                            predictions.add(item);
                        }
                    }
                } else {
                    predictions.addAll(subList);
                }
                break;
            } default: {
                predictions.addAll(lib.subList(required));
                break;
            }
        }
        return predictions;
    }

    /**
     *
     * @param sql
     * @param caretPosition
     * @return
     */
    protected static Class<? extends Item> getRequired(String sql, int caretPosition) {
        for (int i = caretPosition - 1; i >= 0; i--) {
            String text = sql.substring(i, caretPosition);
            if (SQL_TABLE_PATTERN.matcher(text).find()) {
                return Table.class;
            } else if (SQL_COLUMN_PATTERN.matcher(text).find()) {
                return Column.class;
            }
        }
        return Schema.class;
    }
    
    /**
     * Registers PredictionListener
     * @param listener 
     */
    public void addPredictionListener(PredictionListener listener) {
        this.listeners.add(PredictionListener.class, listener);
    }
    
    /**
     * De-registers PredictionListener
     * @param listener 
     */
    public void removePredictionListener(PredictionListener listener) {
        this.listeners.remove(PredictionListener.class, listener);
    }

    /**
     * 
     * @return
     */
    public PredictionListener[] getListeners() {
        return this.listeners.getListeners(PredictionListener.class);
    }
    
    /**
     * 
     */
    public void firePredicting() {
        for (EventListener listener : getListeners()) {
            ((PredictionListener) listener).predicting(new PredictionEvent(this));
        }
    }
    
    /**
     * 
     */
    public void firePredicted() {
        for (EventListener listener : getListeners()) {
            ((PredictionListener) listener).predicted(new PredictionEvent(this, ((SQLPredictionsListModel) this.lstPredictions.getModel()).getList()));
        }
    }
    
    /**
     * 
     */
    public void firePredictionSelected() {
        for (EventListener listener : getListeners()) {
            ((PredictionListener) listener).predictionSelected(new PredictionEvent(this, ((SQLPredictionsListModel) this.lstPredictions.getModel()).getList(), this.lstPredictions.getSelectedIndex(), this.current));
        }
    }

    /**
     *
     */
    @Override
    public void toFront() {
        super.toFront();
        if (this.lstPredictions.getModel().getSize() > 0) {
            this.lstPredictions.setSelectedIndex(0);
            this.lstPredictions.ensureIndexIsVisible(0);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        lstPredictions = new javax.swing.JList();

        setAlwaysOnTop(true);
        setFocusTraversalPolicyProvider(true);
        setUndecorated(true);
        setResizable(false);
        addWindowFocusListener(new java.awt.event.WindowFocusListener() {
            public void windowGainedFocus(java.awt.event.WindowEvent evt) {
            }
            public void windowLostFocus(java.awt.event.WindowEvent evt) {
                loseFocus(evt);
            }
        });

        jScrollPane1.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane1.setRequestFocusEnabled(false);

        lstPredictions.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        lstPredictions.setToolTipText("");
        jScrollPane1.setViewportView(lstPredictions);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 345, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 216, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * 
     * @param evt
     */
    private void loseFocus(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_loseFocus
        //setVisible(false);
    }//GEN-LAST:event_loseFocus

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JList lstPredictions;
    // End of variables declaration//GEN-END:variables
    
}
