/*
 * Copyright (C) 2013 Michael
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.jm.jmsql.sql;

import com.jm.commons.fio.FileSystem;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.filechooser.FileFilter;

/**
 *
 * @created Dec 5, 2012
 * @author Michael L.R. Marques
 */
public class SQLFileOperations {
    
    /**
     * 
     * @param file
     * @return 
     * @throws java.io.IOException
     */
    public static String get(String file) throws IOException {
        return get(new File(file));
    }
    
    /**
     * 
     * @param file
     * @return 
     * @throws java.io.IOException
     */
    public static String get(File file) throws IOException {
        StringBuilder sql = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line;
            while((line = reader.readLine()) != null) {
                sql.append(line);
                sql.append(System.getProperty("line.separator"));
            }
        }
        return sql.toString();
    }
    
    /**
     * 
     * @param file
     * @param sql 
     * @throws java.io.IOException
     */
    public static void set(String file, String sql) throws IOException {
        set(new File(file), sql);
    }
    
    /**
     * 
     * @param file
     * @param sql 
     * @throws java.io.IOException
     */
    public static void set(File file, String sql) throws IOException {
        if (!file.exists()) {
            file.createNewFile();
        }
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
            writer.write(sql);
        }
    }
    
    /**
     *
     */
    public static class SQLFileFilter extends FileFilter {
        
        /**
         * 
         */
        public static String[] EXTENSIONS = new String[] { "sql" };
        
        /**
         *
         * @return
         */
        public static FileFilter getInstance() {
            return new SQLFileFilter();
        }
        
        /**
         * 
         * @param f
         * @return 
         */
        @Override public boolean accept(File f) {
            return f.isDirectory() ||
                        FileSystem.isValidExtension(f, EXTENSIONS);
        }
        
        /**
         * 
         * @return 
         */
        @Override public String getDescription() {
            return "SQL Files (*.sql)";
        }
        
    }
    
}
