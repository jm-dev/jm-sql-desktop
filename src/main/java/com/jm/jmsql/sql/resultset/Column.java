/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.jmsql.sql.resultset;

import com.jm.jmsql.jdbc.JDBCType;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

/**
 *
 * @author Marquema
 */
public class Column {
    
    private final int index;
    private final String name;
    private final JDBCType type;
    private final int length;
    private final int decimal;
    private boolean primaryKey;
    private boolean foreignKey;
    private boolean toString;

    /**
     *
     * @param rsmd
     * @param index
     * @throws SQLException
     */
    public Column(ResultSetMetaData rsmd, int index) throws SQLException {
        this.index = index;                                                                                     // Sets the columns index
        this.name = rsmd.getColumnName(index);                                                                  // Passes the column name
        this.type = JDBCType.get(rsmd.getColumnType(index));                                                    // Converts the column type to a java type
        this.length = rsmd.getColumnDisplaySize(index);                                                         // Gets the column length
        this.decimal = rsmd.getScale(index);                                                                    // Gets the columns decimal places
    }
    
    /**
     *
     * @return
     */
    public int getIndex() {
        return this.index;
    }

    /**
     *
     * @return
     */
    public String getName() {
        return this.name;
    }

    /**
     *
     * @return
     */
    public JDBCType getType() {
        if (this.toString) {
            return JDBCType.VarChar;
        }
        return this.type;
    }

    /**
     *
     * @return
     */
    public int getLength() {
        return this.length;
    }

    /**
     *
     * @return
     */
    public int getDecimal() {
        return this.decimal;
    }
    
    /**
     * @return the primaryKey
     */
    @Deprecated
    public boolean isPrimaryKey() {
        return primaryKey;
    }
    
    /**
     * @param primaryKey the primaryKey to set
     */
    @Deprecated
    public void setPrimaryKey(boolean primaryKey) {
        this.primaryKey = primaryKey;
    }

    /**
     * @return the foreignKey
     */
    @Deprecated
    public boolean isForeignKey() {
        return foreignKey;
    }

    /**
     * @param foreignKey the foreignKey to set
     */
    @Deprecated
    public void setForeignKey(boolean foreignKey) {
        this.foreignKey = foreignKey;
    }

    /**
     *
     * @return
     */
    @Deprecated
    public boolean isToString() {
        return this.toString;
    }

    /**
     *
     * @param toString
     */
    @Deprecated
    public void setToString(boolean toString) {
        this.toString = toString;
    }
    
    /**
     * 
     * @return 
     */
    public String getProperties() {
        StringBuilder builder = new StringBuilder();
        builder.append("<html><body>");
        builder.append("Name: ");
        builder.append(this.name);
        builder.append("<br>");
        builder.append("Type: ");
        builder.append(this.type.getName());
        builder.append("<br>");
        builder.append("Length: ");
        builder.append(this.length);
        builder.append("<br>");
        builder.append("Decimals: ");
        builder.append(this.decimal);
        builder.append("<br>");
        builder.append("Index: ");
        builder.append(this.index);
        builder.append("</body></html>");
        return builder.toString();
    }

}
