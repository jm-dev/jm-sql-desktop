/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.jmsql.sql.resultset.renderers;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author Michael L.R. Marques
 */
public class BlobRenderer extends ResultSetTableCellRenderer<byte[]> {

    /**
     * 
     * @param tableCellRenderer
     * @param table
     * @param value
     * @param isSelected
     * @param hasFocus
     * @param row
     * @param column
     * @return Component
     */
    @Override
    public JLabel getTableCellRendererComponent(TableCellRenderer tableCellRenderer, JTable table, byte[] value, boolean isSelected, boolean hasFocus, int row, int column) {
        if (value != null) {
            StringBuilder blob = new StringBuilder();
            blob.append("<Blob ");
            blob.append(value.length / 1000);
            blob.append("kb");
            blob.append(">");
            setText(blob.toString());
            setHorizontalAlignment(SwingConstants.CENTER);
        }
        return this;
    }

}
