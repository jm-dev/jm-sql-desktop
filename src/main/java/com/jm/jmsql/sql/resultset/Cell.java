/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.jmsql.sql.resultset;

/**
 *
 * @author Marquema
 * @param <T extends Object>
 */
public class Cell<T extends Object> {

    private final boolean editable;
    private final Row row;
    private final Column column;
    private T value;
    private boolean changed;

    /**
     *
     * @param row
     * @param column
     * @param value
     */
    public Cell(Row row, Column column, T value) {
        this.editable = false;
        this.row = row;
        this.column = column;
        this.value = value;
    }

    /**
     *
     * @return 
     */
    public boolean isEditable() {
        return this.editable;
    }

    /**
     *
     * @return
     */
    public Row getRow() {
        return this.row;
    }

    /**
     *
     * @return
     */
    public Column getColumn() {
        return this.column;
    }

    /**
     *
     * @return
     */
    public T getValue() {
        if (this.column.getType().isFormattable() &&
                this.value != null) {
            return (T) this.column.getType().getFormat().format(this.value);
        }
        return this.value;
    }

    /**
     *
     * @param value
     */
    public void setValue(T value) {
        this.value = value;
    }

    /**
     *
     * @return
     */
    public boolean isChanged() {
        return this.changed;
    }

    /**
     *
     * @param changed
     */
    public void setChanged(boolean changed) {
        this.changed = changed;
    }

}
