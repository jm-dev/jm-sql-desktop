/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.jmsql.sql.resultset.renderers;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author Marquema
 */
public class NumberRenderer extends ResultSetTableCellRenderer<Number> {
    
    /**
     * 
     * @param tableCellRenderer
     * @param table
     * @param value
     * @param isSelected
     * @param hasFocus
     * @param row
     * @param column
     * @return 
     */
    @Override
    public JLabel getTableCellRendererComponent(TableCellRenderer tableCellRenderer, JTable table, Number value, boolean isSelected, boolean hasFocus, int row, int column) {
        return this;
    }

}
