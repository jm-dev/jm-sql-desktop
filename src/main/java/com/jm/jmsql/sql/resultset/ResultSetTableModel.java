/*
 * Copyright (C) 2013 Michael
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.jm.jmsql.sql.resultset;

import static com.jm.jmsql.sql.SQLEditor.DEFAULT_PAGE_SIZE;
import com.jm.jmsql.xplora.Definition;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.event.EventListenerList;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

/**
 *
 * @author Michael L.R. Marques
 */
public class ResultSetTableModel implements TableModel {

    private static int pageSize = DEFAULT_PAGE_SIZE;

    private final boolean readOnly;
    private final EventListenerList listeners;
    private final Definition definition;
    private final ResultSetData resultSetData;
    private int totalRows;
    private int pages;
    private int page;
    private boolean editing;
    
    /**
     * 
     * @param definition
     * @throws SQLException 
     */
    public ResultSetTableModel(Definition definition) throws SQLException {
        this(definition, 0);
    }
    
    /**
     * 
     * @param definition
     * @param limit
     * @throws SQLException 
     */
    public ResultSetTableModel(Definition definition, int limit) throws SQLException {
        // Set the result set
        this.definition = definition;
        // Initialize the event listener list
        this.listeners = new EventListenerList();
        // Create the column data
        this.resultSetData = new ResultSetData(definition.getResults());
        // Get total rows
        this.totalRows = limit > 0 ? Math.min(definition.getRowCount(), limit) : definition.getRowCount();
        // Calculate the number of pages
        this.pages = pageSize == this.totalRows ? 1 : ((this.totalRows / pageSize) + (this.totalRows % pageSize > 0 ? 1 : 0));
        // Move result set position to beginning of page and load rows
        this.resultSetData.setData(this.definition.getResults(), getPagePosition(this.page = 1), pageSize);
        this.readOnly = this.definition.getConnection().isReadOnly();
    }

    /**
     *
     * @return
     */
    public boolean isEditing() {
        return this.editing;
    }

    /**
     * 
     * @param editing
     */
    public void setEditing(boolean editing) {
        this.editing = editing;
    }
    
    /**
     * 
     * @return 
     */
    public ResultSet getResults() {
        return this.definition.getResults();
    }
    
    /**
     * 
     * @return 
     */
    public ResultSetData getData() {
        return this.resultSetData;
    }

    /**
     * 
     * @throws SQLException
     */
    public void commit() throws SQLException {
        if (!this.editing ||
                !this.resultSetData.isChanged()) {
            return;
        }
        
        ResultSet resultSet = this.definition.getResults();
        for (Row row : this.resultSetData) {
            if (row.isChanged()) {
                resultSet.absolute(row.getIndex());
                for (Cell cell : row) {
                    if (cell.isChanged()) {
                        resultSet.updateObject(cell.getColumn().getName(), cell.getValue());
                    }
                }
                resultSet.updateRow();
            }
        }
        for (Row row : this.resultSetData) {
            if (row.isChanged()) {
                for (Cell cell : row) {
                    if (cell.isChanged()) {
                        cell.setChanged(false);
                    }
                }
            }
            row.setChanged(false);
        }

        this.resultSetData.setChanged(false);
        this.editing = false;
    }
    
    /**
     * 
     * @param index 
     */
    @Deprecated
    public void setColumnToString(int index) {
        this.resultSetData.getColumn(index - 1).setToString(true);
        fireTableChanged();
    }
    
    /**
     * Set the page to go to
     * @param page 
     * @throws java.sql.SQLException 
     */
    public void setPage(int page) throws SQLException {
        // Check if the page is available
        if (page > 0 &&
                page <= this.pages) {
            // Set the page
            this.page = page;
            // Move result set position to beginning of page, using the next page and load rows
            this.resultSetData.setData(this.definition.getResults(), getPagePosition(page), pageSize);
            // Fire table changed
            fireTableChanged();
        }
    }
    
    /**
     * Set the size of the page
     * @param newPageSize
     * @throws java.sql.SQLException 
     */
    public void setPageSize(int newPageSize) throws SQLException {
        // Do not set the page size if the new value is the same as the old value
        if (pageSize != newPageSize) {
            // Calculate page size
            pageSize = newPageSize;
            // Calculate the number of pages
            this.pages = pageSize == this.totalRows ? 1 : ((this.totalRows / pageSize) + (this.totalRows % pageSize > 0 ? 1 : 0));
            // Move result set position to beginning of page and load rows
            this.resultSetData.setData(this.definition.getResults(), getPagePosition(1), pageSize);
            // Fire table changed
            fireTableChanged();
        }
    }
    
    /**
     * Reset position in result set to beginning of page and load rows
     * @throws SQLException 
     */
    public void refresh() throws SQLException {
        // Move result set position to beginning of page and load rows
        this.resultSetData.setData(this.definition.getResults(), getPagePosition(), pageSize);
        // Fire table changed
        fireTableChanged();
    }
    
    /**
     * Move to first page and load rows
     * @throws SQLException 
     */
    public void firstPage() throws SQLException {
        // Check if we are not already on the first page
        if (!isFirstPage()) {
            // Move result set position to beginning of page and load rows
            this.resultSetData.setData(this.definition.getResults(), getPagePosition(1), pageSize);
            // Fire table changed
            fireTableChanged();
        }
    }
    
    /**
     * Move to the previous page if not on first page and load rows
     * @throws SQLException 
     */
    public void previousPage() throws SQLException {
        // Check if we have a previous page
        if (hasPreviousPage()) {
            // Move result set position to beginning of page, using the previous page and load rows
            this.resultSetData.setData(this.definition.getResults(), getPagePosition(--this.page), pageSize);
            // Fire table changed
            fireTableChanged();
        }
    }
    
    /**
     * Move to the next page if not on last page and load rows
     * @throws java.sql.SQLException
     */
    public void nextPage() throws SQLException {
        // Check if we have a next page
        if (hasNextPage()) {
            // Move result set position to beginning of page, using the next page and load rows
            this.resultSetData.setData(this.definition.getResults(), getPagePosition(++this.page), pageSize);
            // Fire table changed
            fireTableChanged();
        }
    }
    
    /**
     * Move to last page and load rows
     * @throws java.sql.SQLException
     */
    public void lastPage() throws SQLException {
        // Check if we are not already on the last page
        if (!isLastPage()) {
            // Move result set position to beginning of page, using the last page and load rows
            this.resultSetData.setData(this.definition.getResults(), getPagePosition(this.pages), pageSize);
            // Fire table changed
            fireTableChanged();
        }
    }
    
    /**
     * The number of pages
     * @return 
     */
    public int getPages() {
        return this.pages;
    }
    
    /**
     * The current page
     * @return 
     */
    public int getPage() {
        return this.page;
    }
    
    /**
     * 
     * @return 
     */
    public int getPageSize() {
        return pageSize;
    }
    
    /**
     * Are we on the first page
     * @return 
     */
    public boolean isFirstPage() {
        return this.page == 1;
    }
    
    /**
     * Are we on the last page
     * @return 
     */
    public boolean isLastPage() {
        return this.page == this.pages;
    }
    
    /**
     * Is there a next page
     * @return 
     */
    public boolean hasNextPage() {
        return this.page < this.pages;
    }
    
    /**
     * Is there a previous page
     * @return 
     */
    public boolean hasPreviousPage() {
        return this.page > 1;
    }
    
    /**
     * Gets the number of records in the result set
     * @return 
     */
    public int getTotalRows() {
        return this.totalRows;
    }
    
    /**
     * Gets the number of columns in the result set
     * @return 
     */
    public int getTotalColumns() {
        return this.resultSetData.getColumns().size();
    }
    
    /**
     * 
     * @param rowIndices
     * @return 
     */
    public List<Row> getRows(int[] rowIndices) {
        return new ArrayList(this.resultSetData);
    }
    
    /**
     * The number of rows the table can view
     * @return 
     */
    @Override
    public int getRowCount() {
        return this.resultSetData.size();
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public int getColumnCount() {
        return this.resultSetData.getColumns().size() + 1;
    }
    
    /**
     * 
     * @param columnIndex
     * @return 
     */
    @Override
    public String getColumnName(int columnIndex) {
        return columnIndex == 0 ? "#" : this.resultSetData.getColumn(columnIndex - 1).getName();
    }
    
    /**
     * 
     * @param columnIndex
     * @return 
     */
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return columnIndex == 0 ? Integer.class : this.resultSetData.getColumn(columnIndex - 1).getType().getType();
    }
    
    /**
     * 
     * @param rowIndex
     * @param columnIndex
     * @return 
     */
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return !this.readOnly &&
                    this.editing;
    }
    
    /**
     * 
     * @param rowIndex
     * @param columnIndex
     * @return 
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return columnIndex == 0 ? this.resultSetData.get(rowIndex).getIndex() : this.resultSetData.getCell(rowIndex, columnIndex - 1).getValue();
    }
    
    /**
     * 
     * @param newValue
     * @param rowIndex
     * @param columnIndex 
     */
    @Override
    public void setValueAt(Object newValue, int rowIndex, int columnIndex) {
        this.resultSetData.setCell(rowIndex, columnIndex - 1, newValue);
    }
    
    /**
     * 
     * @param l 
     */
    @Override
    public void addTableModelListener(TableModelListener l) {
        this.listeners.add(TableModelListener.class, l);
    }
    
    /**
     * 
     * @param l 
     */
    @Override
    public void removeTableModelListener(TableModelListener l) {
        this.listeners.remove(TableModelListener.class, l);
    }
    
    /**
     * 
     */
    public void fireTableChanged() {
        // Loop through the listeners
        for (TableModelListener listener : this.listeners.getListeners(TableModelListener.class)) {
            // Lazily create the event:
            listener.tableChanged(new TableModelEvent(this, 0, this.resultSetData.size(), TableModelEvent.ALL_COLUMNS));
        }
    }
    
    /**
     * 
     * @return 
     */
    private int getPagePosition() {
        return getPagePosition(this.page);
    }
    
    /**
     * 
     * @param page
     * @return 
     */
    private int getPagePosition(int page) {
        return pageSize * (this.page = page) - pageSize + 1;
    }

    /**
     *
     * @param index
     * @return
     */
    private int getResultSetIndex(int index) {
       return pageSize * this.page + index + 1;
    }
    
}
