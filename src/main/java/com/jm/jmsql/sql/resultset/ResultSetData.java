/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.jmsql.sql.resultset;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author Michael L.R. Marques
 */
public class ResultSetData implements List<Row> {
        
    private final List<Column> columns;
    private final List<Row> rows;
    private boolean changed;

    /**
     * 
     * @param results 
     * @throws SQLException
     */
    public ResultSetData(final ResultSet results) throws SQLException {
        ResultSetMetaData rsmd = results.getMetaData();
        this.rows = new ArrayList();
        this.columns = new ArrayList(rsmd.getColumnCount());
        for (int i = 1; i <= rsmd.getColumnCount(); i++) {
            this.columns.add(new Column(rsmd, i));
        }   
    }

    /**
     * 
     * @param results
     * @param index
     * @param length
     * @throws SQLException 
     */
    public void setData(final ResultSet results, int index, int length) throws SQLException {
        this.rows.clear();
        boolean next = results.absolute(index);
        // Add the rows to the row collection
        for (int i = 1; i <= length; i++) {
            // Check if the current record is valid
            if (next) {
                this.rows.add(new Row(i, this.columns, results));
            }
            // Check if there is a next record
            if (!(next = results.next())) {
                return;
            }
        }
    }

    /**
     * 
     * @return
     */
    public boolean isChanged() {
        return this.changed;
    }

    /**
     *
     * @param changed
     */
    public void setChanged(boolean changed) {
        this.changed = changed;
    }

    /**
     * 
     * @return 
     */
    public List<Column> getColumns() {
        return this.columns;
    }

    /**
     * 
     * @param index
     * @return 
     */
    public Column getColumn(int index) {
        return this.columns.get(index);
    }

    /**
     * 
     * @param row
     * @param column
     * @return 
     */
    public Cell getCell(int row, int column) {
        return this.rows.get(row).get(column);
    }

    /**
     * 
     * @param rowIndex
     * @param columnIndex
     * @param value
     * @return 
     */
    public Cell setCell(int rowIndex, int columnIndex, Object value) {
        Row row = this.rows.get(rowIndex);
        Cell cell = row.get(columnIndex);
        Object oldValue = cell.getValue();
        if ((oldValue == null && value != null) ||
                (oldValue != null && !oldValue.equals(value))) {
            cell.setValue(value);
            cell.setChanged(true);
            row.setChanged(true);
            setChanged(true);
        }
        
        return cell;
    }

    /**
     * 
     * @return 
     */
    @Override
    public int size() {
        return this.rows.size();
    }

    /**
     * 
     * @return 
     */
    @Override
    public boolean isEmpty() {
        return this.rows.isEmpty();
    }

    /**
     * 
     * @param o
     * @return 
     */
    @Override
    public boolean contains(Object o) {
        return this.rows.contains(o);
    }

    /**
     * 
     * @return 
     */
    @Override
    public Iterator<Row> iterator() {
        return this.rows.iterator();
    }

    /**
     * 
     * @return 
     */
    @Override
    public Object[] toArray() {
        return this.rows.toArray();
    }

    /**
     * 
     * @param <T>
     * @param a
     * @return 
     */
    @Override
    public <T> T[] toArray(T[] a) {
        return this.rows.toArray(a);
    }

    /**
     * 
     * @param e
     * @return 
     */
    @Override
    public boolean add(Row e) {
        return this.rows.add(e);
    }

    /**
     * 
     * @param o
     * @return 
     */
    @Override
    public boolean remove(Object o) {
        return this.rows.remove(o);
    }

    /**
     * 
     * @param c
     * @return 
     */
    @Override
    public boolean containsAll(Collection<?> c) {
        return this.rows.containsAll(c);
    }

    /**
     * 
     * @param c
     * @return 
     */
    @Override
    public boolean addAll(Collection<? extends Row> c) {
        return this.rows.addAll(c);
    }

    /**
     * 
     * @param index
     * @param c
     * @return 
     */
    @Override
    public boolean addAll(int index, Collection<? extends Row> c) {
        return this.rows.addAll(index, c);
    }

    /**
     * 
     * @param c
     * @return 
     */
    @Override
    public boolean removeAll(Collection<?> c) {
        return this.rows.removeAll(c);
    }

    /**
     * 
     * @param c
     * @return 
     */
    @Override
    public boolean retainAll(Collection<?> c) {
        return this.rows.retainAll(c);
    }

    /**
     * 
     */
    @Override
    public void clear() {
        this.rows.clear();
    }

    /**
     * 
     * @param index
     * @return 
     */
    @Override
    public Row get(int index) {
        return this.rows.get(index);
    }

    /**
     * 
     * @param index
     * @param element
     * @return 
     */
    @Override
    public Row set(int index, Row element) {
        return this.rows.set(index, element);
    }

    /**
     * 
     * @param index
     * @param element 
     */
    @Override
    public void add(int index, Row element) {
        this.rows.add(index, element);
    }

    /**
     * 
     * @param index
     * @return 
     */
    @Override
    public Row remove(int index) {
        return this.rows.remove(index);
    }

    /**
     * 
     * @param o
     * @return 
     */
    @Override
    public int indexOf(Object o) {
        return this.rows.indexOf(o);
    }

    /**
     * 
     * @param o
     * @return 
     */
    @Override
    public int lastIndexOf(Object o) {
        return this.rows.lastIndexOf(o);
    }

    /**
     * 
     * @return 
     */
    @Override
    public ListIterator<Row> listIterator() {
        return this.rows.listIterator();
    }

    /**
     * 
     * @param index
     * @return 
     */
    @Override
    public ListIterator<Row> listIterator(int index) {
        return this.rows.listIterator(index);
    }

    /**
     * 
     * @param fromIndex
     * @param toIndex
     * @return 
     */
    @Override
    public List<Row> subList(int fromIndex, int toIndex) {
        return this.rows.subList(fromIndex, toIndex);
    }

}
