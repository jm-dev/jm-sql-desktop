/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.jmsql.sql.resultset.renderers;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author Michael L.R. Marques
 * @param <T>
 */
public abstract class ResultSetTableCellRenderer<T extends Object> extends DefaultTableCellRenderer implements TableCellRenderer {

    private static final Color DEFAULT_SELECTED_COLOUR = new Color(100, 149, 237);
    private static final String NULL_STRING = "<null>";

    /**
     *
     * @param table
     * @param value
     * @param isSelected
     * @param hasFocus
     * @param row
     * @param column
     * @return
     */
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        JLabel label = getTableCellRendererComponent(this, table, (T) value, isSelected, hasFocus, row, column);
        if (value == null) {
            label.setText(NULL_STRING);
            label.setHorizontalAlignment(SwingConstants.CENTER);
            
        }
        
        if (isSelected) {
            label.setBackground(DEFAULT_SELECTED_COLOUR);
            label.setForeground(Color.white);
        } else {
            label.setBackground((row % 2 == 0) ? Color.lightGray : Color.white);
            label.setForeground(Color.black);
        }
        return label;
    }

    /**
     * 
     * @param tableCellRenderer
     * @param table
     * @param value
     * @param isSelected
     * @param hasFocus
     * @param row
     * @param column
     * @return
     */
    public abstract JLabel getTableCellRendererComponent(TableCellRenderer tableCellRenderer, JTable table, T value, boolean isSelected, boolean hasFocus, int row, int column);

}
