/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.jmsql.sql.resultset;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author Michael L.R. Marques
 */
public class Row implements List<Cell> {

    private final List<Column> columns;
    private final List<Cell> cells;
    private final int index;
    private boolean changed;

    /**
     *
     * @param index
     * @param results
     * @param columns
     * @throws SQLException
     */
    public Row(int index, List<Column> columns, ResultSet results) throws SQLException {
        this.columns = columns;
        this.index = index;
        this.cells = new ArrayList(columns.size());
        for (Column column : columns) {
            this.cells.add(new Cell(this, column, results.getObject(column.getName())));
        }
    }

    /**
     *
     * @return
     */
    public List<Column> getColumns() {
        return this.columns;
    }

    /**
     *
     * @return
     */
    public int getIndex() {
        return this.index;
    }

    /**
     *
     * @return
     */
    public boolean isChanged() {
        return this.changed;
    }

    /**
     *
     * @param changed
     */
    public void setChanged(boolean changed) {
        this.changed = changed;
    }

    /**
     *
     * @return
     */
    @Override
    public int size() {
        return this.cells.size();
    }

    /**
     *
     * @return
     */
    @Override
    public boolean isEmpty() {
        return this.cells.isEmpty();
    }

    /**
     *
     * @param o
     * @return
     */
    @Override
    public boolean contains(Object o) {
        return this.cells.contains(o);
    }

    /**
     *
     * @return
     */
    @Override
    public Iterator<Cell> iterator() {
        return this.cells.iterator();
    }

    /**
     *
     * @return
     */
    @Override
    public Object[] toArray() {
        return this.cells.toArray();
    }

    /**
     *
     * @param <T>
     * @param a
     * @return
     */
    @Override
    public <T> T[] toArray(T[] a) {
        return this.cells.toArray(a);
    }

    /**
     *
     * @param e
     * @return
     */
    @Override
    public boolean add(Cell e) {
        return this.cells.add(e);
    }

    /**
     *
     * @param o
     * @return
     */
    @Override
    public boolean remove(Object o) {
        return this.cells.remove(o);
    }

    /**
     *
     * @param c
     * @return
     */
    @Override
    public boolean containsAll(Collection<?> c) {
        return this.cells.containsAll(c);
    }

    /**
     *
     * @param c
     * @return
     */
    @Override
    public boolean addAll(Collection<? extends Cell> c) {
        return this.cells.addAll(c);
    }

    /**
     *
     * @param index
     * @param c
     * @return
     */
    @Override
    public boolean addAll(int index, Collection<? extends Cell> c) {
        return this.cells.addAll(index, c);
    }

    /**
     *
     * @param c
     * @return
     */
    @Override
    public boolean removeAll(Collection<?> c) {
        return this.cells.removeAll(c);
    }

    /**
     *
     * @param c
     * @return
     */
    @Override
    public boolean retainAll(Collection<?> c) {
        return this.cells.retainAll(c);
    }

    /**
     *
     */
    @Override
    public void clear() {
        this.cells.clear();
    }

    /**
     *
     * @param index
     * @return
     */
    @Override
    public Cell get(int index) {
        return this.cells.get(index);
    }

    /**
     *
     * @param index
     * @param element
     * @return
     */
    @Override
    public Cell set(int index, Cell element) {
        this.cells.get(index).setValue(element.getValue());
        return this.cells.get(index);
    }

    /**
     *
     * @param index
     * @param element
     */
    @Override
    public void add(int index, Cell element) {
        this.cells.add(index, element);
    }

    /**
     *
     * @param index
     * @return
     */
    @Override
    public Cell remove(int index) {
        return this.cells.remove(index);
    }

    /**
     *
     * @param o
     * @return
     */
    @Override
    public int indexOf(Object o) {
        return this.cells.indexOf(o);
    }

    /**
     *
     * @param o
     * @return
     */
    @Override
    public int lastIndexOf(Object o) {
        return this.cells.lastIndexOf(o);
    }

    /**
     *
     * @return
     */
    @Override
    public ListIterator<Cell> listIterator() {
        return this.cells.listIterator();
    }

    /**
     *
     * @param index
     * @return
     */
    @Override
    public ListIterator<Cell> listIterator(int index) {
        return this.cells.listIterator(index);
    }

    /**
     *
     * @param fromIndex
     * @param toIndex
     * @return
     */
    @Override
    public List<Cell> subList(int fromIndex, int toIndex) {
        return this.cells.subList(fromIndex, toIndex);
    }

}
