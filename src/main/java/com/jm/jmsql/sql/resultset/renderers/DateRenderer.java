/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.jmsql.sql.resultset.renderers;

import com.jm.jmsql.utils.Settings;
import java.util.Date;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author Marquema
 */
public class DateRenderer extends ResultSetTableCellRenderer<Date> {

    /**
     * 
     * @param tableCellRenderer
     * @param table
     * @param value
     * @param isSelected
     * @param hasFocus
     * @param row
     * @param column
     * @return
     */
    @Override
    public JLabel getTableCellRendererComponent(TableCellRenderer tableCellRenderer, JTable table, Date value, boolean isSelected, boolean hasFocus, int row, int column) {
        if (value != null) {
            setText(Settings.getDateFormat().format(value));
        }
        return this;
    }

}
