/*
 * Copyright (C) 2012-2013 Michael L.R. Marques
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Contact: michaellrmarques@gmail.com
 */

package com.jm.jmsql.sql;

import com.jm.commons.components.editor.JMEditorPane;
import com.jm.commons.components.editor.UndoRedoEvent;
import com.jm.commons.components.editor.UndoRedoListener;
import com.jm.commons.models.AdvancedSpinnerNumberModel;
import com.jm.jmsql.JMSqlApp;
import com.jm.jmsql.models.DefinitionComboBoxModel;
import com.jm.jmsql.models.LibraryComboBoxModel;
import com.jm.jmsql.models.SQLHistoryTableModel;
import com.jm.jmsql.models.VariablesComboBoxModel;
import com.jm.jmsql.sql.predictions.SQLPredictions;
import com.jm.jmsql.sql.predictions.events.PredictionEvent;
import com.jm.jmsql.sql.predictions.events.PredictionListener;
import com.jm.jmsql.sql.resultset.Column;
import com.jm.jmsql.sql.resultset.ResultSetTableModel;
import com.jm.jmsql.sql.resultset.renderers.BlobRenderer;
import com.jm.jmsql.sql.resultset.renderers.DateRenderer;
import com.jm.jmsql.sql.resultset.renderers.NumberRenderer;
import com.jm.jmsql.sql.resultset.renderers.StringRenderer;
import com.jm.jmsql.sql.var.VariableException;
import com.jm.jmsql.sql.var.VariableManager;
import com.jm.jmsql.sql.var.Variables;
import com.jm.jmsql.tabulation.TabPageHeader;
import com.jm.jmsql.utils.Constants.SQL;
import com.jm.jmsql.utils.Settings;
import com.jm.jmsql.xplora.Database;
import com.jm.jmsql.xplora.Databases;
import com.jm.jmsql.xplora.Definition;
import com.jm.jmsql.xplora.Library;
import com.jm.jmsql.xplora.events.DatabaseEvent;
import com.jm.jmsql.xplora.events.DatabaseListener;
import com.jm.jmsql.xplora.events.DefinitionEvent;
import com.jm.jmsql.xplora.events.DefinitionListener;
import com.jm.jmsql.xplora.exceptions.LoadDriverException;
import com.jm.jmsql.xport.XPortManager;
import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.FontMetrics;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.print.PrinterException;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.regex.Matcher;
import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * 
 * 
 * @author Michael L.R. Marques
 */
public class SQLEditor extends JPanel implements TableModelListener, UndoRedoListener, DatabaseListener, DefinitionListener, PredictionListener {
    
    public static final String NEW_SQL_EDITOR = "New.sql";
    public static final int DEFAULT_MAXIMUM_PAGE_SIZE = 1000000;
    public static final int DEFAULT_PAGE_SIZE = 20;
    
    private static final Logger log = LogManager.getLogger(SQLEditor.class);
    
    private static final AdvancedSpinnerNumberModel DEFAULT_PAGE_NUMBER_MODEL = new AdvancedSpinnerNumberModel(0, 0, 0, 1);
    
    private JMSqlApp app;
    private SQLPredictions predictions;
    private boolean edited;
    private boolean running;
    
    /**
     * Creates new form JMEditor
     * @param app
     */
    public SQLEditor(JMSqlApp app) {
        this(app, app.getDatabases());
    }
    
    /**
     * 
     * @param app
     * @param databases 
     */
    public SQLEditor(JMSqlApp app, Databases databases) {
        this(app, databases, (Definition) null);
    }
    
    /**
     * 
     * @param app
     * @param databases
     * @param definition 
     */
    public SQLEditor(JMSqlApp app, Databases databases, Definition definition) {
        this(app, databases, definition, null, false);
    }
    
    /**
     * 
     * @param app
     * @param databases
     * @param sqlFile
     * @throws IOException 
     */
    public SQLEditor(JMSqlApp app, Databases databases, File sqlFile) throws IOException {
        this(app, databases, null, SQLFileOperations.get(sqlFile));
    }
    
    /**
     * 
     * @param app
     * @param databases
     * @param definition
     * @param sqlFile
     * @throws IOException 
     */
    public SQLEditor(JMSqlApp app, Databases databases, Definition definition, File sqlFile) throws IOException {
        this(app, databases, definition, SQLFileOperations.get(sqlFile));
    }

    /**
     * 
     * @param app
     * @param databases
     * @param sql
     */
    public SQLEditor(JMSqlApp app, Databases databases, String sql) {
        this(app, databases, null, sql);
    }
    
    /**
     * 
     * @param app
     * @param databases
     * @param definition
     * @param sql 
     */
    public SQLEditor(JMSqlApp app, Databases databases, Definition definition, String sql) {
        this(app, databases, definition, sql, false);
    }
    
    /**
     * 
     * @param app
     * @param databases
     * @param definition
     * @param sql
     * @param execute 
     */
    public SQLEditor(JMSqlApp app, Databases databases, Definition definition, String sql, boolean execute) {
        initComponents();
        this.app = app;
        this.predictions = new SQLPredictions();
        this.predictions.addPredictionListener(this);
        this.codeEditor.setContentType("text/sql");
        this.codeEditor.setComponentPopupMenu(null);
        this.codeEditor.addUndoRedoListener(this);
        this.codeEditor.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (predictions.isVisible()) {
                    predictions.setVisible(false);
                }
            }
        });
        this.codeEditor.setText(sql);

        this.tableResults.setDefaultRenderer(String.class, new StringRenderer());
        this.tableResults.setDefaultRenderer(Number.class, new NumberRenderer());
        this.tableResults.setDefaultRenderer(Byte[].class, new BlobRenderer());
        this.tableResults.setDefaultRenderer(Date.class, new DateRenderer());

        this.cbxLibrarys.setVisible(false);
        // Add prediction listener if active
        if (Settings.isPredicting()) {
            this.codeEditor.addKeyListener(new KeyAdapter() {
                @Override
                public void keyPressed(KeyEvent e) {
                    // Only begin predicting if the definition is connected
                    if (cbxDefinitions.getSelectedItem() != null) {
                        final Definition definition = (Definition) cbxDefinitions.getSelectedItem();
                        final Library library = (Library) cbxLibrarys.getSelectedItem();
                        if (((Definition) cbxDefinitions.getSelectedItem()).isConnected()) {
                            if (predictions.isVisible()) {
                                switch (e.getKeyCode()) {
                                    case KeyEvent.VK_ESCAPE: e.consume(); predictions.setVisible(false); break;
                                    case KeyEvent.VK_ENTER:
                                    case KeyEvent.VK_DOWN:
                                    case KeyEvent.VK_UP: e.consume(); predictions.toFront(); break;
                                    default: predictions.predict(codeEditor, definition, library); break;
                                }
                            } else {
                                if ((KeyEvent.VK_SPACE == e.getKeyCode() &&
                                        KeyEvent.CTRL_MASK == e.getModifiers()) ||
                                            KeyEvent.VK_PERIOD == e.getKeyCode() ||
                                                KeyEvent.VK_PERIOD == e.getKeyCode() ||
                                                (definition.getParent().getLibraryDelimeter() != null &&
                                                    !definition.getParent().getLibraryDelimeter().isEmpty() &&
                                                        definition.getParent().getLibraryDelimeter().charAt(0) == e.getKeyChar())) {
                                    predictions.predict(codeEditor, definition, library);
                                }
                            }
                        }
                    }
                }
            });
        }
        this.spinnerPage.setModel(DEFAULT_PAGE_NUMBER_MODEL);
        this.spinnerPageSize.setModel(new AdvancedSpinnerNumberModel(DEFAULT_PAGE_SIZE, 1, DEFAULT_MAXIMUM_PAGE_SIZE, 1));

        for (Database aDatabase : databases) {
            aDatabase.addDatabaseListener(this);
            for (Definition aDefinition : aDatabase) {
                aDefinition.addDefinitionListener(this);
            }
        }
        this.cbxDefinitions.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                loadLibrarys();
            }
        });
        this.cbxDefinitions.setModel(new DefinitionComboBoxModel(databases, definition));
        this.cbxDefinitions.setRenderer(new DefinitionComboBoxModel.DefinitionComboBoxCellRenderer());
        
        this.tblHistory.setModel(new SQLHistoryTableModel());
        this.tblHistory.getColumnModel().getColumn(0).setCellRenderer(new SQLEditorTableCellRenderer(app));

        this.codeEditor.setDropTarget(new SQLFileDropTarget(this));
        try {
            this.cbxVariables.setModel(new VariablesComboBoxModel());
            this.cbxVariables.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                    if (cbxVariables.getItemCount() > 0 &&
                            cbxVariables.getSelectedIndex() >= 0) {
                        try {
                            if (Variables.getInstance().containsVariable((String) cbxVariables.getSelectedItem())) {
                                txtVariableValue.setText(Variables.getInstance().getVariable((String) cbxVariables.getSelectedItem()));
                            }
                        } catch (VariableException ve) {
                            log.error(ve.getMessage(), ve);
                        }
                    }
                }
            });
        } catch (VariableException ve) {
            log.error(ve.getMessage(), ve);
        }
        
        if (execute) {
            execute(sql);
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                loadLibrarys();
            }
        }).start();
    }

    /**
     *
     * @return
     */
    public JMSqlApp getApp() {
        return this.app;
    }

    /**
     * 
     * @param app
     */
    protected void setApp(JMSqlApp app) {
        this.app = app;
    }

    /**
     *
     * @return
     */
    public JMEditorPane getCodeEditor() {
        return this.codeEditor;
    }
    
    /**
     * 
     * @return 
     */
    public boolean canExecute() {
        return this.cbxDefinitions.getSelectedItem() != null;
    }
    
    /**
     * 
     * @param query 
     */
    public void setSQLQuery(String query) {
        this.codeEditor.setText(query);
    }
    
    /**
     * 
     * @return 
     */
    public String getSQLQuery() {
        return this.codeEditor.getText();
    }

    /**
     * 
     * @return
     */
    public boolean isFileSet() {
        return getHeader().getToolTipText() != null &&
                    !getHeader().getToolTipText().isEmpty();
    }

    /**
     *
     * @param file
     */
    public void setFile(File file) {
        try {
            this.codeEditor.setText(SQLFileOperations.get(file));
            getHeader().setText(file.getName());
            getHeader().setToolTipText(file.getAbsolutePath());
            getHeader().setEdited(false);
        } catch (IOException ioe) {
            log.error(ioe.getMessage(), ioe);
        }
    }

    /**
     * 
     * @return
     */
    public File getFile() {
        if (!isFileSet()) {
            return null;
        }
        return new File(getHeader().getToolTipText());
    }

    /**
     *
     */
    public void execute() {
        execute(this.codeEditor.getSelectedText() != null &&
                    !this.codeEditor.getSelectedText().trim().isEmpty() ? this.codeEditor.getSelectedText() : this.codeEditor.getText());
    }
    
    /**
     * 
     * @param sql 
     */
    protected void execute(final String sql) {
        // Check if a definition is selected
        if (this.cbxDefinitions.getSelectedItem() != null) {
            // Get the selected definition
            final Definition definition = (Definition) this.cbxDefinitions.getSelectedItem();
            // Check if the definition is connected
            if (!definition.isConnected()) {
                try {
                    // Connect the definition
                    if (!definition.connect()) {
                        return;
                    }
                } catch (LoadDriverException e) {
                    information(e.getMessage(), true);
                    return;
                }
            }
            final SQLEditor editor = this;
            // If connected try to execute the query
            if (definition.isConnected() &&
                    !definition.isExecuting()) {
                SwingWorker<Void, Void> worker = new SwingWorker() {
                    @Override
                    protected Void doInBackground() throws Exception {
                        for (String executableSQL : sql.split(SQL.DELIMETER)) {
                            if (!executableSQL.trim().isEmpty()) {
                                StringBuilder query = new StringBuilder(executableSQL);
                                if (!Variables.getInstance().isEmpty()) {
                                    Matcher matcher = Variables.getInstance().toPattern().matcher(executableSQL);
                                    while (matcher.find()) {
                                        try {
                                            int start = matcher.start();
                                            int end = matcher.end();
                                            String value = query.substring(start, end);
                                            if (value != null &&
                                                    Variables.getInstance().containsVariable(value.replace("$", ""))) {
                                                String variable = Variables.getInstance().getVariable(value.replace("$", ""));
                                                query.replace(start, end, variable);
                                                matcher = Variables.getInstance().toPattern().matcher(query);
                                            } else {
                                                JOptionPane.showMessageDialog(getApp(), "Variable", "Execute Query", JOptionPane.ERROR_MESSAGE);
                                                return null;
                                            }
                                        } catch (VariableException | HeadlessException e) {
                                            JOptionPane.showMessageDialog(getApp(), e.getMessage(), "Execute Query", JOptionPane.ERROR_MESSAGE);
                                            throw e;
                                        }
                                    }
                                }
                                
                                definition.execute(editor, query.toString(), (int) spinnerLimit.getValue());
                            }
                        }
                        return null;
                    }
                };
                worker.execute();
            }
        }
    }
    
    /**
     * 
     */
    public void stopExecution() {
        // Check if a definition is selected
        if (this.cbxDefinitions.getSelectedItem() != null) {
            // Get the selected definition
            Definition definition = (Definition) this.cbxDefinitions.getSelectedItem();
            try {
                definition.cancelExecute();
            } catch (SQLException sqle) {
                information(sqle.getMessage(), true);
            }
        }
    }
    
    /**
     * 
     */
    public void openSQLFile() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Open SQL File...");
        fileChooser.setCurrentDirectory(Settings.getSQLLocation());
        fileChooser.setFileFilter(SQLFileOperations.SQLFileFilter.getInstance());
        fileChooser.setMultiSelectionEnabled(false);
        if (JFileChooser.APPROVE_OPTION == fileChooser.showOpenDialog(this)) {
            setFile(fileChooser.getSelectedFile());
            Settings.setSQLLocation(fileChooser.getCurrentDirectory());
        }
    }
    
    /**
     * 
     */
    public void saveSQLFile() {
        saveSQLFile(false);
    }
    
    /**
     * 
     * @param saveAs 
     */
    public void saveSQLFile(boolean saveAs) {
        if (this.edited &&
                getHeader() != null) {
            if (!saveAs &&
                    getHeader().getToolTipText() != null &&
                        new File(getHeader().getToolTipText()).exists()) {
                try {
                    File file = new File(getHeader().getToolTipText());
                    SQLFileOperations.set(file, this.codeEditor.getText());
                    setFile(file);
                } catch (IOException ioe) {
                    log.error(ioe.getMessage(), ioe);
                }
            } else {
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setDialogTitle("Save SQL File...");
                fileChooser.setCurrentDirectory(new java.io.File("./"));
                fileChooser.setFileFilter(SQLFileOperations.SQLFileFilter.getInstance());
                fileChooser.setMultiSelectionEnabled(false);
                if (JFileChooser.APPROVE_OPTION == fileChooser.showSaveDialog(this)) {
                    try {
                        SQLFileOperations.set(fileChooser.getSelectedFile(), this.codeEditor.getText());
                        setFile(fileChooser.getSelectedFile());
                    } catch (IOException ioe) {
                        log.error(ioe.getMessage(), ioe);
                    }
                }
            }
        }
    }
    
    /**
     * 
     * @return 
     */
    public boolean isEdited() {
        return this.edited;
    }
    
    /**
     * 
     * @param edited 
     */
    public void setEdited(boolean edited) {
        if (getHeader() != null) {
            if (this.edited != edited) {
                this.edited = edited;
                getHeader().setEdited(this.edited);
            }
        }
    }
    
    /**
     * 
     * @return 
     */
    public boolean canUndo() {
       return this.codeEditor.canUndo(); 
    }
    
    /**
     * 
     */
    public void undo() {
        this.codeEditor.undo();
    }
    
    /**
     * 
     * @return 
     */
    public boolean canRedo() {
        return this.codeEditor.canRedo(); 
    }
    
    /**
     * 
     */
    public void redo() {
        this.codeEditor.redo();
    }
    
    /**
     * 
     */
    public void cut() {
        this.codeEditor.cut();
    }
    
    /**
     * 
     */
    public void copy() {
        this.codeEditor.copy();
    }
    
    /**
     * 
     */
    public void paste() {
        this.codeEditor.paste();
    }
    
    /**
     * 
     * @return 
     */
    public boolean canExport() {
        if (this.tableResults.getModel() instanceof ResultSetTableModel) {
            return ((ResultSetTableModel) this.tableResults.getModel()).getTotalRows() > 0;
        } else return false;
    }
    
    /**
     * 
     */
    public synchronized void export() {
        final XPortManager xportManager = new XPortManager(this.app);
        xportManager.setResultSet(((ResultSetTableModel) this.tableResults.getModel()).getResults());
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                xportManager.setVisible(true);
            }
        });
    }
    
    /**
     * 
     */
    public void find() {
        
    }
    
    /**
     * 
     */
    public void replace() {
        
    }
    
    /**
     * 
     * @throws PrinterException 
     */
    public void printSQL() throws PrinterException {
        this.codeEditor.print();
    }
    
    /**
     * 
     * @throws PrinterException 
     */
    public void printInformation() throws PrinterException {
        this.txtInformation.print();
    }
    
    /**
     * 
     */
    public void commentText() {
        int lastLineIndex = 0;
        for (String line : this.codeEditor.getText().split("[\n]")) {
            int caretPos = this.codeEditor.getCaretPosition();
            int lineIndex = (lastLineIndex = this.codeEditor.getText().indexOf(line, lastLineIndex));
            if (caretPos >= lineIndex &&
                    caretPos <= lineIndex + line.length()) {
                try {
                    commentLine(line);
                } catch (BadLocationException ble) {
                    ble.printStackTrace();
                }
            }
        }
    }
    
    /**
     * 
     * @param line 
     */
    private void commentLine(String line) throws BadLocationException {
        this.codeEditor.setText(this.codeEditor.getText().replace(line, "-- " + line));        
    }
    
    /**
     * 
     */
    public void selectAllText() {
       this.codeEditor.selectAll(); 
    }

    /**
     *
     * @param info
     * @param show
     */
    private void information(String info, boolean show) {
        information(info, false, show);
    }
    
    /**
     * 
     * @param info
     * @param show 
     */
    private void information(String info, boolean error, boolean show) {
        if (error) {
            Document document = this.txtInformation.getDocument();
            StyleContext context = new StyleContext();
            Style style = context.addStyle("error", null);
            try {
                document.insertString(this.txtInformation.getText().length() - 1, System.getProperty("line.separator") + info, style);
            } catch (BadLocationException ble) {
                log.error(ble.getMessage(), ble);
            }
        } else {
            this.txtInformation.setText(this.txtInformation.getText().trim() + System.getProperty("line.separator") + info);
        }
        if (show) {
            this.tabPane.setSelectedComponent(this.informationPanel);
        }
    }

    /**
     *
     */
    private void loadLibrarys() {
        if (cbxDefinitions.getItemCount() > 0 &&
                cbxDefinitions.getSelectedItem() != null) {
            Definition definition = (Definition) cbxDefinitions.getSelectedItem();
            if (definition.isConnected()) {
                if (!definition.isLibrary()) {
                    while (definition.isLoading()) {
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException ie) {
                            log.debug(ie.getMessage(), ie);
                        }
                    }
                    try {
                        cbxLibrarys.setVisible(true);
                        cbxLibrarys.setModel(new LibraryComboBoxModel(definition));
                        return;
                    } catch (SQLException sqle) {
                        log.debug(sqle.getMessage(), sqle);
                    }
                }
            }
        }
        cbxLibrarys.setVisible(false);
        cbxLibrarys.setModel(new DefaultComboBoxModel());
    }
    
    /**
     * 
     * @param e 
     */
    @Override 
    public void undoPerformed(UndoRedoEvent e) {
        this.btnUndo.setEnabled(e.getUndo());
        this.btnRedo.setEnabled(e.getRedo());
    }
    
    /**
     * 
     * @param e 
     */
    @Override 
    public void redoPerformed(UndoRedoEvent e) {
        this.btnUndo.setEnabled(e.getUndo());
        this.btnRedo.setEnabled(e.getRedo());
    }
    
    /**
     * 
     * @param e 
     */
    @Override 
    public void changePerformed(UndoRedoEvent e) {
        this.btnUndo.setEnabled(e.getUndo());
        this.btnRedo.setEnabled(e.getRedo());
        setEdited(true);
    }
    
    /**
     * 
     * @param e 
     */
    @Override 
    public void tableChanged(TableModelEvent e) {
        // Apply text information
        TableModel tableModel = (TableModel) e.getSource();
        if (tableModel instanceof ResultSetTableModel) {
            ResultSetTableModel resultSetTableModel = (ResultSetTableModel) tableModel;
            
            this.lblRows.setText("Total Rows: " + resultSetTableModel.getTotalRows());
            this.lblColumns.setText("Total Columns: " + resultSetTableModel.getTotalColumns());
            this.lblTotalPages.setText("Pages: " + resultSetTableModel.getPages());
            // Apply enabled/disabled buttons
            this.btnFitContents.setEnabled(resultSetTableModel.getTotalRows() >= 0);
            this.btnExport.setEnabled(resultSetTableModel.getTotalRows() >= 0);
            this.btnRefreshPage.setEnabled(true);
            this.btnFirstPage.setEnabled(!resultSetTableModel.isFirstPage());
            this.btnPreviousPage.setEnabled(resultSetTableModel.hasPreviousPage());
            this.btnNextPage.setEnabled(resultSetTableModel.hasNextPage());
            this.btnLastPage.setEnabled(!resultSetTableModel.isLastPage());
            this.spinnerPage.setEnabled(true);
            this.spinnerPage.setValue(resultSetTableModel.getPage());
            this.spinnerPageSize.setEnabled(true);
            this.btnEditMode.setEnabled(true);
            //        this.spinnerPageSize.setValue(tableModel.getPageSize());
        }
        // Adjust Number Column
        if (tableModel.getRowCount() > 1) {
            TableColumn column = this.tableResults.getTableHeader().getColumnModel().getColumn(0);
            column.setPreferredWidth(Math.max(this.tableResults.prepareRenderer(this.tableResults.getCellRenderer(tableModel.getRowCount()-1, 0), tableModel.getRowCount()-1, 0).getPreferredSize().width, 0));
        }
        this.tableResults.updateUI();
        this.tabPane.setSelectedComponent(this.resultsPanel);
        fitContents();
    }
    
    /**
     * 
     * @return 
     * @throws java.lang.CloneNotSupportedException
     */
    @Override 
    public SQLEditor clone() throws CloneNotSupportedException {
        SQLEditor sqlEditor = (SQLEditor) super.clone();
        sqlEditor.setApp(this.app);
//        ((Definition) this.cbxDefinitions.getSelectedItem()).getParent().getParent(),
//        (Definition) this.cbxDefinitions.getSelectedItem(),
//        this.codeEditor.getText());
        return sqlEditor;
    }
    
    /**
     * 
     * @return 
     */
    private TabPageHeader getHeader() {
        if (getParent() instanceof JTabbedPane &&
                getParent() != null) {
            ((JTabbedPane) getParent()).setSelectedComponent(this);
            return (TabPageHeader) ((JTabbedPane) getParent()).getTabComponentAt(((JTabbedPane) getParent()).getSelectedIndex());
        }
        return null;
    }
    
    /**
     * 
     * @param e 
     */
    @Override 
    public void connecting(DefinitionEvent e) {
        
    }
    
    /**
     * 
     * @param e 
     */
    @Override
    public void connectionSuccessful(DefinitionEvent e) {
        information("Connected to Database: " + e.getDefinition().getName() + ", Time Taken (s): " + (e.getSpent()/1000.00), true);
        selectedDefinitionChanged(null);
    }
    
    /**
     * 
     * @param e 
     */
    @Override 
    public void connectionFailed(DefinitionEvent e) {
        information(e.getException().getMessage(), true, true);
    }
    
    /**
     * 
     * @param e 
     */
    @Override 
    public void connected(DefinitionEvent e) {
        loadLibrarys();
    }
    
    /**
     * 
     * @param e 
     */
    @Override 
    public void disconnecting(DefinitionEvent e) {
        this.tableResults.setModel(new DefaultTableModel());
    }
    
    /**
     * 
     * @param e 
     */
    @Override 
    public void disconnected(DefinitionEvent e) {
        information("Disconnected from Database: " + e.getDefinition().getName() + ", Time Taken (s): " + (e.getSpent()/1000.00), true);
        loadLibrarys();
        selectedDefinitionChanged(null);
    }
    
    /**
     * 
     * @param e 
     */
    @Override 
    public void executing(DefinitionEvent e) {
        if (e.getSource().equals(this)) {
            this.btnPlay.setEnabled(false);
            this.btnStop.setEnabled(true);
            this.tableResults.setModel(new DefaultTableModel());
            SwingWorker<Boolean, Void> executingThread = new SwingWorker() {
                @Override
                protected Boolean doInBackground() throws Exception {
                    running = true;
                    lblStatus.setText("Running...");
                    lblStatus.update(lblStatus.getGraphics());
                    try {
                        while (running) {
                            prgRunning.setValue(prgRunning.getValue() >= prgRunning.getMaximum() ? prgRunning.getMinimum() : (prgRunning.getValue() < prgRunning.getMinimum() ? prgRunning.getMinimum() : prgRunning.getValue() + 5));
                            prgRunning.update(prgRunning.getGraphics());
                            Thread.sleep(100);
                        }
                        return true;
                    } finally {
                        prgRunning.setValue(prgRunning.getMaximum());
                        prgRunning.updateUI();
                    }
                }
            };
            executingThread.execute();
        }
    }

    /**
     *
     * @param e
     */
    @Override
    public void executionSuccessful(DefinitionEvent e) {
        if (e.getSource().equals(this)) {
            if (e.getHasResults()) {
                try {
                    ResultSetTableModel model = new ResultSetTableModel(e.getDefinition(), (int) this.spinnerLimit.getValue());
                    this.tableResults.setModel(model);
//                    this.spinnerPage.setModel(model.getTotalRows() == 0 ? DEFAULT_PAGE_NUMBER_MODEL : new AdvancedSpinnerNumberModel(1, 1, model.getPages(), 1));
                    model.addTableModelListener(this);
                    model.fireTableChanged();
                } catch (SQLException sqle) {
                    log.error(sqle.getMessage(), sqle);
                    information(e.getException().getMessage(), true);
                }
                information("Fetched Records: " + e.getDefinition().getRowCount() + ", Time Taken (s): " + (e.getSpent()/1000.00), false);
            } else {
                information("Updated Records: " + e.getDefinition().getUpdateCount() + ", Time Taken (s): " + (e.getSpent()/1000.00), true);
                this.tableResults.setModel(new DefaultTableModel());
            }
            this.lblStatus.setText("Done...");
        }
    }
    
    /**
     * 
     * @param e 
     */
    @Override
    public void executionFailed(DefinitionEvent e) {
        if (e.getSource().equals(this)) {
            log.error(e.getException().getMessage(), e.getException());
            information(e.getException().getMessage(), true);
            this.lblStatus.setText("Failed...");
        }
    }

    /**
     *
     * @param e
     */
    @Override
    public void executed(DefinitionEvent e) {
        if (e.getSource().equals(this)) {
            this.running = false;
            this.btnPlay.setEnabled(true);
            this.btnStop.setEnabled(false);
            
            SQLHistoryTableModel model = (SQLHistoryTableModel) this.tblHistory.getModel();
            if (!model.contains(e.getSQLQuery())) {
                model.add(0, e.getSQLQuery());
            }
        }
    }

    /**
     *
     * @param e
     */
    @Override
    public void addedDefinition(DatabaseEvent e) {
        Definition definition = (Definition) this.cbxDefinitions.getSelectedItem();
        this.cbxDefinitions.setModel(new DefinitionComboBoxModel(definition.getParent(Databases.class), definition));
    }

    /**
     *
     * @param e
     */
    @Override
    public void deletedDefinition(DatabaseEvent e) {
        Definition definition = (Definition) this.cbxDefinitions.getSelectedItem();
        if (this.cbxDefinitions.getSelectedItem().equals(definition)) {
            this.cbxDefinitions.setModel(new DefinitionComboBoxModel(definition.getParent(Databases.class), definition));
        } else {
            this.cbxDefinitions.setModel(new DefinitionComboBoxModel(definition.getParent(Databases.class)));
        }
    }
    
    /**
     * 
     * @param e 
     */
    @Override
    public void predicting(PredictionEvent e) {
        if (!this.predictions.isVisible()) {
            this.predictions.setLocation(codeEditor.getLocationOnScreen().x + codeEditor.getCaret().getMagicCaretPosition().x + 25, codeEditor.getLocationOnScreen().y + codeEditor.getCaret().getMagicCaretPosition().y + 25);
            this.predictions.setVisible(true);
        }
        this.codeEditor.requestFocusInWindow();
        this.codeEditor.requestFocus();
    }
    
    /**
     * 
     * @param e 
     */
    @Override
    public void predicted(PredictionEvent e) {
        if (e.isPredictionListEmpty()) {
            this.predictions.setVisible(false);
        }
        this.codeEditor.requestFocusInWindow();
        this.codeEditor.requestFocus();
    }
    
    /**
     * 
     * @param e 
     */
    @Override
    public void predictionSelected(PredictionEvent e) {
        this.predictions.setVisible(false);
        if (e.getCurrent() != null ||
                !e.getCurrent().isEmpty()) {
            for (int i = this.codeEditor.getCaretPosition(); i >= 0; i--) {
                if (this.codeEditor.getText().substring(i, this.codeEditor.getCaretPosition()).startsWith(e.getCurrent())) {
                    this.codeEditor.setSelectionStart(i);
                    break;
                }
            }
            this.codeEditor.setSelectionEnd(this.codeEditor.getCaretPosition());
        }
        this.codeEditor.replaceSelection(e.getPrediction().getName());
    }
    
    /**
     * 
     */
    private void fitContents() {
        if (this.btnFitContents.isSelected() &&
                this.tableResults.getRowCount() > 0) {
            DefaultTableColumnModel colModel = (DefaultTableColumnModel) this.tableResults.getColumnModel();
            for (int i = 0; i < colModel.getColumnCount(); i++) {
                TableColumn col = colModel.getColumn(i);
                int width;

                TableCellRenderer renderer = col.getHeaderRenderer();
                if (renderer == null) {
                    renderer = this.tableResults.getTableHeader().getDefaultRenderer();
                }
                Component comp = renderer.getTableCellRendererComponent(this.tableResults, col.getHeaderValue(), false, false, 0, 0);
                width = comp.getPreferredSize().width;

                for (int r = 0; r < this.tableResults.getRowCount(); r++) {
                    renderer = this.tableResults.getCellRenderer(r, i);
                    comp = renderer.getTableCellRendererComponent(this.tableResults, this.tableResults.getValueAt(r, i), false, false, r, i);
                    int currentWidth = comp.getPreferredSize().width;
                    width = Math.max(width, currentWidth);
                }

                width += 2 * 2;

                col.setPreferredWidth(width);
                col.setWidth(width);
            }
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainSplitPane = new javax.swing.JSplitPane();
        tabPane = new javax.swing.JTabbedPane();
        resultsPanel = new javax.swing.JPanel();
        jToolBar3 = new javax.swing.JToolBar();
        btnEditMode = new javax.swing.JToggleButton();
        btnCommit = new javax.swing.JButton();
        jSeparator8 = new javax.swing.JToolBar.Separator();
        lblTotalPages = new javax.swing.JLabel();
        jSeparator10 = new javax.swing.JToolBar.Separator();
        lblPage = new javax.swing.JLabel();
        spinnerPage = new javax.swing.JSpinner();
        btnRefreshPage = new javax.swing.JButton();
        btnFirstPage = new javax.swing.JButton();
        btnPreviousPage = new javax.swing.JButton();
        btnNextPage = new javax.swing.JButton();
        btnLastPage = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JToolBar.Separator();
        jLabel1 = new javax.swing.JLabel();
        spinnerPageSize = new javax.swing.JSpinner();
        jSeparator2 = new javax.swing.JToolBar.Separator();
        lblRows = new javax.swing.JLabel();
        jSeparator7 = new javax.swing.JToolBar.Separator();
        lblColumns = new javax.swing.JLabel();
        jSeparator11 = new javax.swing.JToolBar.Separator();
        btnExport = new javax.swing.JButton();
        btnFitContents = new javax.swing.JToggleButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableResults = new javax.swing.JTable();
        informationPanel = new javax.swing.JPanel();
        jToolBar1 = new javax.swing.JToolBar();
        btnClearInformation = new javax.swing.JButton();
        btnPrintInformation = new javax.swing.JButton();
        scpInformation = new javax.swing.JScrollPane();
        txtInformation = new javax.swing.JTextPane();
        historyPanel = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblHistory = new javax.swing.JTable();
        jScrollPane3 = new javax.swing.JScrollPane();
        codeEditor = new com.jm.commons.components.editor.JMEditorPane();
        jToolBar6 = new javax.swing.JToolBar();
        cbxDefinitions = new javax.swing.JComboBox();
        btnConnect = new javax.swing.JButton();
        btnDisconnect = new javax.swing.JButton();
        jSeparator3 = new javax.swing.JToolBar.Separator();
        btnPlay = new javax.swing.JButton();
        btnStop = new javax.swing.JButton();
        jSeparator12 = new javax.swing.JToolBar.Separator();
        btnOpen = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        jSeparator4 = new javax.swing.JToolBar.Separator();
        btnUndo = new javax.swing.JButton();
        btnRedo = new javax.swing.JButton();
        jSeparator5 = new javax.swing.JToolBar.Separator();
        btnCut = new javax.swing.JButton();
        btnCopy = new javax.swing.JButton();
        btnPaste = new javax.swing.JButton();
        jSeparator9 = new javax.swing.JToolBar.Separator();
        btnPrint = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        prgRunning = new javax.swing.JProgressBar();
        lblStatus = new javax.swing.JLabel();
        jToolBar7 = new javax.swing.JToolBar();
        cbxLibrarys = new javax.swing.JComboBox();
        jSeparator6 = new javax.swing.JToolBar.Separator();
        jLabel2 = new javax.swing.JLabel();
        spinnerLimit = new javax.swing.JSpinner();
        jSeparator13 = new javax.swing.JToolBar.Separator();
        cbxVariables = new javax.swing.JComboBox();
        btnInjectVariable = new javax.swing.JButton();
        btnEditVariables = new javax.swing.JButton();
        txtVariableValue = new com.jm.commons.components.textfield.HintJTextField();
        btnSetVariableValue = new javax.swing.JButton();

        mainSplitPane.setDividerLocation(230);
        mainSplitPane.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        mainSplitPane.setOneTouchExpandable(true);

        tabPane.setToolTipText("");
        tabPane.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N

        jToolBar3.setFloatable(false);
        jToolBar3.setRollover(true);

        btnEditMode.setIcon(new javax.swing.ImageIcon(getClass().getResource("/edit_records.png"))); // NOI18N
        btnEditMode.setToolTipText("Edit Mode");
        btnEditMode.setEnabled(false);
        btnEditMode.setFocusable(false);
        btnEditMode.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnEditMode.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnEditMode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editMode(evt);
            }
        });
        jToolBar3.add(btnEditMode);

        btnCommit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/commit_records.png"))); // NOI18N
        btnCommit.setToolTipText("Commit Recoreds");
        btnCommit.setEnabled(false);
        btnCommit.setFocusable(false);
        btnCommit.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnCommit.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnCommit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                commit(evt);
            }
        });
        jToolBar3.add(btnCommit);
        jToolBar3.add(jSeparator8);

        lblTotalPages.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        lblTotalPages.setText("Pages: 0");
        jToolBar3.add(lblTotalPages);
        jToolBar3.add(jSeparator10);

        lblPage.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        lblPage.setText("Page:");
        jToolBar3.add(lblPage);

        spinnerPage.setEnabled(false);
        spinnerPage.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spinnerPageStateChanged(evt);
            }
        });
        jToolBar3.add(spinnerPage);

        btnRefreshPage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/refresh_results.png"))); // NOI18N
        btnRefreshPage.setToolTipText("Refresh Results");
        btnRefreshPage.setEnabled(false);
        btnRefreshPage.setFocusable(false);
        btnRefreshPage.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnRefreshPage.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnRefreshPage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                refresh(evt);
            }
        });
        jToolBar3.add(btnRefreshPage);

        btnFirstPage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/first_results.png"))); // NOI18N
        btnFirstPage.setToolTipText("Go to First Page");
        btnFirstPage.setEnabled(false);
        btnFirstPage.setFocusable(false);
        btnFirstPage.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnFirstPage.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnFirstPage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                firstPage(evt);
            }
        });
        jToolBar3.add(btnFirstPage);

        btnPreviousPage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/previous_results.png"))); // NOI18N
        btnPreviousPage.setToolTipText("Previous Page");
        btnPreviousPage.setEnabled(false);
        btnPreviousPage.setFocusable(false);
        btnPreviousPage.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnPreviousPage.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnPreviousPage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                previousPage(evt);
            }
        });
        jToolBar3.add(btnPreviousPage);

        btnNextPage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/next_results.png"))); // NOI18N
        btnNextPage.setToolTipText("Next Page");
        btnNextPage.setEnabled(false);
        btnNextPage.setFocusable(false);
        btnNextPage.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnNextPage.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnNextPage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nextPage(evt);
            }
        });
        jToolBar3.add(btnNextPage);

        btnLastPage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/last_results.png"))); // NOI18N
        btnLastPage.setToolTipText("Go to Last Page");
        btnLastPage.setEnabled(false);
        btnLastPage.setFocusable(false);
        btnLastPage.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnLastPage.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnLastPage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lastPage(evt);
            }
        });
        jToolBar3.add(btnLastPage);
        jToolBar3.add(jSeparator1);

        jLabel1.setText("Rows: ");
        jToolBar3.add(jLabel1);

        spinnerPageSize.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        spinnerPageSize.setToolTipText("Number of Rows Per Page");
        spinnerPageSize.setEnabled(false);
        spinnerPageSize.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                pageSizeChanged(evt);
            }
        });
        jToolBar3.add(spinnerPageSize);
        jToolBar3.add(jSeparator2);

        lblRows.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        lblRows.setText("Total Rows: 0");
        jToolBar3.add(lblRows);
        jToolBar3.add(jSeparator7);

        lblColumns.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        lblColumns.setText("Total Columns: 0");
        jToolBar3.add(lblColumns);
        jToolBar3.add(jSeparator11);

        btnExport.setIcon(new javax.swing.ImageIcon(getClass().getResource("/xport.png"))); // NOI18N
        btnExport.setToolTipText("XPort Results... (Shift+Ctrl+X)");
        btnExport.setEnabled(false);
        btnExport.setFocusable(false);
        btnExport.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnExport.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnExport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExportActionPerformed(evt);
            }
        });
        jToolBar3.add(btnExport);

        btnFitContents.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fit.png"))); // NOI18N
        btnFitContents.setToolTipText("Fit Result Set Contects");
        btnFitContents.setFocusable(false);
        btnFitContents.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnFitContents.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnFitContents.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fitContents(evt);
            }
        });
        jToolBar3.add(btnFitContents);

        tableResults.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        tableResults.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tableResults.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tableResults.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                rowSelected(evt);
            }
        });
        tableResults.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                cellMouseOver(evt);
            }
        });
        jScrollPane1.setViewportView(tableResults);

        javax.swing.GroupLayout resultsPanelLayout = new javax.swing.GroupLayout(resultsPanel);
        resultsPanel.setLayout(resultsPanelLayout);
        resultsPanelLayout.setHorizontalGroup(
            resultsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar3, javax.swing.GroupLayout.DEFAULT_SIZE, 1097, Short.MAX_VALUE)
            .addGroup(resultsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1097, Short.MAX_VALUE))
        );
        resultsPanelLayout.setVerticalGroup(
            resultsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(resultsPanelLayout.createSequentialGroup()
                .addComponent(jToolBar3, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 161, Short.MAX_VALUE))
            .addGroup(resultsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, resultsPanelLayout.createSequentialGroup()
                    .addGap(26, 26, 26)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 160, Short.MAX_VALUE)))
        );

        tabPane.addTab("Results", new javax.swing.ImageIcon(getClass().getResource("/results.png")), resultsPanel, "SQL Results"); // NOI18N

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);

        btnClearInformation.setIcon(new javax.swing.ImageIcon(getClass().getResource("/clear.png"))); // NOI18N
        btnClearInformation.setFocusable(false);
        btnClearInformation.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnClearInformation.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnClearInformation.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clearInformation(evt);
            }
        });
        jToolBar1.add(btnClearInformation);

        btnPrintInformation.setIcon(new javax.swing.ImageIcon(getClass().getResource("/printer.png"))); // NOI18N
        btnPrintInformation.setFocusable(false);
        btnPrintInformation.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnPrintInformation.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnPrintInformation.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                printInformation(evt);
            }
        });
        jToolBar1.add(btnPrintInformation);

        scpInformation.setViewportView(txtInformation);

        javax.swing.GroupLayout informationPanelLayout = new javax.swing.GroupLayout(informationPanel);
        informationPanel.setLayout(informationPanelLayout);
        informationPanelLayout.setHorizontalGroup(
            informationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, 1097, Short.MAX_VALUE)
            .addComponent(scpInformation)
        );
        informationPanelLayout.setVerticalGroup(
            informationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(informationPanelLayout.createSequentialGroup()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scpInformation, javax.swing.GroupLayout.DEFAULT_SIZE, 155, Short.MAX_VALUE))
        );

        tabPane.addTab("Information", new javax.swing.ImageIcon(getClass().getResource("/information.png")), informationPanel, "Database SQL Information"); // NOI18N

        tblHistory.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "History"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(tblHistory);

        javax.swing.GroupLayout historyPanelLayout = new javax.swing.GroupLayout(historyPanel);
        historyPanel.setLayout(historyPanelLayout);
        historyPanelLayout.setHorizontalGroup(
            historyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 1097, Short.MAX_VALUE)
        );
        historyPanelLayout.setVerticalGroup(
            historyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(historyPanelLayout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 186, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        tabPane.addTab("History", new javax.swing.ImageIcon(getClass().getResource("/history.png")), historyPanel, "History of executed SQL queries"); // NOI18N

        mainSplitPane.setRightComponent(tabPane);
        tabPane.getAccessibleContext().setAccessibleName("tabbedPane");

        jScrollPane3.setViewportView(codeEditor);

        mainSplitPane.setLeftComponent(jScrollPane3);

        jToolBar6.setFloatable(false);

        cbxDefinitions.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                selectedDefinitionChanged(evt);
            }
        });
        jToolBar6.add(cbxDefinitions);

        btnConnect.setIcon(new javax.swing.ImageIcon(getClass().getResource("/connected_definition.png"))); // NOI18N
        btnConnect.setToolTipText("Connect");
        btnConnect.setFocusable(false);
        btnConnect.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnConnect.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnConnect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                connect(evt);
            }
        });
        jToolBar6.add(btnConnect);

        btnDisconnect.setIcon(new javax.swing.ImageIcon(getClass().getResource("/disconnected_definition.png"))); // NOI18N
        btnDisconnect.setToolTipText("Disconnect");
        btnDisconnect.setFocusable(false);
        btnDisconnect.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnDisconnect.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnDisconnect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                disconnect(evt);
            }
        });
        jToolBar6.add(btnDisconnect);
        jToolBar6.add(jSeparator3);

        btnPlay.setIcon(new javax.swing.ImageIcon(getClass().getResource("/play.png"))); // NOI18N
        btnPlay.setToolTipText("Execute Query (F5)");
        btnPlay.setFocusable(false);
        btnPlay.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnPlay.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnPlay.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                executeQuery(evt);
            }
        });
        jToolBar6.add(btnPlay);

        btnStop.setIcon(new javax.swing.ImageIcon(getClass().getResource("/stop.png"))); // NOI18N
        btnStop.setToolTipText("Stop Execution");
        btnStop.setEnabled(false);
        btnStop.setFocusable(false);
        btnStop.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnStop.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnStop.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stopExecution(evt);
            }
        });
        jToolBar6.add(btnStop);
        jToolBar6.add(jSeparator12);

        btnOpen.setIcon(new javax.swing.ImageIcon(getClass().getResource("/open.png"))); // NOI18N
        btnOpen.setToolTipText("Open... (Ctrl+O)");
        btnOpen.setFocusable(false);
        btnOpen.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnOpen.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnOpen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openSQLFile(evt);
            }
        });
        jToolBar6.add(btnOpen);

        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/save.png"))); // NOI18N
        btnSave.setToolTipText("Save (Ctrl+S)");
        btnSave.setFocusable(false);
        btnSave.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnSave.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveSQLFile(evt);
            }
        });
        jToolBar6.add(btnSave);
        jToolBar6.add(jSeparator4);

        btnUndo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/undo.png"))); // NOI18N
        btnUndo.setToolTipText("Undo (Ctrl+Z)");
        btnUndo.setEnabled(false);
        btnUndo.setFocusable(false);
        btnUndo.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnUndo.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnUndo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                undo(evt);
            }
        });
        jToolBar6.add(btnUndo);

        btnRedo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/redo.png"))); // NOI18N
        btnRedo.setToolTipText("Redo (Ctrl+Y)");
        btnRedo.setEnabled(false);
        btnRedo.setFocusable(false);
        btnRedo.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnRedo.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnRedo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                redo(evt);
            }
        });
        jToolBar6.add(btnRedo);
        jToolBar6.add(jSeparator5);

        btnCut.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cut.png"))); // NOI18N
        btnCut.setToolTipText("Cut (Ctrl+X)");
        btnCut.setFocusable(false);
        btnCut.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnCut.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnCut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cut(evt);
            }
        });
        jToolBar6.add(btnCut);

        btnCopy.setIcon(new javax.swing.ImageIcon(getClass().getResource("/copy.png"))); // NOI18N
        btnCopy.setToolTipText("Copy (Ctrl+C)");
        btnCopy.setFocusable(false);
        btnCopy.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnCopy.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnCopy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                copy(evt);
            }
        });
        jToolBar6.add(btnCopy);

        btnPaste.setIcon(new javax.swing.ImageIcon(getClass().getResource("/paste.png"))); // NOI18N
        btnPaste.setToolTipText("Paste (Ctrl+V)");
        btnPaste.setFocusable(false);
        btnPaste.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnPaste.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnPaste.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                paste(evt);
            }
        });
        jToolBar6.add(btnPaste);
        jToolBar6.add(jSeparator9);

        btnPrint.setIcon(new javax.swing.ImageIcon(getClass().getResource("/printer.png"))); // NOI18N
        btnPrint.setToolTipText("Print (Ctrl+P)");
        btnPrint.setFocusable(false);
        btnPrint.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnPrint.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                print(evt);
            }
        });
        jToolBar6.add(btnPrint);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(prgRunning, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(prgRunning, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jToolBar7.setFloatable(false);
        jToolBar7.setRollover(true);

        jToolBar7.add(cbxLibrarys);
        jToolBar7.add(jSeparator6);

        jLabel2.setText("Limit:");
        jToolBar7.add(jLabel2);

        spinnerLimit.setModel(new javax.swing.SpinnerNumberModel(0, 0, 10000, 1));
        jToolBar7.add(spinnerLimit);
        jToolBar7.add(jSeparator13);

        cbxVariables.setToolTipText("Variables...");
        jToolBar7.add(cbxVariables);

        btnInjectVariable.setIcon(new javax.swing.ImageIcon(getClass().getResource("/sort.png"))); // NOI18N
        btnInjectVariable.setToolTipText("Inject Variable");
        btnInjectVariable.setFocusable(false);
        btnInjectVariable.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnInjectVariable.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnInjectVariable.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                injectVariable(evt);
            }
        });
        jToolBar7.add(btnInjectVariable);

        btnEditVariables.setText("...");
        btnEditVariables.setToolTipText("Edit Variables");
        btnEditVariables.setFocusable(false);
        btnEditVariables.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnEditVariables.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnEditVariables.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editVariables(evt);
            }
        });
        jToolBar7.add(btnEditVariables);

        txtVariableValue.setHintText("Variable Value...");
        txtVariableValue.setToolTipText("");
        txtVariableValue.setMinimumSize(new java.awt.Dimension(85, 20));
        jToolBar7.add(txtVariableValue);

        btnSetVariableValue.setIcon(new javax.swing.ImageIcon(getClass().getResource("/commit_records.png"))); // NOI18N
        btnSetVariableValue.setToolTipText("Set Variable Value...");
        btnSetVariableValue.setFocusable(false);
        btnSetVariableValue.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnSetVariableValue.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnSetVariableValue.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                setVariableValue(evt);
            }
        });
        jToolBar7.add(btnSetVariableValue);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mainSplitPane)
            .addGroup(layout.createSequentialGroup()
                .addComponent(lblStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 553, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(jToolBar6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jToolBar7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jToolBar6, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jToolBar7, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(mainSplitPane, javax.swing.GroupLayout.DEFAULT_SIZE, 454, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblStatus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
    }// </editor-fold>//GEN-END:initComponents
    
    /**
     * 
     * @param evt 
     */
    private void selectedDefinitionChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_selectedDefinitionChanged
        if (this.cbxDefinitions.getSelectedItem() != null) {
            this.btnConnect.setEnabled(!((Definition) this.cbxDefinitions.getSelectedItem()).isConnected());
            this.btnDisconnect.setEnabled(((Definition) this.cbxDefinitions.getSelectedItem()).isConnected());
        }
    }//GEN-LAST:event_selectedDefinitionChanged
    
    /**
     * 
     * @param evt 
     */
    private void connect(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_connect
        if (this.cbxDefinitions.getSelectedItem() != null) {
            ((Definition) this.cbxDefinitions.getSelectedItem()).asyncConnect();
        }
    }//GEN-LAST:event_connect
    
    /**
     * 
     * @param evt 
     */
    private void disconnect(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_disconnect
        if (this.cbxDefinitions.getSelectedItem() != null) {
            ((Definition) this.cbxDefinitions.getSelectedItem()).disconnect();
        }
    }//GEN-LAST:event_disconnect
    
    /**
     * 
     * @param evt 
     */
    private void executeQuery(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_executeQuery
        execute();
    }//GEN-LAST:event_executeQuery
    
    /**
     * 
     * @param evt 
     */
    private void openSQLFile(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openSQLFile
        openSQLFile();
    }//GEN-LAST:event_openSQLFile
    
    /**
     * 
     * @param evt 
     */
    private void saveSQLFile(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveSQLFile
        saveSQLFile(getHeader().getText().equals(NEW_SQL_EDITOR));
    }//GEN-LAST:event_saveSQLFile
    
    /**
     * 
     * @param evt 
     */
    private void undo(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_undo
        undo();
    }//GEN-LAST:event_undo
    
    /**
     * 
     * @param evt 
     */
    private void redo(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_redo
        redo();
    }//GEN-LAST:event_redo
    
    /**
     * 
     * @param evt 
     */
    private void cut(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cut
        cut();
    }//GEN-LAST:event_cut
    
    /**
     * 
     * @param evt 
     */
    private void copy(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_copy
        copy();
    }//GEN-LAST:event_copy
    
    /**
     * 
     * @param evt 
     */
    private void paste(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_paste
        paste();
    }//GEN-LAST:event_paste
    
    /**
     * 
     * @param evt 
     */
    private void pageSizeChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_pageSizeChanged
        try {
            ResultSetTableModel model = (ResultSetTableModel) this.tableResults.getModel();
            model.setPageSize(((Number) this.spinnerPageSize.getValue()).intValue());
            this.spinnerPage.setModel(model.getTotalRows() == 0 ? DEFAULT_PAGE_NUMBER_MODEL : new AdvancedSpinnerNumberModel(1, 1, model.getPages(), 1));
        } catch (SQLException sqle) {
            log.error(sqle.getMessage(), sqle);
            information(sqle.getMessage(), true);
        }
    }//GEN-LAST:event_pageSizeChanged
    
    /**
     * 
     * @param evt 
     */
    private void refresh(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_refresh
        try {
            ((ResultSetTableModel) this.tableResults.getModel()).refresh();
        } catch (SQLException sqle) {
            log.error(sqle.getMessage(), sqle);
            information(sqle.getMessage(), true);
        }
    }//GEN-LAST:event_refresh
    
    /**
     * 
     * @param evt 
     */
    private void firstPage(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_firstPage
        try {
            ((ResultSetTableModel) this.tableResults.getModel()).firstPage();
        } catch (SQLException sqle) {
            log.error(sqle.getMessage(), sqle);
            information(sqle.getMessage(), true);
        }
    }//GEN-LAST:event_firstPage
    
    /**
     * 
     * @param evt 
     */
    private void previousPage(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_previousPage
        try {
            ((ResultSetTableModel) this.tableResults.getModel()).previousPage();
        } catch (SQLException sqle) {
            log.error(sqle.getMessage(), sqle);
            information(sqle.getMessage(), true);
        }
    }//GEN-LAST:event_previousPage
    
    /**
     * 
     * @param evt 
     */
    private void nextPage(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nextPage
        try {
            ((ResultSetTableModel) this.tableResults.getModel()).nextPage();
        } catch (SQLException sqle) {
            log.error(sqle.getMessage(), sqle);
            information(sqle.getMessage(), true);
        }
    }//GEN-LAST:event_nextPage
    
    /**
     * 
     * @param evt 
     */
    private void lastPage(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lastPage
        try {
            ((ResultSetTableModel) this.tableResults.getModel()).lastPage();
        } catch (SQLException sqle) {
            log.error(sqle.getMessage(), sqle);
            information(sqle.getMessage(), true);
        }
    }//GEN-LAST:event_lastPage
    
    /**
     * 
     * @param evt 
     */
    private void print(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_print
        try {
            printSQL();
        } catch (PrinterException pe) {
            log.error(pe.getMessage(), pe);
        }
    }//GEN-LAST:event_print
    
    /**
     * 
     * @param evt 
     */
    private void btnExportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExportActionPerformed
        export();
    }//GEN-LAST:event_btnExportActionPerformed
    
    /**
     * 
     * @param evt 
     */
    private void rowSelected(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rowSelected
        if (evt.getClickCount() == 1 &&
                evt.getButton() == java.awt.event.MouseEvent.BUTTON3) {
            SQLEditorTablePopupMenu.show(this.tableResults, evt.getX(), evt.getY(), this.tableResults.getSelectedRow(), this.tableResults.getSelectedColumn());
        } else if (evt.getClickCount() == 2 &&
                evt.getButton() == java.awt.event.MouseEvent.BUTTON1) {
            System.out.println(evt);
            if (this.tableResults.getRowCount() > 0 &&
                    this.tableResults.getSelectedRow() >= 0 &&
                        this.tableResults.getColumnCount() > 0 &&
                            this.tableResults.getSelectedColumn() >= 0) {
                if (this.tableResults.getValueAt(this.tableResults.getSelectedRow(), this.tableResults.getSelectedColumn()) != null &&
                        !this.tableResults.getValueAt(this.tableResults.getSelectedRow(), this.tableResults.getSelectedColumn()).toString().isEmpty()) {
                    Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                    ClipboardOwner owner = new ClipboardOwner() {
                        @Override
                        public void lostOwnership(Clipboard clipboard, Transferable contents) {
                            // Do nothing
                        }
                    };
                    clipboard.setContents(new StringSelection(this.tableResults.getValueAt(this.tableResults.getSelectedRow(), this.tableResults.getSelectedColumn()).toString().trim()), owner);
                }
            }
        }
    }//GEN-LAST:event_rowSelected
    
    /**
     * 
     * @param evt 
     */
    private void spinnerPageStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spinnerPageStateChanged
        try {
            ((ResultSetTableModel) this.tableResults.getModel()).setPage(((Number) this.spinnerPage.getValue()).intValue());
        } catch (SQLException sqle) {
            log.error(sqle.getMessage(), sqle);
            information(sqle.getMessage(), true);
        }
    }//GEN-LAST:event_spinnerPageStateChanged
    
    /**
     * 
     * @param evt 
     */
    private void printInformation(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_printInformation
        try {
            printInformation();
        } catch (PrinterException pe) {
            log.error(pe.getMessage(), pe);
        }
    }//GEN-LAST:event_printInformation
    
    /**
     * 
     * @param evt 
     */
    private void clearInformation(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clearInformation
        this.txtInformation.setText("");
    }//GEN-LAST:event_clearInformation
    
    /**
     * 
     * @param evt 
     */
    private void fitContents(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fitContents
//        SwingWorker<Void, Void> fitContentsThread = new SwingWorker<Void, Void>() {
//            @Override
//            protected Void doInBackground() throws Exception {
//                fitContents();
//                return null;
//            }
//        };
//        fitContentsThread.execute();
        fitContents();
    }//GEN-LAST:event_fitContents
    
    /**
     * 
     * @param evt 
     */
    private void cellMouseOver(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cellMouseOver
        if (this.tableResults.getColumnCount() > 0) {
            int columnIndex = this.tableResults.columnAtPoint(evt.getPoint());
            if (columnIndex > 0) {
                ResultSetTableModel model = (ResultSetTableModel) this.tableResults.getModel();
                Column column = model.getData().getColumn(columnIndex - 1);
                
                this.tableResults.setToolTipText(column.getProperties());
            }
        }
    }//GEN-LAST:event_cellMouseOver

    /**
     *
     * @param evt
     */
    private void stopExecution(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stopExecution
        stopExecution();
    }//GEN-LAST:event_stopExecution

    /**
     *
     * @param evt
     */
    private void injectVariable(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_injectVariable
        if (this.cbxVariables.getItemCount() > 0 &&
                this.cbxVariables.getSelectedIndex() >= 0) {
            if (this.codeEditor.getSelectedText() != null &&
                    !this.codeEditor.getSelectedText().isEmpty()) {
                this.codeEditor.replaceSelection("$" + this.cbxVariables.getSelectedItem());
            } else {
                SimpleAttributeSet keyWord = new SimpleAttributeSet();
                StyleConstants.setForeground(keyWord, Color.pink);
                StyleConstants.setBackground(keyWord, Color.white);
                StyleConstants.setBold(keyWord, true);
                try {
                    this.codeEditor.getDocument().insertString(this.codeEditor.getCaretPosition(), "$" + this.cbxVariables.getSelectedItem(), keyWord);
                } catch (BadLocationException ble) {
                    log.error(ble.getMessage(), ble);
                }
            }
        }
    }//GEN-LAST:event_injectVariable

    /**
     *
     * @param evt
     */
    private void editVariables(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editVariables
        EventQueue.invokeLater(new Runnable() {
            @Override public void run() {
                new VariableManager(getApp()).setVisible(true);
            }
        });
    }//GEN-LAST:event_editVariables

    /**
     *
     * @param evt
     */
    private void setVariableValue(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_setVariableValue
        if (this.cbxVariables.getItemCount() == 0 ||
                this.cbxVariables.getSelectedIndex() < 0) {
            JOptionPane.showMessageDialog(this, "No Variable Selected", "Set Variable Value...", JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (this.txtVariableValue.getText() == null ||
                this.txtVariableValue.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Variable value cannot be empty", "Set Variable Value...", JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        try {
            Variables.getInstance().setVariable((String) this.cbxVariables.getSelectedItem(), this.txtVariableValue.getText());
        } catch (VariableException ve) {
            JOptionPane.showMessageDialog(this, ve.getMessage(), "Set Variable Value...", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_setVariableValue
    
    /**
     * 
     * @param evt 
     */
    private void commit(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_commit
        ResultSetTableModel model = (ResultSetTableModel) this.tableResults.getModel();
        try {
            model.commit();
            model.setEditing(false);
            this.btnCommit.setEnabled(false);
            this.btnEditMode.setSelected(false);
        } catch (SQLException sqle) {
            information(sqle.getMessage(), true);
        }
    }//GEN-LAST:event_commit
    
    /**
     * 
     * @param evt 
     */
    private void editMode(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editMode
        ResultSetTableModel model = (ResultSetTableModel) this.tableResults.getModel();
        model.setEditing(this.btnEditMode.isSelected());
        this.btnCommit.setEnabled(this.btnEditMode.isSelected());
    }//GEN-LAST:event_editMode

   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClearInformation;
    private javax.swing.JButton btnCommit;
    private javax.swing.JButton btnConnect;
    private javax.swing.JButton btnCopy;
    private javax.swing.JButton btnCut;
    private javax.swing.JButton btnDisconnect;
    private javax.swing.JToggleButton btnEditMode;
    private javax.swing.JButton btnEditVariables;
    private javax.swing.JButton btnExport;
    private javax.swing.JButton btnFirstPage;
    private javax.swing.JToggleButton btnFitContents;
    private javax.swing.JButton btnInjectVariable;
    private javax.swing.JButton btnLastPage;
    private javax.swing.JButton btnNextPage;
    private javax.swing.JButton btnOpen;
    private javax.swing.JButton btnPaste;
    private javax.swing.JButton btnPlay;
    private javax.swing.JButton btnPreviousPage;
    private javax.swing.JButton btnPrint;
    private javax.swing.JButton btnPrintInformation;
    private javax.swing.JButton btnRedo;
    private javax.swing.JButton btnRefreshPage;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnSetVariableValue;
    private javax.swing.JButton btnStop;
    private javax.swing.JButton btnUndo;
    private javax.swing.JComboBox cbxDefinitions;
    private javax.swing.JComboBox cbxLibrarys;
    private javax.swing.JComboBox cbxVariables;
    private com.jm.commons.components.editor.JMEditorPane codeEditor;
    private javax.swing.JPanel historyPanel;
    private javax.swing.JPanel informationPanel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JToolBar.Separator jSeparator1;
    private javax.swing.JToolBar.Separator jSeparator10;
    private javax.swing.JToolBar.Separator jSeparator11;
    private javax.swing.JToolBar.Separator jSeparator12;
    private javax.swing.JToolBar.Separator jSeparator13;
    private javax.swing.JToolBar.Separator jSeparator2;
    private javax.swing.JToolBar.Separator jSeparator3;
    private javax.swing.JToolBar.Separator jSeparator4;
    private javax.swing.JToolBar.Separator jSeparator5;
    private javax.swing.JToolBar.Separator jSeparator6;
    private javax.swing.JToolBar.Separator jSeparator7;
    private javax.swing.JToolBar.Separator jSeparator8;
    private javax.swing.JToolBar.Separator jSeparator9;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JToolBar jToolBar3;
    private javax.swing.JToolBar jToolBar6;
    private javax.swing.JToolBar jToolBar7;
    private javax.swing.JLabel lblColumns;
    private javax.swing.JLabel lblPage;
    private javax.swing.JLabel lblRows;
    private javax.swing.JLabel lblStatus;
    private javax.swing.JLabel lblTotalPages;
    private javax.swing.JSplitPane mainSplitPane;
    private javax.swing.JProgressBar prgRunning;
    private javax.swing.JPanel resultsPanel;
    private javax.swing.JScrollPane scpInformation;
    private javax.swing.JSpinner spinnerLimit;
    private javax.swing.JSpinner spinnerPage;
    private javax.swing.JSpinner spinnerPageSize;
    private javax.swing.JTabbedPane tabPane;
    private javax.swing.JTable tableResults;
    private javax.swing.JTable tblHistory;
    private javax.swing.JTextPane txtInformation;
    private com.jm.commons.components.textfield.HintJTextField txtVariableValue;
    // End of variables declaration//GEN-END:variables
    
}

/**
 * http://www.java2s.com/Tutorial/Java/0240__Swing/CreatingaJTablewithrowsofvariableheight.htm
 *
 * @author Matthias Bonora (mat.bonora AT gmail.com), Michael L.R. Marques
 */
class SQLEditorTableCellRenderer extends JMEditorPane implements TableCellRenderer, MouseListener {

    private final JMSqlApp app;

    /**
     *
     */
    public SQLEditorTableCellRenderer(JMSqlApp app) {
        super();
        this.app = app;
        setEditable(false);
//        setContentType("text/sql");
        addMouseListener(this);
    }

    /**
     *
     * @param table
     * @param value
     * @param isSelected
     * @param hasFocus
     * @param row
     * @param column
     * @return
     */
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        if (isSelected) {
            setBackground(UIManager.getColor("Table.selectionBackground"));
        }

        if (hasFocus) {
            setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));

            if (table.isCellEditable(row, column)) {
                super.setForeground(UIManager.getColor("Table.focusCellForeground"));
                super.setBackground(UIManager.getColor("Table.focusCellBackground"));
            }
        } else {
            setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        }
        
        setText((String) value);
        table.setRowHeight(row, getRowHeight((String) value));
        
        return this;
    }

    /**
     *
     * @return
     */
    private int getRowHeight(String value) {
        FontMetrics fontMetrics = getFontMetrics(getFont());
        fontMetrics.getLineMetrics(value, getGraphics());
        return fontMetrics.getHeight();
    }

    /**
     *
     * @param e
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        this.app.newSQLEditor(getText());
    }

    /**
     *
     * @param e
     */
    @Override
    public void mousePressed(MouseEvent e) {
        
    }

    /**
     *
     * @param e
     */
    @Override
    public void mouseReleased(MouseEvent e) {
        
    }

    /**
     *
     * @param e
     */
    @Override
    public void mouseEntered(MouseEvent e) {
        
    }

    /**
     * 
     * @param e
     */
    @Override
    public void mouseExited(MouseEvent e) {
        
    }

}
