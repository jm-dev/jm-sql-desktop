/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.jmsql.sql;

import com.jm.jmsql.JMSqlApp;
import java.awt.Component;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDropEvent;
import java.io.File;
import java.io.IOException;
import java.util.List;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Michael L.R. Marques
 */
public class SQLFileDropTarget extends DropTarget {

    private static final Logger logger = LogManager.getLogger(SQLFileDropTarget.class);

    private final JMSqlApp app;
    private SQLEditor sqlEditor;

    public SQLFileDropTarget(SQLEditor sqlEditor) {
        this.app = sqlEditor.getApp();
        this.sqlEditor = sqlEditor;
        this.setComponent(sqlEditor.getCodeEditor());
    }

    public SQLFileDropTarget(JMSqlApp app) {
        this.app = app;
        this.setComponent(app.tabPages);
    }

    @Override
    public synchronized void drop(DropTargetDropEvent evt) {
        try {
            evt.acceptDrop(DnDConstants.ACTION_COPY);
            List<File> droppedFiles = (List<File>) evt.getTransferable().getTransferData(DataFlavor.javaFileListFlavor);
            if (droppedFiles.size() > 0) {
                Component component = getComponent();
                if (this.sqlEditor.getCodeEditor() != null &&
                        component.equals(this.sqlEditor.getCodeEditor())) {
                    for (File file : droppedFiles) {
                        this.sqlEditor.setFile(file);
                    }
                } else if (this.app != null &&
                        component.equals(this.app.tabPages)) {
                    for (File file : droppedFiles) {
                        this.app.newSQLEditor(file);
                    }
                }
            }
        } catch (UnsupportedFlavorException | IOException e) {
            logger.error(e.getMessage(), e);
        }
    }

}
