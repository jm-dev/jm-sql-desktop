/*
 * Copyright (C) 2013 Michael
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.jm.jmsql.sql;

import com.jm.jmsql.sql.resultset.ResultSetTableModel;
import com.jm.jmsql.xport.XPortManager;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;

/**
 *
 * @created Feb 20, 2013
 * @author Michael L.R. Marques
 */
public class SQLEditorTablePopupMenu extends JPopupMenu {
    
    /**
     * 
     * @param table
     * @param x
     * @param y
     * @param row
     * @param column 
     */
    public static void show(final JTable table, final int x, final int y, final int row, final int column) {
        SQLEditorTablePopupMenu popup = new SQLEditorTablePopupMenu();
        if (table.getModel() instanceof ResultSetTableModel) {
            final ResultSetTableModel model = (ResultSetTableModel) table.getModel();
            if (model.getTotalColumns() > 0) {
                JMenuItem grabColumn = new JMenuItem("Grab Column Name...");
                grabColumn.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        String columnName = model.getColumnName(column);
                        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                        ClipboardOwner owner = new ClipboardOwner() {
                            @Override 
                            public void lostOwnership(Clipboard clipboard, Transferable contents) {
                                // Do nothing
                            }
                        };
                        clipboard.setContents(new StringSelection(columnName), owner);
                    }
                });
                if (column >= 0) {
                    popup.add(grabColumn);
                }
            }
            if (model.getTotalColumns() > 0 &&
                    model.getTotalRows() > 0) {
                JMenuItem grabCell = new JMenuItem("Grab Cell Content...");
                grabCell.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        Object value = model.getValueAt(row, column);
                        if (value != null &&
                                !value.toString().isEmpty()) {
                            Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                            ClipboardOwner owner = new ClipboardOwner() {
                                @Override 
                                public void lostOwnership(Clipboard clipboard, Transferable contents) {
                                    // Do nothing
                                }
                            };
                            clipboard.setContents(new StringSelection(value.toString()), owner);
                        }
                    }
                });
                if (row >= 0 &&
                        column >= 0) {
                    popup.add(grabCell);
                }
            }
            if (model.getTotalRows() > 0) {
                JMenuItem plain = new JMenuItem("Plain Output...");
                plain.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        model.setColumnToString(column);
                    }
                });
                // XPort Results
                JMenuItem export = new JMenuItem("XPort Results...");
                export.setIcon(new ImageIcon(popup.getClass().getResource("/xport.png")));
                export.addActionListener(new ActionListener() {
                    @Override public void actionPerformed(ActionEvent e) {
                        final XPortManager xpm = new XPortManager(table.getRootPane());
                        xpm.setResultSet(model.getResults());
                        java.awt.EventQueue.invokeLater(new Runnable() {
                            @Override public void run() {
                                xpm.setVisible(true);
                            }
                        });
                    }
                });
                // Add Popup menu items
                if (row >= 0 &&
                        column >= 0 &&
                            !model.getColumnClass(column).equals(String.class)) {
                    if (popup.getComponentCount() > 0) {
                        popup.addSeparator();
                    }
//                    popup.add(plain);
//                    popup.addSeparator();
                }
                popup.add(export);
            }
        }
        
        if (popup.getComponentCount() > 0) {
            popup.show(table, x, y);
        }
    }
    
}
