/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.jmsql.sql.var.event;

import java.util.EventListener;

/**
 *
 * @author Michael L.R. Marques
 */
public interface VariableListener extends EventListener {

    void variableAdded(VariableEvent ve);

    void variableDeleted(VariableEvent ve);

    void variableChanged(VariableEvent ve);

}
