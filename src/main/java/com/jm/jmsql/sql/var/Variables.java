/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.jmsql.sql.var;

import com.jm.jmsql.sql.var.event.VariableEvent;
import com.jm.jmsql.sql.var.event.VariableListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.regex.Pattern;
import javax.swing.event.EventListenerList;

/**
 *
 * @author Michael L.R. Marques
 */
public class Variables {
    
    private static final File VAR_FILE = new File("./settings/variables.properties");

    private static Variables instance;

    private final EventListenerList eventListeners;
    private final Map<String, String> variables;

    /**
     *
     * @throws VariableException
     */
    private Variables() throws VariableException {
        this.eventListeners = new EventListenerList();
        this.variables = new HashMap<>();
        if (VAR_FILE.exists()) {
            try (FileInputStream input = new FileInputStream(VAR_FILE)) {
                Properties props = new Properties();
                props.load(input);
                for (Entry<Object, Object> entry : props.entrySet()) {
                    this.variables.put(entry.getKey().toString(), entry.getValue().toString());
                }
            } catch (IOException ioe) {
                throw new VariableException(ioe.getMessage(), ioe);
            }
        } else {
            VAR_FILE.getParentFile().mkdirs();
            try {
                VAR_FILE.createNewFile();
            } catch (IOException ioe) {
                throw new VariableException(ioe.getMessage(), ioe);
            }
        }
    }

    /**
     *
     * @return
     * @throws VariableException
     */
    public static Variables getInstance() throws VariableException {
        if (instance == null) {
            instance = new Variables();
        }
        return instance;
    }

    /**
     *
     * @return
     */
    public Map<String, String> getVariables() {
        return this.variables;
    }

    /**
     *
     * @param key
     * @return
     */
    public boolean containsVariable(String key) {
        return this.variables.containsKey(key);
    }

    /**
     *
     * @param key
     * @return
     */
    public String getVariable(String key) {
        return this.variables.get(key);
    }

    /**
     *
     * @param key
     * @throws VariableException
     */
    public void addVariable(String key) throws VariableException {
        setVariable(key, "");
    }

    /**
     *
     * @param key
     * @param value
     * @throws VariableException
     */
    public void setVariable(String key, String value) throws VariableException {
        boolean variableExists = containsVariable(key);

        this.variables.put(key, value);

        if (variableExists) {
            fireVariableChanged(key);
        } else {
            fireVariableAdded(key);
        }
        
        save();
    }

    /**
     *
     * @param key
     * @throws VariableException
     */
    public void deleteVariable(String key) throws VariableException {
        if (!containsVariable(key)) {
            throw new VariableException("Key " + key + " does not exist");
        }
        
        this.variables.remove(key);

        fireVariableDeleted(key);
        
        save();
    }

    /**
     *
     * @return
     */
    public boolean isEmpty() {
        return this.variables.isEmpty();
    }

    /**
     *
     * @param listener
     */
    public void addVariableListener(VariableListener listener) {
        this.eventListeners.add(VariableListener.class, listener);
    }

    /**
     *
     * @param listener
     */
    public void deleteVariableListener(VariableListener listener) {
        this.eventListeners.remove(VariableListener.class, listener);
    }

    /**
     *
     * @param key
     */
    public void fireVariableAdded(String key) {
        for (VariableListener listener : this.eventListeners.getListeners(VariableListener.class)) {
            listener.variableAdded(new VariableEvent(this, key));
        }
    }

    /**
     *
     * @param key
     */
    public void fireVariableDeleted(String key) {
        for (VariableListener listener : this.eventListeners.getListeners(VariableListener.class)) {
            listener.variableDeleted(new VariableEvent(this, key));
        }
    }

    /**
     *
     * @param key
     */
    public void fireVariableChanged(String key) {
        for (VariableListener listener : this.eventListeners.getListeners(VariableListener.class)) {
            listener.variableChanged(new VariableEvent(this, key));
        }
    }

    /**
     *
     * @return
     */
    public Pattern toPattern() {
        StringBuilder builder = new StringBuilder();

        builder.append("(");
        for (String key : this.variables.keySet()) {
            builder.append("\\$");
            builder.append(key);
            builder.append("|");
        }
        if (builder.length() > 1) {
            builder.deleteCharAt(builder.length() - 1);
        }
        builder.append(")");

        return Pattern.compile(builder.toString());
    }

    /**
     *
     * @throws VariableException
     */
    private void save() throws VariableException {
        try (FileOutputStream input = new FileOutputStream(VAR_FILE)) {
            Properties props = new Properties();
            for (Entry<String, String> entry : this.variables.entrySet()) {
                props.put(entry.getKey(), entry.getValue());
            }
            props.store(input, "SQL queries that are referencable");
        } catch (IOException ioe) {
            throw new VariableException("Failed to save variables", ioe);
        }
    }

}
