/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.jmsql.sql.var;

/**
 *
 * @author Michael L.R. Marques
 */
public class VariableException extends Exception {

    public VariableException(String message) {
        super(message);
    }

    public VariableException(Exception exception) {
        super(exception);
    }

    public VariableException(String message, Exception exception) {
        super(message, exception);
    }

}
