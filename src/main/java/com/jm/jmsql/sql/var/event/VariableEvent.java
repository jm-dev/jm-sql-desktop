/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.jmsql.sql.var.event;

import java.util.EventObject;

/**
 *
 * @author Michael L.R. Marques
 */
public class VariableEvent extends EventObject {

    private final String variable;

    /**
     * 
     * @param source
     * @param variable
     */
    public VariableEvent(Object source, String variable) {
        super(source);
        this.variable = variable;
    }

    /**
     * @return the variable
     */
    public String getVariable() {
        return variable;
    }
    
}
