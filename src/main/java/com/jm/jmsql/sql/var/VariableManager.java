/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.jmsql.sql.var;

import com.jm.commons.components.JMDialog;
import com.jm.jmsql.models.VariablesComboBoxModel;
import java.awt.Frame;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Michael L.R. Marques
 */
public class VariableManager extends JMDialog {

    private static final Logger logger = LogManager.getLogger(VariableManager.class);

    /**
     * Creates new form VariableManager
     * @param parent
     */
    public VariableManager(Frame parent) {
        super(parent, true);
        initComponents();

        this.pnlMain.setImage(new File("./images/optionslogo.png"));

        try {
            setIconImage(ImageIO.read(getClass().getResource("/icon.png")));
        } catch (IOException ioe) {
            logger.error(ioe.getMessage(), ioe);
        }

        try {
            this.cbxVariables.setModel(new VariablesComboBoxModel());
        } catch (VariableException ve) {
            JOptionPane.showMessageDialog(this, ve.getMessage(), "Loading Variables", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlMain = new com.jm.commons.components.panel.ImagePanel();
        jButton1 = new javax.swing.JButton();
        btnAdd = new javax.swing.JButton();
        txtVariable = new com.jm.commons.components.textfield.HintJTextField();
        cbxVariables = new javax.swing.JComboBox();
        btnRemove = new javax.swing.JButton();

        setTitle("Variable Manager");
        setResizable(false);

        jButton1.setText("Done");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                done(evt);
            }
        });

        btnAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/add.png"))); // NOI18N
        btnAdd.setOpaque(false);
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                add(evt);
            }
        });

        txtVariable.setHintText("Variable...");

        btnRemove.setIcon(new javax.swing.ImageIcon(getClass().getResource("/clear.png"))); // NOI18N
        btnRemove.setOpaque(false);
        btnRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                remove(evt);
            }
        });

        javax.swing.GroupLayout pnlMainLayout = new javax.swing.GroupLayout(pnlMain);
        pnlMain.setLayout(pnlMainLayout);
        pnlMainLayout.setHorizontalGroup(
            pnlMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlMainLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButton1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(pnlMainLayout.createSequentialGroup()
                        .addGroup(pnlMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(cbxVariables, javax.swing.GroupLayout.Alignment.LEADING, 0, 338, Short.MAX_VALUE)
                            .addComponent(txtVariable, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnRemove, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );

        pnlMainLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnAdd, btnRemove});

        pnlMainLayout.setVerticalGroup(
            pnlMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlMainLayout.createSequentialGroup()
                .addContainerGap(98, Short.MAX_VALUE)
                .addGroup(pnlMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtVariable, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addGap(13, 13, 13)
                .addGroup(pnlMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbxVariables, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnRemove, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(34, 34, 34)
                .addComponent(jButton1)
                .addContainerGap())
        );

        pnlMainLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnAdd, txtVariable});

        pnlMainLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnRemove, cbxVariables});

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlMain, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlMain, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void done(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_done
        setVisible(false);
    }//GEN-LAST:event_done

    private void add(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_add
        if (this.txtVariable.getText() == null ||
                this.txtVariable.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Variable name cannot be empty", "Add Variable", JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        try {
            if (Variables.getInstance().containsVariable(this.txtVariable.getText())) {
                JOptionPane.showMessageDialog(this, "Variable already exists", "Add Variable", JOptionPane.ERROR_MESSAGE);
                return;
            }
            Variables.getInstance().addVariable(this.txtVariable.getText());

            this.cbxVariables.setModel(new VariablesComboBoxModel());
        } catch (VariableException ve) {
            JOptionPane.showMessageDialog(this, ve.getMessage(), "Add Variable", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_add

    private void remove(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_remove
        if (this.cbxVariables.getItemCount() == 0 &&
                this.cbxVariables.getSelectedIndex() < 0) {
            JOptionPane.showMessageDialog(this, "No variable is selected", "Remove Variable", JOptionPane.ERROR_MESSAGE);
            return;
        }

        try {
            Variables.getInstance().deleteVariable((String) this.cbxVariables.getSelectedItem());
        } catch (VariableException ve) {
            JOptionPane.showMessageDialog(this, ve.getMessage(), "Add Variable", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_remove
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnRemove;
    private javax.swing.JComboBox cbxVariables;
    private javax.swing.JButton jButton1;
    private com.jm.commons.components.panel.ImagePanel pnlMain;
    private com.jm.commons.components.textfield.HintJTextField txtVariable;
    // End of variables declaration//GEN-END:variables
}
