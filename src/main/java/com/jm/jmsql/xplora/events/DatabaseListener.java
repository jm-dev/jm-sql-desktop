/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.jmsql.xplora.events;

import java.util.EventListener;

/**
 *
 * @author Michael L.R. Marques
 */
public interface DatabaseListener extends EventListener {

    /**
     *
     * @param e
     */
    public void addedDefinition(DatabaseEvent e);

    /**
     *
     * @param e
     */
    public void deletedDefinition(DatabaseEvent e);

}
