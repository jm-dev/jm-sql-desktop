/*
 * Copyright (C) 2012-2013 Michael L.R. Marques
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Contact: michaellrmarques@gmail.com
 */
package com.jm.jmsql.xplora.events;

import static com.jm.commons.utils.Constants.EMPTY_STRING;
import com.jm.jmsql.xplora.Definition;
import java.util.EventObject;

/**
 *
 * @param <S>
 * @param <X>
 * @created Feb 19, 2013
 * @author Michael L.R. Marques
 */
public class DefinitionEvent<S extends Object, X extends Throwable> extends EventObject {

    private Definition definition;
    private final String sql;
    private final X exception;
    private final long start;
    private final long end;
    private final long spent;
    private final boolean result;
    private boolean cancel;

    /**
     *
     * @param source
     * @param definition
     */
    public DefinitionEvent(S source, Definition definition) {
        this(source, definition, 0);
    }

    /**
     *
     * @param source
     * @param definition
     * @param start
     */
    public DefinitionEvent(S source, Definition definition, long start) {
        this(source, definition, start, start);
    }

    /**
     *
     * @param source
     * @param definition
     * @param sql
     * @param start
     */
    public DefinitionEvent(S source, Definition definition, String sql, long start) {
        this(source, definition, sql, null, start, start, false);
    }

    /**
     *
     * @param source
     * @param definition
     * @param start
     * @param end
     */
    public DefinitionEvent(S source, Definition definition, long start, long end) {
        this(source, definition, EMPTY_STRING, start, end, false);
    }

    /**
     *
     * @param source
     * @param definition
     * @param sql
     * @param start
     * @param end
     * @param result
     */
    public DefinitionEvent(S source, Definition definition, String sql, long start, long end, boolean result) {
        this(source, definition, sql, null, start, end, result);
    }

    /**
     * 
     * @param source
     * @param definition
     * @param exception
     * @param start
     * @param end
     */
    public DefinitionEvent(S source, Definition definition, X exception, long start, long end) {
        this(source, definition, EMPTY_STRING, exception, start, end);
    }

    /**
     * 
     * @param source
     * @param definition
     * @param sql
     * @param exception
     * @param start
     * @param end
     */
    public DefinitionEvent(S source, Definition definition, String sql, X exception, long start, long end) {
        this(source, definition, sql, exception, start, end, false);
    }

    /**
     *
     * @param source
     * @param definition
     * @param sql
     * @param exception
     * @param start
     * @param end
     * @param result
     */
    public DefinitionEvent(S source, Definition definition, String sql, X exception, long start, long end, boolean result) {
        super(source);
        this.definition = definition;
        this.sql = sql;
        this.exception = exception;
        this.start = start;
        this.end = end;
        this.spent = end - start;
        this.result = result;
    }

    /**
     *
     * @return
     */
    @Override
    public S getSource() {
        return (S) super.getSource();
    }

    /**
     *
     * @return
     */
    public Definition getDefinition() {
        return (Definition) this.definition;
    }

    /**
     *
     * @return
     */
    public String getSQLQuery() {
        return this.sql;
    }

    /**
     * @return the exception
     */
    public X getException() {
        return exception;
    }

    /**
     *
     * @return
     */
    public long getStart() {
        return this.start;
    }

    /**
     *
     * @return
     */
    public long getEnd() {
        return this.end;
    }

    /**
     *
     * @return
     */
    public long getSpent() {
        return this.spent;
    }

    /**
     *
     * @return
     */
    public boolean getHasResults() {
        return this.result;
    }

    /**
     *
     * @param cancel
     */
    public void setCancel(boolean cancel) {
        this.cancel = cancel;
    }

    /**
     *
     * @return
     */
    public boolean isCancelled() {
        return this.cancel;
    }
    
}
