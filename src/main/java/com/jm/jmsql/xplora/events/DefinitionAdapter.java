/*
 * Copyright (C) 2012-2013 Michael L.R. Marques
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Contact: michaellrmarques@gmail.com
 */
package com.jm.jmsql.xplora.events;

/**
 *
 * @created Feb 19, 2013
 * @author Michael L.R. Marques
 */
public class DefinitionAdapter implements DefinitionListener {
    
    /**
     * 
     * @param e 
     */
    @Override
    public void connecting(DefinitionEvent e) {
        
    }
    
    /**
     * 
     * @param e 
     */
    @Override
    public void connected(DefinitionEvent e) {
        
    }
    
    /**
     * 
     * @param e 
     */
    @Override
    public void connectionSuccessful(DefinitionEvent e) {
        
    }
    
    /**
     * 
     * @param e 
     */
    @Override
    public void connectionFailed(DefinitionEvent e) {
        
    }
    
    /**
     * 
     * @param e 
     */
    @Override
    public void disconnecting(DefinitionEvent e) {
        
    }
    
    /**
     * 
     * @param e 
     */
    @Override
    public void disconnected(DefinitionEvent e) {
        
    }
    
    /**
     * 
     * @param e 
     */
    @Override
    public void executing(DefinitionEvent e) {
        
    }
    
    /**
     * 
     * @param e 
     */
    @Override
    public void executionSuccessful(DefinitionEvent e) {
        
    }
    
    /**
     * 
     * @param e 
     */
    @Override
    public void executionFailed(DefinitionEvent e) {
        
    }
    
    /**
     * 
     * @param e 
     */
    @Override
    public void executed(DefinitionEvent e) {
        
    }
    
}
