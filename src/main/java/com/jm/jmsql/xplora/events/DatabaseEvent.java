/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.jmsql.xplora.events;

import com.jm.jmsql.xplora.Database;
import java.util.EventObject;

/**
 *
 * @author Michael L.R. Marques
 */
public class DatabaseEvent extends EventObject {

    private final int index;

    /**
     * 
     * @param database
     * @param index
     */
    public DatabaseEvent(Database database, int index) {
        super(database);
        this.index = index;
    }

    /**
     *
     * @return
     */
    public int getIndex() {
        return this.index;
    }

}
