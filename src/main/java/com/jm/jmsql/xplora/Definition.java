/*
 * Copyright (C) 2012-2013 Michael L.R. Marques
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Contact: michaellrmarques@gmail.com
 */
package com.jm.jmsql.xplora;

import com.jm.commons.cryptography.Crypto;
import com.jm.jmsql.xplora.Item.Type;
import static com.jm.jmsql.xplora.Item.Type.Definition;
import com.jm.jmsql.xplora.events.DefinitionEvent;
import com.jm.jmsql.xplora.events.DefinitionListener;
import com.jm.jmsql.xplora.exceptions.LoadDriverException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.SwingWorker;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * @author Michael L.R. Marques
 */
@XmlRootElement(name = "definition")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Definition extends Library<Database, Item> {
    
    private String datasource;
    private String username;
    private String password;
    private boolean autoConnect;
    private boolean library;
    private String defaultLibrary;
    
    private boolean executing;
    private Connection connection;
    private Statement statement;
    private ResultSet resultSet;
    private SQLException exception;
    
    /**
     * 
     */
    Definition() {
        super();
    }
    
    /**
     * 
     * @param database
     * @param name
     * @param datasource
     * @param username
     * @param password 
     * @param autoConnect
     */
    public Definition(Database database, String name, String datasource, String username, String password, boolean autoConnect) {
        super(database, name);
        this.datasource = datasource;
        this.username = username;
        this.password = password;
        this.autoConnect = autoConnect;
    }
    
    /**
     * 
     * @return 
     */
    @Override
    @XmlElement
    public String getName() {
        return super.getName();
    }
    
    /**
     * 
     * @param name 
     */
    @Override
    public void setName(String name) {
        super.setName(name);
    }
    
    /**
     * 
     * @return 
     */
    @XmlElement
    public String getDatasource() {
        return this.datasource;
    }
    
    /**
     * 
     * @param datasource 
     */
    public void setDatasource(String datasource) {
        this.datasource = datasource;
    }
    
    /**
     * 
     * @return 
     */
    @XmlElement
    public String getUsername() {
        return this.username;
    }
    
    /**
     * 
     * @param username 
     */
    public void setUsername(String username) {
        this.username = username;
    }
    
    /**
     * 
     * @return 
     */
    @XmlElement
    @XmlJavaTypeAdapter(PasswordAdapter.class)
    public String getPassword() {
        return this.password;
    }
    
    /**
     * 
     * @param password 
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the autoConnect
     */
    @XmlAttribute(name = "auto-connect")
    public boolean isAutoConnect() {
        return autoConnect;
    }

    /**
     * @param autoConnect the autoConnect to set
     */
    public void setAutoConnect(boolean autoConnect) {
        this.autoConnect = autoConnect;
    }

    /**
     *
     * @return the defaultLibrary
     */
    @XmlAttribute(name = "default")
    public String getDefaultLibrary() {
        return this.defaultLibrary;
    }

    /**
     *
     * @param defaultLibrary
     */
    public void setDefaultLibrary(String defaultLibrary) {
        this.defaultLibrary = defaultLibrary;
    }
    
    /**
     * 
     * @return 
     */
    public boolean isExecuting() {
        return this.executing;
    }

    /**
     *
     * @return
     */
    public boolean isLibrary() {
        return this.library;
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public Item load() {
        if (isLoaded() ||
                isLoading()) {
            return this;
        }
        if (isConnected()) {
            setLoading(true);
            DatabaseMetaData dmd = getDatabaseMetaData();
            try {
                String defaultCatalog = getConnection().getCatalog();
                if (defaultCatalog != null &&
                        !defaultCatalog.isEmpty()) {
                    this.defaultLibrary = defaultCatalog;
                }
                try (ResultSet results = dmd.getCatalogs()) {
                    while (results.next()) {
                        try {
                            add(new Catalog(this, results.getString(1)));
                        } catch (SQLException sqle) {
                            log.error(sqle.getMessage(), sqle);
                        }
                    }
                }
                if (super.isEmpty()) {
                    try {
                        String defaultSchema = getConnection().getSchema();
                        if (defaultSchema != null &&
                                !defaultSchema.isEmpty()) {
                            this.defaultLibrary = defaultSchema;
                        }
                    } catch (AbstractMethodError ame) {
                        ame.printStackTrace();
                    }
                    try (ResultSet results = dmd.getSchemas()) {
                        while (results.next()) {
                            try {
                                add(new Schema(this, results.getString(1)));
                            } catch (SQLException sqle) {
                                log.error(sqle.getMessage(), sqle);
                            }
                        }
                    }
                }
            } catch (SQLException sqle) {
                log.error(sqle.getMessage(), sqle);
            } finally {
                setLoading(false);
            }

            if (super.isEmpty()) {
                this.library = true;
            } else {
                setLoaded(true);
                this.library = false;
            }
        }
        return super.load();
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public Icon getIcon() {
        return new ImageIcon(getClass().getResource("/" + (isConnected() ? "connected" : "disconnected") + "_definition.png"));
    }

    /**
     *
     * @return
     */
    @Override
    public int size() {
        if (isLibrary()) {
            return super.size();
        }
        return this.children.size();
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public boolean isSortable() {
        return isConnected() &&
                    !isLibrary();
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public boolean isEmpty() {
        return isConnected() &&
                    super.isEmpty();
    }

    /**
     * 
     * @return
     * @throws LoadDriverException
     */
    public boolean testConnection() throws LoadDriverException {
        try {
            this.connection = getParent().connect(getDatasource(), getUsername(), getPassword());
            return this.connection != null ||
                        !this.connection.isClosed();
        } catch (SQLException sqle) {
            log.error(sqle.getMessage(), sqle);
            this.exception = sqle;
        }
        return false;
    }
    
    /**
     * 
     * @return 
     * @throws LoadDriverException
     */
    public boolean connect() throws LoadDriverException {
        DefinitionEvent event = fireConnecting();
        try {
            this.connection = getParent().connect(getDatasource(), getUsername(), getPassword());
            if (this.connection != null ||
                    !this.connection.isClosed()) {
                fireConnectionSuccessful(event);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        load();
                    }
                }).start();
            } else {
                fireConnectionFailed(event);
                return false;
            }
        } catch (SQLException sqle) {
            log.error(sqle.getMessage(), sqle);
            this.exception = sqle;
            fireConnectionFailed(event);
            return false;
        } finally {
            fireConnected(event);
        }
        return true;
    }
    
    /**
     * 
     */
    public void asyncConnect() {
        SwingWorker<Boolean, Void> worker = new SwingWorker() {
            @Override
            protected Object doInBackground() throws Exception {
                return connect();
            }
        };
        worker.execute();
    }
    
    /**
     * 
     * @return 
     */
    public boolean disconnect() {
        try {
            DefinitionEvent event = fireDisconnecting();
            if (this.statement != null) {
                this.statement.close();
                this.statement = null;
            }
            if (this.connection != null) {
                this.connection.close();
            }
            clear();
            fireDisconnected(event);
            return true;
        } catch (SQLException sqle) {
            log.error(sqle.getMessage(), sqle);
            this.exception = sqle;
            return false;
        }
    }
    
    /**
     * 
     * @return
     */
    public boolean isConnected() {
        // 
        if (this.connection == null) {
            return false;
        }
        // 
        try {
            return !this.connection.isClosed();
        } catch (SQLException sqle) {
            log.error(sqle.getMessage(), sqle);
            this.exception = sqle;
            return false;
        }
    }
    
    /**
     * 
     * @return 
     */
    public Connection getConnection() {
        return this.connection;
    }
    
    /**
     * 
     * @return 
     */
    public DatabaseMetaData getDatabaseMetaData() {
        try {
            return this.connection.getMetaData();
        } catch (SQLException sqle) {
            log.error(sqle.getMessage(), sqle);
            this.exception = sqle;
            return null;
        }
    }
    
    /**
     * 
     * @param <T>
     * @param source
     * @param sql
     * @return
     * @throws SQLException 
     */

    public <T> boolean execute(T source, String sql) throws SQLException {
        return execute(source, sql, 0);
    }
    
    /**
     * 
     * @param <T>
     * @param source
     * @param limit
     * @return 
     * @param sql
     * @throws java.sql.SQLException
     */
    public <T> boolean execute(T source, String sql, int limit) throws SQLException {
        this.executing = false;
        DefinitionEvent event = fireExecuting(source);
        try {
            this.exception = null;
            try {
                this.statement = this.connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                                                                    ResultSet.CONCUR_UPDATABLE);
            } catch (SQLException sqle) {
                this.statement = this.connection.createStatement();
               log.error(sqle.getMessage(), sqle);
            }
            this.statement.setMaxRows(limit);
            boolean executed = this.statement.execute(sql);
            if (executed) {
                this.resultSet = this.statement.getResultSet();
            }
            return fireExecutionSuccessful(source, event, sql, executed);
        } catch (SQLException sqle) {
            log.error(sqle.getMessage(), sqle);
            this.exception = sqle;
            fireExecutionFailed(source, event, sql);
            throw sqle;
        } finally {
            fireExecuted(source, event, sql);
            this.executing = false;
        }
    }
    
    /**
     * 
     * @throws SQLException 
     */
    public void cancelExecute() throws SQLException {
        this.statement.cancel();
    }
    
    /**
     * 
     * @return 
     */
    public ResultSet getResults() {
        return this.resultSet;
    }
    
    /**
     * 
     * @return 
     */
    public int getRowCount() {
        if (this.resultSet == null) {
            return 0;
        }
        try {
            this.resultSet.last();
            try {
                return this.resultSet.getRow();
            } finally {
                getResults().beforeFirst();
            }
        } catch (SQLException sqle) {
            log.error(sqle.getMessage(), sqle);
            this.exception = sqle;
        }
        try {
            int count = 0;
            try {
                while (this.resultSet.next()) {
                    count++;
                }
            } finally {
                this.resultSet.beforeFirst();
            }
            return count;
        } catch (SQLException sqle) {
            log.error(sqle.getMessage(), sqle);
            this.exception = sqle;
        }
        try {
            return this.resultSet.getFetchSize();
        } catch (SQLException sqle) {
            log.error(sqle.getMessage(), sqle);
            this.exception = sqle;
        }
        return 0;
    }
    
    /**
     * 
     * @return 
     */
    public int getUpdateCount() {
        try {
            return this.statement.getUpdateCount();
        } catch (SQLException sqle) {
            log.error(sqle.getMessage(), sqle);
            this.exception = sqle;
            return 0;
        }
    }
    
    /**
     * 
     * @return 
     */
    public SQLException getException() {
        return this.exception;
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public Type getObjectType() {
        return Definition;
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public int getColumnCount() {
        return isConnected() ? 9 : 8;
    }
    
    /**
     * 
     * @param index
     * @return 
     */
    @Override
    public Class<?> getColumnType(int index) {
        switch(index) {
            case 5:
            case 6: return Boolean.class;
            case 7: return String.class;
            case 8: return Integer.class;
            default: return getColumn(index).getClass();
        }
    }
    
    /**
     * 
     * @param index
     * @return 
     */
    @Override
    public String getColumnName(int index) {
        switch (index) {
            case 0: return "Name";
            case 1: return "Database";
            case 2: return "Datasource";
            case 3: return "Username";
            case 4: return "Password";
            case 5: return "Connected";
            case 6: return "Auto-Connect";
            case 7: return "Default";
            case 8: return isEmpty() ? "Unknown" : get(0).getObjectType().toString();
            default: return null;
        }
    }
    
    /**
     * 
     * @param index
     * @return 
     */
    @Override
    public Object getColumn(int index) {
        switch (index) {
            case 0: return getName();
            case 1: return getParent().getName();
            case 2: return this.datasource;
            case 3: return this.username;
            case 4: return Crypto.encrypt(this.password);
            case 5: return isConnected();
            case 6: return isAutoConnect();
            case 7: return getDefaultLibrary() != null ? getDefaultLibrary() : "None";
            case 8: return size();
            default: return null;
        }
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public int hashCode() {
        return super.hashCode();
    }
    
    /**
     * 
     * @param that
     * @return 
     */
    @Override
    public boolean equals(Object that) {
        return super.equals(that) &&
                    that instanceof Definition &&
                        ((Definition) that).getDatasource().equals(getDatasource()) &&
                            ((Definition) that).getUsername().equals(getUsername()) &&
                                ((Definition) that).getPassword().equals(getPassword()) &&
                                    ((Definition) that).isAutoConnect() == isAutoConnect();
    }
    
    /**
     * 
     * @param o
     * @return 
     */
    @Override
    public int compareTo(Item o) {
        return isConnected() ? (((Definition) o).isConnected() ? 0 : -1) : ((Definition) o).isConnected() ? 1 : super.compareTo(o);
    }
    
    /**
     * 
     * @param index
     * @return 
     */
    @Override
    public boolean isColumnEditable(int index) {
        return false;
    }
    
    /**
     * 
     * @param index
     * @param object 
     */
    @Override
    public void setColumn(int index, Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    /**
     * 
     * @param item
     * @return 
     */
    @Override
    public boolean update(Item item) {
        if (!(item instanceof Definition)) {
            return false;
        }
        
        boolean update = super.update(item);
        
        if (update) {
            Definition newDefinition = (Definition) item;
            setDatasource(newDefinition.getDatasource());
            setUsername(newDefinition.getUsername());
            setPassword(newDefinition.getPassword());
            setAutoConnect(newDefinition.isAutoConnect());
        }
        
        return update;
    }
    
    /**
     * 
     * @param listener 
     */
    public void addDefinitionListener(DefinitionListener listener) {
        this.listeners.add(DefinitionListener.class, listener);
    }
    
    /**
     * 
     * @param listener 
     */
    public void removeDefinitionListener(DefinitionListener listener) {
        this.listeners.remove(DefinitionListener.class, listener);
    }
    
    /**
     * 
     * @return 
     */
    public DefinitionListener[] getDefinitionListeners() {
        return this.listeners.getListeners(DefinitionListener.class);
    }
    
    /**
     * 
     * @return 
     */
    public DefinitionEvent fireConnecting() {
        DefinitionEvent event = new DefinitionEvent(this, this, System.currentTimeMillis());
        for (DefinitionListener listener : getDefinitionListeners()) {
            listener.connecting(event);
        }
        return event;
    }
    
    /**
     * 
     * @param connecting
     */
    public void fireConnected(DefinitionEvent connecting) {
        DefinitionEvent event = new DefinitionEvent(this, this, connecting.getStart(), System.currentTimeMillis());
        for (DefinitionListener listener : getDefinitionListeners()) {
            listener.connected(event);
        }
    }
    
    /**
     * 
     * @param connecting
     */
    public void fireConnectionSuccessful(DefinitionEvent connecting) {
        DefinitionEvent event = new DefinitionEvent(this, this, connecting.getStart(), System.currentTimeMillis());
        for (DefinitionListener listener : getDefinitionListeners()) {
            listener.connectionSuccessful(event);
        }
    }
    
    /**
     * 
     * @param connecting
     */
    public void fireConnectionFailed(DefinitionEvent connecting) {
        DefinitionEvent event = new DefinitionEvent(this, this, getException(), connecting.getStart(), System.currentTimeMillis());
        for (DefinitionListener listener : getDefinitionListeners()) {
            listener.connectionFailed(event);
        }
    }
    
    /**
     * 
     * @param <T>
     * @return 
     */
    public <T> DefinitionEvent fireDisconnecting() {
        DefinitionEvent event = new DefinitionEvent(this, this, System.currentTimeMillis());
        for (DefinitionListener listener : getDefinitionListeners()) {
            listener.disconnecting(event);
        }
        return event;
    }
    
    /**
     * 
     * @param <T>
     * @param disconnecting
     */
    public <T> void fireDisconnected(DefinitionEvent<T, Throwable> disconnecting) {
        DefinitionEvent event = new DefinitionEvent(this, this, disconnecting.getStart(), System.currentTimeMillis());
        for (DefinitionListener listener : getDefinitionListeners()) {
            listener.disconnected(event);
        }
    }
    
    /**
     * 
     * @param <T>
     * @param source
     * @return 
     */
    public <T> DefinitionEvent<T, Throwable> fireExecuting(T source) {
        DefinitionEvent<T, Throwable> event = new DefinitionEvent(source, this, System.currentTimeMillis());
        for (DefinitionListener listener : getDefinitionListeners()) {
            listener.executing(event);
        }
        return event;
    }
    
    /**
     *
     * @param <T>
     * @param source
     * @param executing
     * @param sql
     * @param select
     * @return
     */
    public <T> boolean fireExecutionSuccessful(T source, DefinitionEvent executing, String sql, boolean select) {
        DefinitionEvent event = new DefinitionEvent(source, this, sql, executing.getStart(), System.currentTimeMillis(), select);
        for (DefinitionListener listener : getDefinitionListeners()) {
            listener.executionSuccessful(event);
        }
        return select;
    }
    
    /**
     *
     * @param <T>
     * @param source
     * @param executing
     * @param sql 
     */
    public <T> void fireExecutionFailed(T source, DefinitionEvent executing, String sql) {
        DefinitionEvent event = new DefinitionEvent(source, this, sql, getException(), executing.getStart(), System.currentTimeMillis(), false);
        for (DefinitionListener listener : getDefinitionListeners()) {
            listener.executionFailed(event);
        }
    }

    /**
     *
     * @param <T>
     * @param source
     * @param executing
     * @param sql
     */
    public <T> void fireExecuted(T source, DefinitionEvent executing, String sql) {
        DefinitionEvent event = new DefinitionEvent(source, this, sql, executing.getStart(), System.currentTimeMillis(), false);
        for (DefinitionListener listener : getDefinitionListeners()) {
            listener.executed(event);
        }
    }
    
}

/**
 * 
 * @author Michael L.R. Marques
 */
class PasswordAdapter extends XmlAdapter<String, String> {

    /**
     * @see XmlAdapter
     * @param encryptedPassword
     * @return String
     * @throws Exception
     */
    @Override
    public String unmarshal(String encryptedPassword) throws Exception {
        if (encryptedPassword == null
                || encryptedPassword.isEmpty()) {
            return "";
        }
        return Crypto.decrypt(encryptedPassword);
    }

    /**
     * @see XmlAdapter
     * @param password
     * @return String
     * @throws Exception
     */
    @Override
    public String marshal(String password) throws Exception {
        return Crypto.encrypt(password);
    }

}
