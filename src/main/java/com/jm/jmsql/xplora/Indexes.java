/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.jmsql.xplora;

import static com.jm.jmsql.xplora.Item.Type.Indexes;
import static com.jm.jmsql.xplora.Item.log;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 *
 * @author Marquema
 */
public class Indexes extends Grouping<Index> {

    /**
     * 
     * @param parent
     */
    public Indexes(Library parent) {
        super(parent, "Indexes");
    }

    /**
     * Not needed
     * @return
     */
    @Override
    public Item load() {
        if (isLoaded() ||
                isLoading()) {
            return this;
        }
        setLoading(true);
        if (isEmpty()) {
            DatabaseMetaData dmd = getParent(Definition.class).getDatabaseMetaData();
            try (ResultSet results = dmd.getIndexInfo(getParent() instanceof Catalog ? getParent().getName() : null, getParent() instanceof Schema ? getParent().getName() : null, null, false, true)) {
                while (results.next()) {
                    try {
                        add(new Index(this, results.getString("INDEX_NAME"), results.getString("TABLE_NAME"), results.getString("INDEX_QUALIFIER")));
                    } catch (SQLException sqle) {
                        log.error(sqle.getMessage(), sqle);
                    }
                }
            } catch (SQLException sqle) {
                log.error(sqle.getMessage(), sqle);
            }
        }
        return super.load();
    }

    /**
     *
     * @return
     */
    @Override
    public Icon getIcon() {
        return new ImageIcon(getClass().getResource("/indexes.png"));
    }

    /**
     *
     * @return
     */
    @Override
    public Type getObjectType() {
        return Indexes;
    }

    /**
     *
     * @return
     */
    @Override
    public int getColumnCount() {
        return 2;
    }

    /**
     *
     * @param index
     * @return
     */
    @Override
    public String getColumnName(int index) {
        switch(index) {
            case 0: return "Name";
            case 1: return "Indexes";
            default: return null;
        }
    }

    /**
     *
     * @param index
     * @return
     */
    @Override
    public Object getColumn(int index) {
        switch(index) {
            case 0: return getName();
            case 1: return size();
            default: return null;
        }
    }

    /**
     *
     * @param index
     * @return
     */
    @Override
    public boolean isColumnEditable(int index) {
        return false;
    }

    /**
     *
     * @param index
     * @param object
     */
    @Override
    public void setColumn(int index, Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
