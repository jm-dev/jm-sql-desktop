/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jm.jmsql.xplora;

import static com.jm.jmsql.xplora.Item.Type.Grouping;
import java.awt.Color;

/**
 *
 * @author Marquema
 * @param <C>
 */
public class Grouping<C extends Item> extends Item<Library, C> {
    
    /**
     * 
     * @param parent
     * @param name 
     */
    public Grouping(Library parent, String name) {
        super(parent, name);
    }

    /**
     *
     * @return
     */
    @Override
    public String getText() {
        if (isLoading()) {
            return this.name + " (Loading...)";
        }
        return super.getText();
    }

    /**
     * 
     * @return
     */
    @Override
    public Color getForeground() {
        if (isLoading()) {
            return Color.gray;
        }
        return super.getForeground();
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public Type getObjectType() {
        return Grouping;
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public int getColumnCount() {
        return 2;
    }
    
    /**
     * 
     * @param index
     * @return 
     */
    @Override
    public String getColumnName(int index) {
        switch (index) {
            case 0: return "Name";
            case 1: return getObjectType().toString();
            default: return null;
        }
    }
    
    /**
     * 
     * @param index
     * @return 
     */
    @Override
    public Object getColumn(int index) {
        switch (index) {
            case 0: return getName();
            case 1: return size();
            default: return null;
        }
    }
    
    /**
     * 
     * @param index
     * @return 
     */
    @Override
    public boolean isColumnEditable(int index) {
        return false;
    }
    
    /**
     * 
     * @param index
     * @param object 
     */
    @Override
    public void setColumn(int index, Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
