/*
 * Copyright (C) 2012-2013 Michael L.R. Marques
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Contact: michaellrmarques@gmail.com
 */
package com.jm.jmsql.xplora;

import com.jm.commons.utils.Constants;
import static com.jm.commons.utils.Constants.NEW_LINE;
import static com.jm.commons.utils.Constants.SPACE;
import static com.jm.commons.utils.Constants.TAB;
import static com.jm.jmsql.xplora.Database.DEFAULT_LIBRARY_DELIMETER;
import static com.jm.jmsql.xplora.Item.Type.Table;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.regex.Pattern;
import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 *
 * @author Michael L.R. Marques
 */
public class Table extends Item<Tables, Column> implements Cloneable {
    
    // The Table Description
    private String description;
    
    /**
     * 
     * @param name 
     */
    public Table(String name) {
        this(name, null);
    }
    
    /**
     * 
     * @param name
     * @param description 
     */
    public Table(String name, String description) {
        this(null, name, description);
    }
    
    /**
     * 
     * @param parent 
     */
    public Table(Tables parent)  {
        this(parent, "");
    }
    
    /**
     * 
     * @param parent
     * @param name 
     */
    public Table(Tables parent, String name) {
        this(parent, name, "", new ArrayList(0));
    }
    
    /**
     * 
     * @param parent
     * @param name
     * @param description 
     */
    public Table(Tables parent, String name, String description) {
        this(parent, name, description, new ArrayList(0));
    }
    
    /**
     * 
     * @param parent
     * @param name 
     * @param children 
     */
    public Table(Tables parent, String name, List<Column> children) {
        this(parent, name, "", children);
    }
    
    /**
     * 
     * @param parent
     * @param name
     * @param description
     * @param children 
     */
    public Table(Tables parent, String name, String description, List<Column> children) {
        super(parent, name);
        this.description = description;
        addAll(children);
    }

    /**
     *
     * @return
     */
    public String getDescription() {
        return this.description;
    }

    /**
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public Item load() {
        if (isLoaded() ||
                isLoading()) {
            return this;
        }
        setLoading(true);
        if (isEmpty()) {
            Item object = getParent().getParent();
            DatabaseMetaData dmd = ((Definition) (object instanceof Definition ? object : object.getParent())).getDatabaseMetaData();
            try {
                try (ResultSet results = dmd.getColumns(object instanceof Catalog ? object.getName() : null, object instanceof Schema ? object.getName() : null, getName(), "%")) {
                    int column = 0;
                    int columns = results.getMetaData().getColumnCount();
                    while (results.next()) {
                        try {
                            if (columns >= 12 &&
                                    columns <= 17) {
                                add(new Column<>(this, results.getString(4), results.getInt(5), results.getString(6), results.getInt(7), results.getInt(8), results.getString(12), ++column));
                            } else if (columns >= 18 &&
                                            columns <= 22) {
                                add(new Column<>(this, results.getString(4), results.getInt(5), results.getString(6), results.getInt(7), results.getInt(8), results.getString(12), ++column, results.getString(18).equals("YES")));
                            } else if (columns == 23) {
                                add(new Column<>(this, results.getString(4), results.getInt(5), results.getString(6), results.getInt(7), results.getInt(8), results.getString(12), ++column, results.getString(18).equals("YES"), results.getString(23).equals("YES")));
                            } else {
                                add(new Column<>(this, results.getString(4), results.getInt(5), results.getString(6), results.getInt(7), results.getInt(8), results.getString(12), ++column, results.getString(18).equals("YES"), results.getString(23).equals("YES"), results.getString(24).equals("YES")));
                            }
                        } catch (SQLException sqle) {
                            log.error(sqle.getMessage(), sqle);
                        }
                    }
                }
            } catch (SQLException sqle) {
                log.error(sqle.getMessage(), sqle);
            }
        }
        return super.load();
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public Icon getIcon() {
        return new ImageIcon(getClass().getResource("/table.png"));
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public Type getObjectType() {
        return Table;
    }
    
    /**
     * 
     * @return 
     */
    public String selectSQLQuery() {
        if (!isLoaded()) {
            load();
        }
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT ");
        // Add the columns
        for (Column column : this) {
            sql.append(column.getName());
            // If the is the final column, exclude the comma
            if (column.getIndex() <= size() - 1) {
                sql.append(", ");
            }
        }
        sql.append(Constants.NEW_LINE);
        sql.append("FROM ");
        Library library = getParent(Library.class);
        if (!(library instanceof Definition)) {
            sql.append(library.getName());
            String delimeter = getParent(Database.class).getLibraryDelimeter();
            if (delimeter == null ||
                    delimeter.isEmpty()) {
                sql.append(DEFAULT_LIBRARY_DELIMETER);
            } else {
                sql.append(getParent(Database.class).getLibraryDelimeter());
            }
        }
        sql.append(getName());
        return sql.toString();
    }
    
    /**
     * 
     * @return 
     */
    public String insertSQLQuery() {
        if (!isLoaded()) {
            load();
        }
        StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO ");
        Library library = getParent(Library.class);
        if (!(library instanceof Definition)) {
            sql.append(library.getName());
            String delimeter = getParent(Database.class).getLibraryDelimeter();
            if (delimeter == null ||
                    delimeter.isEmpty()) {
                sql.append(DEFAULT_LIBRARY_DELIMETER);
            } else {
                sql.append(getParent(Database.class).getLibraryDelimeter());
            }
        }
        sql.append(getName());
        sql.append(" (");
        // Add the columns
        for (Column column : this) {
            // Add the columns name
            sql.append(column.getName());
            // If the is the final column, exclude the comma
            if (column.getIndex() <= size() - 1) {
                sql.append(", ");
            }
        }
        sql.append(")");
        sql.append(NEW_LINE);
        sql.append("VALUES (");
        // Add the columns
        for (Column column : this) {
            // Set a value with basic type functionality
            if (Number.class.isAssignableFrom(column.getType().getType())) {
                sql.append("0");
            } else if (Date.class.isAssignableFrom(column.getType().getType())) {
                sql.append((new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.sss")).format(GregorianCalendar.getInstance().getTime()));
            } else if (String.class.isAssignableFrom(column.getType().getType())) {
                sql.append("''");
            } else {
                sql.append("null");
            }
            // If the is the final column, exclude the comma
            if (column.getIndex() <= size() - 1) {
                sql.append(", ");
            }
        }
        sql.append(")");
        return sql.toString();
    }
    
    /**
     * 
     * @return 
     */
    public String updateSQLQuery() {
        if (!isLoaded()) {
            load();
        }
        StringBuilder sql = new StringBuilder();
        sql.append("UPDATE ");
        Library library = getParent(Library.class);
        if (!(library instanceof Definition)) {
            sql.append(library.getName());
            String delimeter = getParent(Database.class).getLibraryDelimeter();
            if (delimeter == null ||
                    delimeter.isEmpty()) {
                sql.append(DEFAULT_LIBRARY_DELIMETER);
            } else {
                sql.append(getParent(Database.class).getLibraryDelimeter());
            }
        }
        sql.append(getName());
        return sql.toString();
    }
    
    /**
     * 
     * @return 
     */
    public String deleteSQLQuery() {
        if (!isLoaded()) {
            load();
        }
        StringBuilder sql = new StringBuilder();
        sql.append("DELETE * FROM ");
       Library library = getParent(Library.class);
        if (!(library instanceof Definition)) {
            sql.append(library.getName());
            String delimeter = getParent(Database.class).getLibraryDelimeter();
            if (delimeter == null ||
                    delimeter.isEmpty()) {
                sql.append(DEFAULT_LIBRARY_DELIMETER);
            } else {
                sql.append(getParent(Database.class).getLibraryDelimeter());
            }
        }
        sql.append(getName());
        return sql.toString();
    }
    
    /**
     * 
     * @return 
     */
    public String dropSQLQuery() {
        StringBuilder sql = new StringBuilder();
        sql.append("DROP TABLE ");
        Library library = getParent(Library.class);
        if (!(library instanceof Definition)) {
            sql.append(library.getName());
            String delimeter = getParent(Database.class).getLibraryDelimeter();
            if (delimeter == null ||
                    delimeter.isEmpty()) {
                sql.append(DEFAULT_LIBRARY_DELIMETER);
            } else {
                sql.append(getParent(Database.class).getLibraryDelimeter());
            }
        }
        sql.append(getName());
        return sql.toString();
    }
    
    /**
     * 
     * @return 
     */
    public String createSQLQuery() {
        // Return a blank string if no parent exists
        if (!hasParent()) {
            return "";
        }
        if (!isLoaded()) {
            load();
        }
        // Build the SQL structure string
        StringBuilder sql = new StringBuilder();
        sql.append("CREATE TABLE");
        sql.append(SPACE);
        Library library = getParent(Library.class);
        if (!(library instanceof Definition)) {
            sql.append(library.getName());
            String delimeter = getParent(Database.class).getLibraryDelimeter();
            if (delimeter == null ||
                    delimeter.isEmpty()) {
                sql.append(DEFAULT_LIBRARY_DELIMETER);
            } else {
                sql.append(getParent(Database.class).getLibraryDelimeter());
            }
        }
        sql.append(getName());
        sql.append(" (");
        sql.append(NEW_LINE);
        // Add the columns
        for (Column column : this) {
            sql.append(TAB);
            sql.append(column.getName());
            sql.append(SPACE);
            sql.append(column.getTypeName());
            if (!Date.class.isAssignableFrom(column.getType().getType())) {
                if (column.getLength() > 0) {
                    sql.append("(");
                    sql.append(column.getLength());
                    if (!Integer.class.equals(column.getType().getType()) &&
                            Number.class.isAssignableFrom(column.getType().getType())) {
                        sql.append(",");
                        sql.append(column.getDecimals());
                    }
                    sql.append(")");
                }
            }
            // Check if the column is a primary key
            if (column.isPrimaryKey()) {
                sql.append(" PRIMARY KEY");
            }
            // Check if the column is nullable
            if (!column.isNullable()) {
                sql.append(" NOT NULL");
            }
            // Check if the column is auto generated
            if (column.isAutoGenerated()) {
                sql.append(" AUTO");
            }
            // If the is the final column, exclude the comma
            if (column.getIndex() <= size() - 1) {
                sql.append(",");
                sql.append(NEW_LINE);
            }
        }
        sql.append(NEW_LINE);
        sql.append(")");
        return sql.toString();
    }
    
    /**
     * 
     * @return 
     */
    @Override 
    public int getColumnCount() {
        return 3;
    }
    
    /**
     * 
     * @param index
     * @return 
     */
    @Override 
    public String getColumnName(int index) {
        switch (index) {
            case 0: return "Name";
            case 1: return "Columns";
            case 2: return "Description";
            default: return null;
        }
    }
    
    /**
     * 
     * @param index
     * @return 
     */
    @Override 
    public Object getColumn(int index) {
        switch (index) {
            case 0: return getName();
            case 1: return size();
            case 2: return this.description == null ? "" : this.description;
            default: return null;
        }
    }
    
    /**
     * 
     * @param index
     * @return 
     */
    @Override
    public boolean isColumnEditable(int index) {
        switch (index) {
            case 0:;
            case 2: return true;
            default: return false;
        }
    }
    
    /**
     * 
     * @param index
     * @param object 
     */
    @Override
    public void setColumn(int index, Object object) {
        if (isInEditMode()) {
            switch (index) {
                case 0: {
                    if (Pattern.matches("[A-Za-z0-9_@]+", object.toString())) {
                        setName(object.toString());
                    }
                    break;
                } case 2: this.description = object.toString();
            }
        }
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public Object clone() {
        return new Table(getParent(), getName(), this.description, this);
    }
    
}
