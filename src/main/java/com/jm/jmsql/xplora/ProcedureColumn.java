/*
 * Copyright (C) 2012-2013 Michael L.R. Marques
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Contact: michaellrmarques@gmail.com
 */
package com.jm.jmsql.xplora;

import com.jm.jmsql.jdbc.JDBCType;
import com.jm.jmsql.jdbc.ProcedureColumnType;

/**
 *
 * @created Dec 13, 2012
 * @author Michael L.R. Marques
 */
public class ProcedureColumn extends Item<Procedure, Item> {
    
    // Procedure column type
    private ProcedureColumnType columnType;
    // Java class type
    private JDBCType jdbcType;
    // The name of the SQL type
    private String typeName;
    // The length of the column
    private int length;
    // The decimal positions of the column (if numerical)
    private int decimals;
    // The remark/comments/description of the column
    private String description;
    // The ordinal position of the column in the table
    private int index;
    
    /**
     * 
     * @param parent
     * @param name
     * @param columnType
     * @param jdbcType
     * @param typeName
     * @param length
     * @param decimals
     * @param description
     * @param index 
     */
    public ProcedureColumn(Procedure parent, String name, int columnType, int jdbcType, String typeName, int length, int decimals, String description, int index) {
        this(parent, name, ProcedureColumnType.get(columnType), JDBCType.get(jdbcType), typeName, length, decimals, description, index);
    }
    
    /**
     * 
     * @param parent
     * @param name
     * @param columnType
     * @param jdbcType
     * @param typeName
     * @param length
     * @param decimals
     * @param description
     * @param index 
     */
    public ProcedureColumn(Procedure parent, String name, ProcedureColumnType columnType, JDBCType jdbcType, String typeName, int length, int decimals, String description, int index) {
        super(parent, name == null || name.isEmpty() ? columnType.getName() + " " + index : name);
        this.columnType = columnType;
        this.jdbcType = jdbcType;
        this.typeName = typeName;
        this.length = length;
        this.decimals = decimals;
        this.description = description;
        this.index = index;
    }

    /**
     * @return the columnType
     */
    public ProcedureColumnType getColumnType() {
        return columnType;
    }

    /**
     * @param columnType the columnType to set
     */
    public void setColumnType(ProcedureColumnType columnType) {
        this.columnType = columnType;
    }

    /**
     * @return the jdbcType
     */
    public JDBCType getJdbcType() {
        return jdbcType;
    }

    /**
     * @param jdbcType the jdbcType to set
     */
    public void setJdbcType(JDBCType jdbcType) {
        this.jdbcType = jdbcType;
    }

    /**
     * @return the typeName
     */
    public String getTypeName() {
        return typeName;
    }

    /**
     * @param typeName the typeName to set
     */
    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    /**
     * @return the length
     */
    public int getLength() {
        return length;
    }

    /**
     * @param length the length to set
     */
    public void setLength(int length) {
        this.length = length;
    }

    /**
     * @return the decimals
     */
    public int getDecimals() {
        return decimals;
    }

    /**
     * @param decimals the decimals to set
     */
    public void setDecimals(int decimals) {
        this.decimals = decimals;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the index
     */
    public int getIndex() {
        return index;
    }

    /**
     * @param index the index to set
     */
    public void setIndex(int index) {
        this.index = index;
    }

    /**
     *
     * @return
     */
    @Override
    public boolean isLeaf() {
        return true;
    }
    
    /**
     * 
     * @return 
     */
    @Override 
    public Type getObjectType() {
        return Type.ProcedureColumn;
    }
    
    /**
     * 
     * @return 
     */
    @Override 
    public int getColumnCount() {
        return 7;
    }
    
    /**
     * 
     * @param index
     * @return 
     */
    @Override 
    public String getColumnName(int index) {
        switch (index) {
            case 0: return "Index";
            case 1: return "Name";
            case 2: return "Column Type";
            case 3: return "Type";
            case 4: return "Length";
            case 5: return "Decimals";
            case 6: return "Description";
            default: return null;
        }
    }
    
    /**
     * 
     * @param index
     * @return 
     */
    @Override 
    public Object getColumn(int index) {
        switch (index) {
            case 0: return this.index;
            case 1: return getName();
            case 2: return this.columnType.getName();
            case 3: return this.typeName;
            case 4: return this.length;
            case 5: return this.decimals;
            case 6: return this.description == null ? "" : this.description;
            default: return null;
        }
    }
    
    /**
     * 
     * @param index
     * @return Class<?>
     */
    public Class<?> getColumnType(int index) {
        switch (index) {
            case 0: 
            case 4: 
            case 5: return Integer.class;
            default: return String.class;
        }
    }
    
    /**
     * 
     * @param index
     * @return boolean
     */
    @Override
    public boolean isColumnEditable(int index) {
        return false;
    }
    
    /**
     * 
     * @param index
     * @param object 
     */
    @Override
    public void setColumn(int index, Object object) {
        if (isInEditMode()) {
            switch (index) {
                
            }
        }
    }

}
