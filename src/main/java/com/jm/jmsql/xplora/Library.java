/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jm.jmsql.xplora;

import com.jm.jmsql.xplora.Item.Type;
import static com.jm.jmsql.xplora.Item.Type.Library;
import java.awt.Font;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 *
 * @author marquesm
 * @param <P>
 * @param <C>
 */
@XmlAccessorType(XmlAccessType.NONE)
public class Library<P extends Item, C extends Item> extends Item<P, C> {

    private static final Font DEFAULT_LIBRARY_FONT = new Font("Arial", Font.BOLD, 11);
    
    /**
     * 
     */
    public Library() {
        super();
    }

    /**
     * 
     * @param parent
     */
    public Library(P parent) {
        this(parent, null);
    }
    
    /**
     * 
     * @param parent
     * @param name
     */
    public Library(P parent, String name) {
        super(parent, name);
    }

    /**
     * 
     * @return
     */
    public boolean isDefaultLibrary() {
        if (getParent() instanceof Definition) {
            if (((Definition) getParent()).getDefaultLibrary() != null &&
                    ((Definition) getParent()).getDefaultLibrary().equals(getName())) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * 
     * @return
     */
    @Override
    public Font getFont() {
        if (isDefaultLibrary()) {
            return DEFAULT_LIBRARY_FONT;
        }
        return super.getFont();
    }

    /**
     * 
     * @return
     */
    @Override
    public Item load() {
        if (isLoaded() ||
                isLoading()) {
            return this;
        }
        setLoading(true);
        if (isEmpty()) {
            Tables tables = new Tables(this);
            Procedures procedures = new Procedures(this);
            Views views = new Views(this);
            Indexes indexes = new Indexes(this);
            
            add((C) tables);
            add((C) procedures);
            add((C) views);
            add((C) indexes);
        }
        return super.load();
    }
    
    /**
     * 
     * @return 
     */
    public Tables getTables() {
        Tables tables = get(Tables.class);
        tables.load();
        return tables;
    }
    
    /**
     * 
     * @return 
     */
    public Procedures getProcedures() {
        Procedures procedures = get(Procedures.class);
        procedures.load();
        return procedures;
    }
    
    /**
     * 
     * @return 
     */
    public Views getViews() {
        Views views = get(Views.class);
        views.load();
        return views;
    }

    /**
     * 
     * @return
     */
    public Indexes getIndexes() {
        Indexes indexes = get(Indexes.class);
        indexes.load();
        return indexes;
    }

    /**
     *
     * @return
     */
    @Override
    public int size() {
        return 4;
    }

    /**
     *
     * @return
     */
    @Override
    public boolean isSortable() {
        return false;
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public Type getObjectType() {
        return Library;
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public int getColumnCount() {
        return 5;
    }
    
    /**
     * 
     * @param index
     * @return 
     */
    @Override
    public Class<?> getColumnType(int index) {
        switch (index) {
            case 1:
            case 2:
            case 3:
            case 4: return Integer.class;
            default: return super.getColumnType(index);
        }
    }
    
    /**
     * 
     * @param index
     * @return 
     */
    @Override
    public String getColumnName(int index) {
        switch (index) {
            case 1: return "Tables";
            case 2: return "Procedures";
            case 3: return "Views";
            case 4: return "Indexes";
            default: return "Name";
        }
    }
    
    /**
     * 
     * @param index
     * @return 
     */
    @Override
    public Object getColumn(int index) {
        if (!isLoaded()) {
            load();
        }
        switch (index) {
            case 1: return getTables().size();
            case 2: return getProcedures().size();
            case 3: return getViews().size();
            case 4: return getIndexes().size();
            default: return getName();
        }
    }
    
    /**
     * 
     * @param index
     * @return 
     */
    @Override
    public boolean isColumnEditable(int index) {
        return false;
    }
    
    /**
     * 
     * @param index
     * @param object 
     */
    @Override
    public void setColumn(int index, Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
