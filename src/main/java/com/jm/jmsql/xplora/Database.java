/*
 * Copyright (C) 2012-2013 Michael L.R. Marques
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Contact: michaellrmarques@gmail.com
 */
package com.jm.jmsql.xplora;

import static com.jm.jmsql.xplora.Item.Type.Database;
import com.jm.jmsql.xplora.events.DatabaseEvent;
import com.jm.jmsql.xplora.events.DatabaseListener;
import com.jm.jmsql.xplora.exceptions.LoadDriverException;
import java.awt.Color;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverPropertyInfo;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.List;
import java.util.Properties;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @created Nov 22, 2012
 * @author Michael
 */
@XmlRootElement(name = "database")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Database extends Item<Databases, Definition> implements Driver {
    
    public static final String DEFAULT_LIBRARY_DELIMETER = ".";
    
    private Driver driver;
    private String className;
    private File library;
    private String libraryDelimeter;
    private boolean uppercasePreferred;
    
    /**
     * 
     */
    Database() {
        super();
    }
    
    /**
     * 
     * @param name
     * @param clazz 
     */
    public Database(String name, Class<?> clazz) {
        this(name, clazz.getSimpleName().substring(0, clazz.getSimpleName().lastIndexOf(".class")));
    }
    
    /**
     * 
     * @param name
     * @param className 
     */
    public Database(String name, String className) {
        this(name, className, (File) null, DEFAULT_LIBRARY_DELIMETER, false);
    }
    
    /**
     * 
     * @param name
     * @param clazz
     * @param library
     * @param libraryDelimeter
     * @param uppercasePreferred 
     */
    public Database(String name, Class<?> clazz, String library, String libraryDelimeter, boolean uppercasePreferred) {
        this(name, clazz.getSimpleName().substring(0, clazz.getSimpleName().lastIndexOf(".class")), library, libraryDelimeter, uppercasePreferred);
    }
    
    /**
     * 
     * @param name
     * @param className
     * @param library
     * @param libraryDelimeter
     * @param uppercasePreferred 
     */
    public Database(String name, String className, String library, String libraryDelimeter, boolean uppercasePreferred) {
        this(name, className, new File(library), libraryDelimeter, uppercasePreferred);
    }
    
    /**
     * 
     * @param name
     * @param clazz
     * @param library
     * @param libraryDelimeter
     * @param uppercasePreferred 
     */
    public Database(String name, Class<?> clazz, File library, String libraryDelimeter, boolean uppercasePreferred) {
        this(name, clazz.getSimpleName().substring(0, clazz.getSimpleName().lastIndexOf(".class")), library, libraryDelimeter, uppercasePreferred);
    }
    
    /**
     * 
     * @param name
     * @param className
     * @param library
     * @param libraryDelimeter
     * @param uppercasePreferred 
     */
    public Database(String name, String className, File library, String libraryDelimeter, boolean uppercasePreferred) {
        super(name);
        this.className = className;
        this.library = library;
        this.libraryDelimeter = libraryDelimeter;
        this.uppercasePreferred = uppercasePreferred;
        load();
    }
    
    /**
     * 
     * @return 
     */
    @Override
    @XmlElement
    public String getName() {
        return super.getName();
    }
    
    /**
     * 
     * @param name 
     */
    @Override
    public void setName(String name) {
        super.setName(name);
    }
    
    /**
     * 
     * @return 
     */
    @XmlElement(name = "definition")
    public List<Definition> getDefinitions() {
        return this;
    }
    
    /**
     * 
     * @return 
     */
    @XmlElement(name = "class")
    public String getClassName() {
        return this.className;
    }
    
    /**
     * 
     * @param className 
     */
    public void setClassName(String className) {
        this.className = className;
    }
    
    /**
     * 
     * @return 
     */
    @XmlElement
    public File getLibrary() {
        return this.library;
    }
    
    /**
     * 
     * @param library
     */
    public void setLibrary(String library) {
        setLibrary(new File(library));
    }
    
    /**
     * 
     * @param library 
     */
    public void setLibrary(File library) {
        this.library = library;
    }
    
    /**
     * 
     * @return 
     */
    @XmlAttribute(name = "library-delimeter")
    public String getLibraryDelimeter() {
        return this.libraryDelimeter;
    }
    
    /**
     * 
     * @param libraryDelimeter 
     */
    public void setLibraryDelimeter(String libraryDelimeter) {
        this.libraryDelimeter = libraryDelimeter;
    }
    
    /**
     * 
     * @return 
     */
    @XmlAttribute(name = "uppercase-preferred")
    public boolean isUppercasePreferred() {
        return this.uppercasePreferred;
    }
    
    /**
     * 
     * @param uppercasePreferred 
     */
    public void setUppercasePreferred(boolean uppercasePreferred) {
        this.uppercasePreferred = uppercasePreferred;
    }
    
    /**
     * 
     * @return 
     * @throws com.jm.jmsql.xplora.exceptions.LoadDriverException 
     */
    public Driver getDriver() throws LoadDriverException {
        if (isSystemDatabase() &&
                this.driver == null) {
            try {
                this.driver = (Driver) Class.forName(this.className).newInstance();
            } catch (ClassNotFoundException cnfe) {
                log.error(cnfe.getMessage(), cnfe);
                throw new LoadDriverException(this.className + " is no longer a supported driver", cnfe, this.library);
            } catch (InstantiationException | IllegalAccessException e) {
                log.error(e.getMessage(), e);
                throw new LoadDriverException(e.getMessage(), e, this.library);
            }
        }
        
        if (isExternalDatabase()) {
            if (!isDriverLoaded() &&
                    !isLibraryAvailable()) {
                log.error(this.library.getAbsolutePath() + " cannot be found");
                throw new LoadDriverException(this.library.getAbsolutePath() + " cannot be found", this.library);
            } else {
                if (this.driver == null) {
                    try {
                        this.driver = (Driver) new URLClassLoader(new URL[] { new URL("jar:file:" + this.library.getAbsolutePath() + "!/")} ).loadClass(this.className).newInstance();
                    } catch (MalformedURLException | ClassNotFoundException | IllegalAccessException | InstantiationException e) {
                        log.error(e.getMessage(), e);
                        throw new LoadDriverException(e.getMessage(), e, this.library);
                    }
                }
            }
        }
        
        return this.driver;
    }
    
    /**
     * 
     * @return 
     */
    public boolean isDriverLoaded() {
        return isSystemDatabase() ||
                    (isExternalDatabase() &&
                        this.driver != null);
    }
    
    /**
     * 
     * @return 
     */
    public boolean isLibraryAvailable() {
        return isExternalDatabase() &&
                    this.library.exists();
    }
    
    /**
     * 
     * @return 
     */
    public boolean isSystemDatabase() {
        return this.library == null;
    }
    
    /**
     * 
     * @return 
     */
    public boolean isExternalDatabase() {
        return this.library != null;
    }
    
    /**
     * 
     * @param url
     * @param username
     * @param password
     * @return 
     * @throws java.sql.SQLException
     * @throws LoadDriverException 
     */
    public Connection connect(String url, String username, String password) throws SQLException, LoadDriverException {
        Properties properties = new Properties();
        properties.put("user", username);
        properties.put("password", password);
        return connect(url, properties);
    }
    
    /**
     * 
     * @param url
     * @param info
     * @return
     * @throws SQLException
     */
    @Override
    public Connection connect(String url, Properties info) throws SQLException {
        try {
            return getDriver().connect(url, info);
        } catch (LoadDriverException lde) {
            log.error(lde.getMessage(), lde);
            throw new SQLException("Could not load driver", lde);
        }
    }
    
    /**
     * 
     * @param url
     * @return
     * @throws SQLException 
     */
    @Override
    public boolean acceptsURL(String url) throws SQLException {
        try {
            return getDriver().acceptsURL(url);
        } catch (LoadDriverException lde) {
            log.error(lde.getMessage(), lde);
            throw new SQLException("Could not load driver", lde);
        }
    }
    
    /**
     * 
     * @param url
     * @param info
     * @return
     * @throws SQLException 
     */
    @Override
    public DriverPropertyInfo[] getPropertyInfo(String url, Properties info) throws SQLException {
        try {
            return getDriver().getPropertyInfo(url, info);
        } catch (LoadDriverException lde) {
            log.error(lde.getMessage(), lde);
            throw new SQLException("Could not load driver", lde);
        }
    }
    
    /**
     * 
     * @return 
     */
    @Override 
    public int getMajorVersion() {
        try {
            return getDriver().getMajorVersion();
        } catch (LoadDriverException lde) {
            log.error(lde.getMessage(), lde);
            return 0;
        }
    }
    
    /**
     * 
     * @return 
     */
    @Override 
    public int getMinorVersion() {
        try {
            return getDriver().getMinorVersion();
        } catch (LoadDriverException lde) {
            log.error(lde.getMessage(), lde);
            return 0;
        }
    }
    
    /**
     * 
     * @return 
     */
    @Override 
    public boolean jdbcCompliant() {
        try {
            return getDriver().jdbcCompliant();
        } catch (LoadDriverException lde) {
            log.error(lde.getMessage(), lde);
            return false;
        }
    }
    
    /**
     * 
     * @return
     * @throws SQLFeatureNotSupportedException 
     */
    @Override
    public java.util.logging.Logger getParentLogger() throws SQLFeatureNotSupportedException {
        try {
            return getDriver().getParentLogger();
        } catch (LoadDriverException lde) {
            throw new SQLFeatureNotSupportedException("Could not load driver", lde);
        }
    }

    /**
     *
     * @return
     */
    @Override
    public Color getForeground() {
        try {
            getDriver();
        } catch (LoadDriverException lde) {
            return Color.red;
        }
        return super.getForeground();
    }

    /**
     * 
     * @return
     */
    @Override
    public String getToolTipText() {
        try {
            getDriver();
        } catch (LoadDriverException lde) {
            return lde.getMessage();
        }
        return super.getToolTipText();
    }

    /**
     * 
     * @return 
     */
    @Override 
    public Type getObjectType() {
        return Database;
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public String toString() {
        return getName() + " - " + getClassName();
    }

    /**
     * 
     * @return 
     */
    @Override 
    public int getColumnCount() {
        return 11;
    }
    
    /**
     * 
     * @param index
     * @return 
     */
    @Override
    public Class<?> getColumnType(int index) {
        switch(index) {
            case 5:
            case 6: return Integer.class;
            case 4:
            case 7:
            case 8:
            case 9:
            case 10: return Boolean.class;
            default: return String.class;
        }
    }
    
    /**
     * 
     * @param index
     * @return 
     */
    @Override 
    public String getColumnName(int index) {
        switch(index) {
            case 0: return "Name";
            case 1: return "Driver";
            case 2: return "Library";
            case 3: return "Library Delimeter";
            case 4: return "Uppercase-Preferred";
            case 5: return "Major Version";
            case 6: return "Minor Version";
            case 7: return "JDBC Compliant";
            case 8: return "System Database";
            case 9: return "External Database";
            case 10: return "Available";
            default: return null;
        }
    }
    
    /**
     * 
     * @param index
     * @return 
     */
    @Override 
    public Object getColumn(int index) {
        switch(index) {
            case 0: return getName();
            case 1: return getClassName();
            case 2: return isSystemDatabase() ? "None" : this.library.getAbsolutePath();
            case 3: return getLibraryDelimeter();
            case 4: return isUppercasePreferred();
            case 5: return isDriverLoaded() ? getMajorVersion() : 0;
            case 6: return isDriverLoaded() ? getMinorVersion() : 0;
            case 7: return isDriverLoaded() &&
                                jdbcCompliant();
            case 8: return isSystemDatabase();
            case 9: return isExternalDatabase();
            case 10: return isDriverLoaded();
            default: return null;
        }
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public int hashCode() {
        return super.hashCode();
    }
    
    /**
     * 
     * @param that
     * @return 
     */
    @Override
    public boolean equals(Object that) {
        if (that instanceof Database) {
            if (super.equals(that)) {
                com.jm.jmsql.xplora.Database database = (com.jm.jmsql.xplora.Database) that;
                if (!database.isSystemDatabase() && !isSystemDatabase()) {
                    if (!database.getLibrary().equals(getLibrary())) {
                        return false;
                    }
                }
                
                if (database.getLibraryDelimeter() != null &&
                        getLibraryDelimeter() != null) {
                    if (!database.getLibraryDelimeter().equals(getLibraryDelimeter())) {
                        return false;
                    }
                }
                
                return database.getClassName().equals(getClassName()) &&
                            database.isUppercasePreferred() == isUppercasePreferred();
            }
        }
        return false;
    }
    
    /**
     * 
     * @param index
     * @return 
     */
    @Override
    public boolean isColumnEditable(int index) {
        return false;
    }
    
    /**
     * 
     * @param index
     * @param object 
     */
    @Override
    public void setColumn(int index, Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     * @param object
     * @return
     */
    @Override
    public boolean add(Definition object) {
        boolean added =  super.add(object);
        if (added) {
            fireAddedDefinition();
        }
        return added;
    }

    /**
     *
     * @param index
     * @param object
     */
    @Override
    public void add(int index, Definition object) {
        super.add(index, object);
        fireAddedDefinition(index);
    }

    /**
     *
     * @param item
     * @return
     */
    public boolean update(Item item) {
        if (item instanceof Database) {
            return false;
        }

        boolean update = super.update(item);

        if (update) {
            Database newDatabase = (Database) item;
            setClassName(newDatabase.getClassName());
            setLibrary(newDatabase.getLibrary());
            setLibraryDelimeter(newDatabase.getLibraryDelimeter());
            setUppercasePreferred(newDatabase.isUppercasePreferred());
        }

        return update;
    }

    /**
     *
     * @param object
     * @return
     */
    @Override
    public boolean remove(Definition object) {
        int index = indexOf(object);
        boolean removed = super.remove(object);
        if (true) {
            fireDeletedDefinition(index);
        }
        return removed;
    }

    /**
     *
     * @param object
     * @return
     */
    @Override
    public boolean remove(Object object) {
        int index = indexOf(object);
        boolean removed = super.remove(object);
        if (true) {
            fireDeletedDefinition(index);
        }
        return removed;
    }

    /**
     *
     * @param index
     * @return
     */
    @Override
    public Definition remove(int index) {
        Definition definition = super.remove(index);
        if (definition != null) {
            fireDeletedDefinition(index);
        }
        return definition;
    }

    /**
     *
     * @param listener
     */
    public void addDatabaseListener(DatabaseListener listener) {
        addListener(DatabaseListener.class, listener);
    }

    /**
     *
     * @param listener
     */
    public void removeDatabaseListener(DatabaseListener listener) {
        this.listeners.remove(DatabaseListener.class, listener);
    }

    /**
     *
     * @return
     */
    public DatabaseListener[] getDatabaseListeners() {
        return this.listeners.getListeners(DatabaseListener.class);
    }

    /**
     * 
     */
    public void fireAddedDefinition() {
        for (DatabaseListener listener : getDatabaseListeners()) {
            listener.addedDefinition(new DatabaseEvent(this, size() - 1));
        }
    }

    /**
     *
     * @param index
     */
    public void fireAddedDefinition(int index) {
        for (DatabaseListener listener : getDatabaseListeners()) {
            listener.addedDefinition(new DatabaseEvent(this, index));
        }
    }

    /**
     *
     * @param index
     */
    public void fireDeletedDefinition(int index) {
        for (DatabaseListener listener : getDatabaseListeners()) {
            listener.deletedDefinition(new DatabaseEvent(this, index));
        }
    }

}

