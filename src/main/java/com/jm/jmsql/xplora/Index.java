/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.jmsql.xplora;

import static com.jm.jmsql.xplora.Item.Type.Index;
import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 *
 * @author Michael L.R. Marques
 * @param <P>
 */
public class Index<P extends Item> extends Item<P, Item> {

    private final String tableName;
    private final String qualifier;
    
    /**
     * 
     * @param parent
     * @param name 
     * @param tableName 
     * @param qualifier 
     */
    public Index(P parent, String name, String tableName, String qualifier) {
        super(parent, name);
        this.tableName = tableName;
        this.qualifier = qualifier;
    }

    /**
     * 
     * @return
     */
    public String getTableName() {
        return this.tableName;
    }
    
    /**
     * 
     * @return 
     */
    public String getQualifier() {
        return this.qualifier;
    }

    /**
     *
     * @return
     */
    @Override
    public boolean isLeaf() {
        return true;
    }

    /**
     *
     * @return
     */
    @Override
    public Icon getIcon() {
        return new ImageIcon(getClass().getResource("/index.png"));
    }

    /**
     *
     * @return
     */
    @Override
    public String getText() {
        return String.format("%s [%s]", this.name, this.tableName);
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public Type getObjectType() {
        return Index;
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public int getColumnCount() {
        return 2;
    }
    
    /**
     * 
     * @param index
     * @return 
     */
    @Override
    public String getColumnName(int index) {
        switch (index) {
            case 1: return "Qualifier";
            default: return "Name";
        }
    }
    
    /**
     * 
     * @param index
     * @return 
     */
    @Override
    public Object getColumn(int index) {
        switch (index) {
            case 1: return getQualifier();
            default: return getName();
        }
    }

    /**
     *
     * @param index
     * @return
     */
    @Override
    public Class<?> getColumnType(int index) {
        return String.class;
    }
    
    /**
     * 
     * @param index
     * @return 
     */
    @Override
    public boolean isColumnEditable(int index) {
        return false;
    }
    
    /**
     * 
     * @param index
     * @param object 
     */
    @Override
    public void setColumn(int index, Object object) {
        
    }
    
}
