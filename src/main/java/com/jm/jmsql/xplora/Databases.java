/*
 * Copyright (C) 2012-2013 Michael L.R. Marques
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Contact: michaellrmarques@gmail.com
 */
package com.jm.jmsql.xplora;

import static com.jm.jmsql.xplora.Item.Type.Databases;
import com.jm.jmsql.utils.Settings;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.stream.XMLStreamReader;

/**
 *
 * @created Nov 22, 2012
 * @author Michael
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Databases extends Item<Item, Database> {
    
    /**
     * 
     */
    private static JAXBContext jaxbContext;
    
    /**
     * 
     */
    Databases() {
       super();
    }
    
    /**
     *
     * @return String
     */
    @Override
    @XmlElement
    public String getName() {
        return super.getName();
    }
    
    /**
     * 
     * @param name 
     */
    @Override
    public void setName(String name) {
        super.setName(name);
    }
    
    /**
     * 
     * @return 
     */
    @XmlElement(name = "database")
    protected List<Database> getDatabases() {
        return this;
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public Icon getIcon() {
        return new ImageIcon(getClass().getResource("/dbxplora.png"));
    }
    
    /**
     * 
     * @return 
     */
    public List<Definition> getConnectedDefinitions() {
        List<Definition> definitions = new ArrayList();
        for (Database database : this) {
            for (Definition definition : database) {
                if (((Definition) definition).isConnected()) {
                    definitions.add(definition);
                }
            }
        }
        return definitions;
    }
    
    /**
     * 
     * @return 
     */
    public List<Definition> getDefinitions() {
        List<Definition> definitions = new ArrayList();
        for (Database database : this) {
            for (Definition definition : database) {
                definitions.add(definition);
            }
        }
        return definitions;
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public Type getObjectType() {
        return Databases;
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public int getColumnCount() {
        return 2;
    }
    
    /**
     * 
     * @param index
     * @return 
     */
    @Override
    public String getColumnName(int index) {
        switch (index) {
            case 0: return "Name";
            case 1: return "Databases";
            default: return null;
        }
    }
    
    /**
     * 
     * @param index
     * @return 
     */
    @Override
    public Object getColumn(int index) {
        switch (index) {
            case 0: return getName();
            case 1: return size();
            default: return null;
        }
    }
    
    /**
     * 
     * @param index
     * @return 
     */
    @Override
    public boolean isColumnEditable(int index) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    /**
     * 
     * @param index
     * @param object 
     */
    @Override
    public void setColumn(int index, Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    /**
     * 
     * @param reader
     * @return
     * @throws JAXBException 
     */
    public static Databases generate(XMLStreamReader reader) throws JAXBException {
        if (jaxbContext == null) {
            jaxbContext = JAXBContext.newInstance(Databases.class);
        }
        // If the databases file does not exist, create a blank one
        if (!Settings.getDatabasesFile().exists()) {
            new Databases().save();
        }
        // Start loading all databases from xml
        Databases databases = (Databases) jaxbContext.createUnmarshaller().unmarshal(reader);
        if (databases.isEmpty()) {
            databases.add(new JdbcOdbcDatabase());
        }
        return databases;
    }
    
    /**
     * 
     * @param file
     * @return
     * @throws JAXBException 
     */
    public static Databases generate(File file) throws JAXBException {
        if (jaxbContext == null) {
            jaxbContext = JAXBContext.newInstance(Databases.class);
        }
        // If the databases file does not exist, create a blank one
        if (!Settings.getDatabasesFile().exists()) {
            new Databases().save();
        }
        // Start loading all databases from xml
        Databases databases = (Databases) jaxbContext.createUnmarshaller().unmarshal(file);
        if (databases.isEmpty()) {
            databases.add(new JdbcOdbcDatabase());
        }
        return databases;
    }

    /**
     *
     * @param xml
     * @return
     * @throws JAXBException
     */
    public static Databases generate(String xml) throws JAXBException {
        if (jaxbContext == null) {
            jaxbContext = JAXBContext.newInstance(Databases.class);
        }
        // Start loading all databases from xml
        try (StringReader reader = new StringReader(xml)) {
            Databases databases = (Databases) jaxbContext.createUnmarshaller().unmarshal(reader);
            if (databases.isEmpty()) {
                databases.add(new JdbcOdbcDatabase());
            }
            return databases;
        }
    }
    
    /**
     * 
     * @return 
     */
    public boolean save() {
        // Create the directorys if they don't exist, delete the databases file just incase
        if (!Settings.getDatabasesFile().getParentFile().exists()) {
            Settings.getDatabasesFile().getParentFile().mkdirs();
        }
        // Re-create the database file
        try {
            Settings.getDatabasesFile().createNewFile();
        } catch (IOException ioe) {
            log.error(ioe.getMessage(), ioe);
            return false;
        }
        try {
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(this, Settings.getDatabasesFile());
            return true;
        } catch (JAXBException jaxbe) {
            log.error(jaxbe.getMessage(), jaxbe);
            return false;
        }
    }
    
    /**
     * 
     * @return 
     */
    public String saveToString() {
        try {
            try (StringWriter writer = new StringWriter()) {
                Marshaller marshaller = jaxbContext.createMarshaller();
                marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                marshaller.marshal(this, writer);
                
                return writer.toString();
            }
        } catch (IOException | JAXBException e) {
            log.error(e.getMessage(), e);
            return new Databases().saveToString();
        }
    }
    
    /**
     * 
     * @param index
     * @param newObject
     * @return 
     */
    @Override
    public Database set(int index, Database newObject) {
        newObject.setParent(this);
        newObject.addAll(get(index));
        return super.set(index, newObject);
    }
    
    /**
     * 
     */
    @Deprecated
    static final class JdbcOdbcDatabase extends Database {
        
        private static final String NAME = "Jdbc-Odbc Bridge";
        
        /**
         * 
         */
        public JdbcOdbcDatabase() {
            super(NAME, "sun.jdbc.odbc.JdbcOdbcDriver");
        }
        
    }

}
