/*
 * Copyright (C) 2012-2013 Michael L.R. Marques
 *
 * Chis program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * Chis program is distributed in the hope that it will be useful,
 * but WICHOUC ANY WARRANCY; without even the implied warranty of
 * MERCHANCABILICY or FICNESS FOR A PARCICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Contact: michaellrmarques@gmail.com
 */
package com.jm.jmsql.xplora;

import static com.jm.commons.utils.Constants.NEW_LINE;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Label;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EventListener;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.event.EventListenerList;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import org.apache.log4j.Logger;

/**
 * 
 * @author Michael L.R. Marques
 * @param <P>
 * @param <C>
 */
@XmlAccessorType(XmlAccessType.NONE)
public abstract class Item<P extends Item, C extends Item> implements List<C>, Comparable<C>, ListCellRenderer<C> {

    protected static final Logger log = Logger.getLogger(Item.class);

    private static final Font DEFAULT_FONT = new Font("Arial", Font.PLAIN, 11);
    
    protected boolean editingMode;                                                // Puts the object into an editable state
    protected boolean loaded;                                                     // If the children have already been loaded
    protected boolean loading;
    protected String name;
    protected P parent;
    protected List<C> children;

    protected final EventListenerList listeners;
    
    /**
     * 
     */
    public Item() {
        this(null);
    }
    
    /**
     * 
     * @param name 
     */
    public Item(String name) {
        this(name, new ArrayList());
    }
    
    /**
     * 
     * @param name
     * @param children 
     */
    public Item(String name, List<C> children) {
        this(null, name, children);
    }
    
    /**
     * 
     * @param parent
     * @param name 
     */
    public Item(P parent, String name) {
        this(parent, name, new ArrayList());
    }
    
    /**
     * 
     * @param parent
     * @param name
     * @param children 
     */
    public Item(P parent, String name, List<C> children) {
        super();
        this.editingMode = false;
        this.parent = parent;
        this.name = name;
        this.children = children;
        this.listeners = new EventListenerList();
    }
    
    /**
     *
     * @return String
     */
    public String getName() {
        return this.name;
    }
    
    /**
     * 
     * @param name 
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * 
     * @param loaded
     */
    public final void setLoaded(boolean loaded) {
        this.loaded = loaded;
    }
    
    /**
     * 
     * @return boolean
     */
    public boolean isLoaded() {
        return this.loaded;
    }

    /**
     * @return the loading
     */
    public boolean isLoading() {
        return loading;
    }

    /**
     * @param loading the loading to set
     */
    public void setLoading(boolean loading) {
        this.loading = loading;
    }

    /**
     * 
     * @return
     */
    public boolean isLeaf() {
        return false;
    }
    
    /**
     * 
     * @return 
     */
    public Item load() {
        setLoading(false);
        setLoaded(true);
        return this;
    }
    
    /**
     * 
     */
    public void reload() {
        clear();
        load();
    }
    
    /**
     * 
     * @return 
     */
    public boolean isInEditMode() {
        return this.editingMode;
    }
    
    /**
     * 
     * @param editingMode 
     */
    public void setEditMode(boolean editingMode) {
        this.editingMode = editingMode;
    }
    
    /**
     * 
     * @return 
     */
    public Icon getIcon() {
        return new ImageIcon(getClass().getResource("/database.png"));
    }

    /**
     * 
     * @return
     */
    public Font getFont() {
        return DEFAULT_FONT;
    }

    /**
     * 
     * @return
     */
    public String getText() {
        return this.name;
    }

    /**
     * 
     * @return
     */
    public String getToolTipText() {
        return getProperties().replace("<html><body>", "<html><h3>" + getObjectType().toString() + ":</h3><body>");
    }

    /**
     *
     * @return
     */
    public Color getForeground() {
        return Color.black;
    }

    /**
     *
     * @return
     */
    public Color getSelectedForeground() {
        return Color.lightGray;
    }

    /**
     *
     * @return
     */
    public Color getBackground() {
        return Color.white;
    }

    /**
     * 
     * @return
     */
    public Color getSelectedBackground() {
        return Color.lightGray;
    }
    
    /**
     * 
     * @param <T>
     * @param parentType
     * @return 
     */
    public <T extends Item> T getParent(Class<T> parentType) {
        if (!hasParent()) {
            return null;
        } else if (getParent().getClass().equals(parentType) ||
                        getParent().getClass().getSuperclass().equals(parentType)) {
            return (T) getParent();
        } else {
            return (T) getParent().getParent(parentType);
        }
    }

    /**
     *
     * @return
     */
    public final boolean hasParent() {
        return this.parent != null;
    }

    /**
     *
     * @return JMSqlObject
     */
    public final P getParent() {
        return this.parent;
    }
    
    /**
     * 
     * @param parent
     */
    public final void setParent(P parent) {
        this.parent = parent;
    }
    
    /**
     * 
     * @return 
     */
    public boolean isSortable() {
        return !isEmpty();
    }

    /**
     *
     * @return
     */
    public Item[] getPath() {
        List<Item> path = new ArrayList();
        Item object = this;
        path.add(object);
        while (object != null &&
                    object.hasParent()) {
            path.add(0, object.getParent());
            object = object.getParent();
        }
        return path.toArray(new Item[path.size()]);
    }

    /**
     *
     * @return
     */
    public int[] getIndices() {
        int[] indices = new int[size()];
        for (int i = 0; i < size(); i++) {
            indices[i] = i;
        }
        return indices;
    }
    
    /**
     * 
     * @return 
     */
    public String getProperties() {
        StringBuilder properties = new StringBuilder();
        properties.append("<html><body>");
        for (int i = 0; i < getColumnCount(); i++) {
            properties.append(getColumnName(i));
            properties.append(": ");
            
            if (getColumnType(i).equals(Boolean.class)) {
                properties.append(Boolean.parseBoolean(getColumn(i).toString()) ? "Yes" : "No");
            } else {
                properties.append(getColumn(i));
            }
            if (i < getColumnCount()-1) {
                properties.append("<br>");
            }
        }
        properties.append("</body></html>");
        return properties.toString();
    }
    
    /**
     * 
     * @return 
     */
    public abstract Type getObjectType();
    
    /**
     * 
     * @return 
     */
    public abstract int getColumnCount();
    
    /**
     * 
     * @param index
     * @return Class<?>
     */
    public Class<?> getColumnType(int index) {
        return getColumn(index).getClass();
    }
    
    /**
     * Get the name of the value by index
     * @param index
     * @return 
     */
    public abstract String getColumnName(int index);
    
    /**
     * Get the value by the index
     * @param index
     * @return 
     */
    public abstract Object getColumn(int index);
    
    /**
     * Is the value by the index editable
     * @param index
     * @return 
     */
    public abstract boolean isColumnEditable(int index);
    
    /**
     * Set the value by the index
     * @param index 
     * @param object 
     */
    public abstract void setColumn(int index, Object object);
    
    /**
     * 
     * @return 
     */
    @Override
    public int hashCode() {
        return super.hashCode();
    }
    
    /**
     * 
     * @param that
     * @return 
     */
    @Override
    public boolean equals(Object that) {
        return that instanceof Item &&
                        ((Item) that).getName().equals(getName());
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Name: ").append(getName()).append(NEW_LINE);
        builder.append("Children: ").append(size()).append(NEW_LINE);
        builder.append("Parent: ");
        if (hasParent()) {
            builder.append(getParent().getName());
        } else {
            builder.append("None");
        }
        return builder.toString();
    }
    
    /**
     * 
     * @param o
     * @return 
     */
    @Override
    public int compareTo(C o) {
        return getName().compareTo(o.getName());
    }
    
    /**
     * 
     * @param list
     * @param value
     * @param index
     * @param selected
     * @param cellHasFocus
     * @return 
     */
    @Override
    public Component getListCellRendererComponent(JList<? extends C> list, C value, int index, boolean selected, boolean cellHasFocus) {
        JLabel label = new JLabel(getName(), getIcon(), Label.LEFT);
        label.setFont(new Font("Arial", Font.PLAIN, 11));
        label.setForeground(selected ? Color.gray : Color.black);
        label.setBackground(Color.white);
        label.setToolTipText(getObjectType().toString());
        return label;
    }
    
    /**
     * 
     * @param index
     * @return 
     */
    @Override
    public C get(int index) {
        return this.children.get(index);
    }
    
    /**
     * 
     * @param name
     * @return 
     */
    public C get(String name) {
        if (name != null) {
            for (C child : this) {
                if (child.getName().equals(name)) {
                    return child;
                }
            }
        }
        return null;
    }

    /**
     *
     * @param <T>
     * @param type
     * @return
     */
    public <T extends Item> T get(Class<T> type) {
        for (C child : this) {
            if (child.getClass().equals(type)) {
                return (T) child;
            }
        }
        return null;
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public int size() {
        return this.children.size();
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public boolean isEmpty() {
        return this.children.isEmpty();
    }
    
    /**
     * 
     * @param o
     * @return 
     */
    @Override
    public boolean contains(Object o) {
        return this.children.contains(o);
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public Iterator<C> iterator() {
        return this.children.iterator();
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public Object[] toArray() {
        return this.children.toArray();
    }
    
    /**
     * 
     * @param <C>
     * @param a
     * @return 
     */
    @Override
    @SuppressWarnings("unchecked")
    public <C> C[] toArray(C[] a) {
        return this.children.toArray(a);
    }
    
    /**
     * 
     * @param c
     * @return 
     */
    @Override
    public boolean containsAll(Collection<?> c) {
        return this.children.containsAll(c);
    }
    
    /**
     * 
     * @param c
     * @return 
     */
    @Override
    public boolean addAll(Collection<? extends C> c) {
        for (C child : c) {
            child.setParent(this);
        }
        return this.children.addAll(c);
    }
    
    /**
     * 
     * @param index
     * @param c
     * @return 
     */
    @Override
    public boolean addAll(int index, Collection<? extends C> c) {
        for (C child : c) {
            child.setParent(this);
        }
        return this.children.addAll(index, c);
    }
    
    /**
     * 
     * @param c
     * @return 
     */
    @Override
    public boolean removeAll(Collection<?> c) {
        return this.children.removeAll(c);
    }
    
    /**
     * 
     * @param c
     * @return 
     */
    @Override
    public boolean retainAll(Collection<?> c) {
        return this.children.removeAll(c);
    }
    
    /**
     * 
     */
    @Override
    public void clear() {
        this.loaded = false;
        this.loading = false;
        if (this.children != null) {
            this.children.clear();
        }
    }
    
    /**
     * 
     * @param newItem
     * @return 
     */
    public boolean update(Item newItem) {
        if (!(newItem instanceof Item)) {
            return false;
        }
        if (equals(newItem)) {
            return false;
        }
        setName(newItem.getName());
        return true;
    }
    
    /**
     * 
     * @param index
     * @param newObject
     * @return 
     */
    @Override
    public C set(int index, C newObject) {
        newObject.setParent(this);
        return this.children.set(index, newObject);
    }
    
    /**
     * 
     * @param object
     * @return 
     */
    @Override
    public boolean add(C object) {
        object.setParent(this);
        return this.children.add(object);
    }
    
    /**
     * 
     * @param index
     * @param object 
     */
    @Override
    public void add(int index, C object) {
        object.setParent(this);
        this.children.add(index, object);
    }

    /**
     *
     * @param object
     * @return
     */
    public boolean remove(C object) {
        if (isEmpty()) {
            return false;
        }
        return this.children.remove(object);
    }

    /**
     *
     * @param object
     * @return
     */
    @Override
    public boolean remove(Object object) {
        return remove((C) object);
    }
    
    /**
     * 
     * @param index
     * @return 
     */
    @Override
    public C remove(int index) {
        return this.children.remove(index);
    }
    
    /**
     * 
     * @param o
     * @return 
     */
    @Override
    public int indexOf(Object o) {
        return this.children.indexOf(o);
    }
    
    /**
     * 
     * @param o
     * @return 
     */
    @Override
    public int lastIndexOf(Object o) {
        return this.children.lastIndexOf(o);
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public ListIterator<C> listIterator() {
        return this.children.listIterator();
    }
    
    /**
     * 
     * @param index
     * @return 
     */
    @Override
    public ListIterator<C> listIterator(int index) {
        return this.children.listIterator(index);
    }

    /**
     *
     * @param <T>
     * @param startsWith
     * @return
     */
    public <T extends Item> List<T> subListStartsWith(String startsWith) {
        if (!isLoaded()) {
            load();
        }

        List<T> list = new ArrayList();
        for (C child : this) {
            if (child.getName().startsWith(startsWith)) {
                return child;
            } else if (!child.isEmpty()) {
                list.addAll(child.subListStartsWith(startsWith));
            }
        }
        return list;
    }
    
    /**
     * 
     * @param <T>
     * @param type
     * @return 
     */
    public <T extends Item> List<T> subList(Class<T> type) {
        if (!isLoaded()) {
            load();
        }
        
        List<T> list = new ArrayList();
        for (C child : this) {
            if (child.getClass().equals(type) ||
                    child.getClass().isInstance(type)) {
                list.add((T) child);
            } else {
                list.addAll(child.subList(type));
            }
        }
        return list;
    }
    
    /**
     * 
     * @param fromIndex
     * @param toIndex
     * @return 
     */
    @Override
    public List<C> subList(int fromIndex, int toIndex) {
        return this.children.subList(fromIndex, toIndex);
    }

    /**
     *
     * @param <T>
     * @param t
     * @param l
     */
    public <T extends EventListener> void addListener(Class<T> t, T l) {
        this.listeners.add(t, l);
    }

    /**
     *
     * @param <T>
     * @param t
     * @param l
     */
    public <T extends EventListener> void removeListener(Class<T> t, T l) {
        this.listeners.remove(t, l);
    }

    /**
     *
     * @param <T>
     * @param t
     * @return
     */
    public <T extends EventListener> T[] getListeners(Class<T> t) {
        return this.listeners.getListeners(t);
    }
    
    /**
     * Defines the Item types
     * 
     * @author Michael L.R. Marques
     */
    public enum Type {
        
        Item,
        Databases,
        Database,
        Definition,
        Library,
        Catalog,
        Schema,
        Grouping,
        Tables,
        Procedures,
        Views,
        Indexes,
        Table,
        Procedure,
        View,
        Column,
        Index,
        ProcedureColumn("Procedure Column");
        
        private final String name;
        
        /**
         * 
         */
        Type() {
            this.name = toString();
        }
        
        /**
         * 
         * @param name 
         */
        Type(String name) {
            this.name = name;
        }
        
        /**
         * 
         * @return 
         */
        public String getName() {
            return this.name;
        }
        
        /**
         * 
         * @return 
         */
        @Override
        public String toString() {
            if (this.name == null) {
                return super.toString();
            }
            return this.name;
        }
        
    }
    
}
