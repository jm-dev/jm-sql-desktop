/*
 * Copyright (C) 2012-2013 Michael L.R. Marques
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Contact: michaellrmarques@gmail.com
 */
package com.jm.jmsql.xplora;

import static com.jm.jmsql.xplora.Item.Type.Procedures;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 *
 * @created Nov 22, 2012
 * @author Michael
 */
public class Procedures extends Grouping<Procedure> {
    
    /**
     * 
     * @param parent 
     */
    public Procedures(Library parent) {
        super(parent, "Procedures");
    }
    
    /**
     * Not needed
     * @return 
     */
    @Override
    public Item load() {
        if (isLoaded() ||
                isLoading()) {
            return this;
        }
        setLoading(true);
        if (isEmpty()) {
            DatabaseMetaData dmd = getParent(Definition.class).getDatabaseMetaData();
            synchronized (dmd) {
                try {
                    try (ResultSet results = dmd.getProcedures(getParent() instanceof Catalog ? getParent().getName() : null, getParent() instanceof Schema ? getParent().getName() : null, "%")) {
                        while (results.next()) {
                            try {
                                add(new Procedure(this, results.getString(3), results.getString(7)));
                            } catch (SQLException sqle) {
                                log.error(sqle.getMessage(), sqle);
                            }
                        }
                    }
                } catch (SQLException sqle) {
                    log.error(sqle.getMessage(), sqle);
                }
            }
        }
        return super.load();
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public Icon getIcon() {
        return new ImageIcon(getClass().getResource("/procedures.png"));
    }
    
    /**
     * 
     * @return 
     */
    @Override 
    public Type getObjectType() {
        return Procedures;
    }

    /**
     * 
     * @return 
     */
    @Override 
    public int getColumnCount() {
        return 2;
    }

    /**
     * 
     * @param index
     * @return 
     */
    @Override 
    public String getColumnName(int index) {
        switch(index) {
            case 0: return "Name";
            case 1: return "Procedures";
            default: return null;
        }
    }
    
    /**
     * 
     * @param index
     * @return 
     */
    @Override 
    public Object getColumn(int index) {
        switch(index) {
            case 0: return getName();
            case 1: return size();
            default: return null;
        }
    }
    
    /**
     * 
     * @param index
     * @return 
     */
    @Override
    public boolean isColumnEditable(int index) {
        return false;
    }
    
    /**
     * 
     * @param index
     * @param object 
     */
    @Override
    public void setColumn(int index, Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
