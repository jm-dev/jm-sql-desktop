/*
 * Copyright (C) 2012-2013 Michael L.R. Marques
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Contact: michaellrmarques@gmail.com
 */
package com.jm.jmsql.xplora;

import com.jm.jmsql.xplora.Item.Type;
import static com.jm.jmsql.xplora.Item.Type.View;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.regex.Pattern;
import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 *
 * @author Michael L.R. Marques
 */
public class View extends Item<Views, Column> {
    
    private String description;
    
    /**
     * 
     * @param views
     * @param name 
     * @param description 
     */
    public View(Views views, String name, String description) {
        super(views, name);
        this.description = description;
    }

    /**
     *
     * @return
     */
    public String getDescription() {
        return this.description;
    }

    /**
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public Item load() {
        if (isLoaded() ||
                isLoading()) {
            return this;
        }
        setLoading(true);
        if (isEmpty()) {
            Item object = getParent().getParent();
            DatabaseMetaData dmd = ((Definition) (object instanceof Definition ? object : object.getParent())).getDatabaseMetaData();
            try {
                try (ResultSet results = dmd.getColumns(object instanceof Catalog ? object.getName() : null, object instanceof Schema ? object.getName() : null, getName(), "%")) {
                    int column = 0;
                    int columns = results.getMetaData().getColumnCount();
                    while (results.next()) {
                        try {
                            if (columns >= 12 &&
                                    columns <= 17) {
                                add(new Column<>(this, results.getString(4), results.getInt(5), results.getString(6), results.getInt(7), results.getInt(8), results.getString(12), ++column));
                            } else if (columns >= 18 &&
                                            columns <= 22) {
                                add(new Column<>(this, results.getString(4), results.getInt(5), results.getString(6), results.getInt(7), results.getInt(8), results.getString(12), ++column, results.getString(18).equals("YES")));
                            } else if (columns == 23) {
                                add(new Column<>(this, results.getString(4), results.getInt(5), results.getString(6), results.getInt(7), results.getInt(8), results.getString(12), ++column, results.getString(18).equals("YES"), results.getString(23).equals("YES")));
                            } else {
                                add(new Column<>(this, results.getString(4), results.getInt(5), results.getString(6), results.getInt(7), results.getInt(8), results.getString(12), ++column, results.getString(18).equals("YES"), results.getString(23).equals("YES"), results.getString(24).equals("YES")));
                            }
                        } catch (SQLException sqle) {
                            log.error(sqle.getMessage(), sqle);
                        }
                    }
                }
                try (ResultSet results = dmd.getPrimaryKeys(object instanceof Catalog ? object.getName() : null, object instanceof Schema ? object.getName() : null, getName())) {
                    while (results.next()) {
                        for (Column<View> column : this) {
                            if (column.getName().equals(results.getString(4))) {
                                column.setPrimaryKey(true);
                            }
                        }
                    }
                }
            } catch (SQLException sqle) {
                log.error(sqle.getMessage(), sqle);
            }
        }
        return super.load();
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public Icon getIcon() {
        return new ImageIcon(getClass().getResource("/view.png"));
    }
    
    /**
     * 
     * @return 
     */
    @Override 
    public Type getObjectType() {
        return View;
    }

    /**
     * 
     * @return 
     */
    @Override 
    public int getColumnCount() {
        return 3;
    }
    
    /**
     * 
     * @param index
     * @return 
     */
    @Override 
    public String getColumnName(int index) {
        switch (index) {
            case 0: return "Name";
            case 1: return "Columns";
            case 2: return "Description";
            default: return null;
        }
    }
    
    /**
     * 
     * @param index
     * @return 
     */
    @Override 
    public Object getColumn(int index) {
        switch (index) {
            case 0: return getName();
            case 1: return size();
            case 2: return this.description == null ? "" : this.description;
            default: return null;
        }
    }
    
    /**
     * 
     * @param index
     * @return 
     */
    @Override
    public boolean isColumnEditable(int index) {
        switch (index) {
            case 0:;
            case 2: return true;
            default: return false;
        }
    }
    
    /**
     * 
     * @param index
     * @param object 
     */
    @Override
    public void setColumn(int index, Object object) {
        if (isInEditMode()) {
            switch (index) {
                case 0: {
                    if (Pattern.matches("[A-Za-z0-9_@]+", object.toString())) {
                        setName(object.toString());
                    }
                    break;
                } case 2: this.description = object.toString();
            }
        }
    }
    
}
