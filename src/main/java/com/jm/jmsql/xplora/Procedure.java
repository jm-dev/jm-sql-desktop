/*
 * Copyright (C) 2012-2013 Michael L.R. Marques
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Contact: michaellrmarques@gmail.com
 */
package com.jm.jmsql.xplora;

import com.jm.jmsql.xplora.Item.Type;
import static com.jm.jmsql.xplora.Item.Type.Procedure;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 *
 * @author Michael L.R. Marques
 */
public class Procedure extends Item<Procedures, ProcedureColumn> {
    
    /**
     * 
     */
    private String description;
    
    /**
     * 
     * @param procedures
     * @param name 
     * @param description 
     */
    public Procedure(Procedures procedures, String name, String description) {
        super(procedures, name);
        this.description = description;
    }

    /**
     *
     * @return
     */
    public String getDescription() {
        return this.description;
    }

    /**
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public Item load() {
        if (isLoaded() ||
                isLoading()) {
            return this;
        }
        setLoading(true);
        if (isEmpty()) {
            DatabaseMetaData dmd = getParent(Definition.class).getDatabaseMetaData();
            try {
                try (ResultSet results = dmd.getProcedureColumns(getParent(Library.class) instanceof Catalog ? getParent(Library.class).getName() : null, getParent(Library.class) instanceof Schema ? getParent(Library.class).getName() : null, getName(), "%")) {
                    while (results.next()) {
                        try {
                            add(new ProcedureColumn(this, results.getString(4), results.getInt(5), results.getInt(6), results.getString(7), results.getInt(9), results.getShort(11), results.getString(13), results.getRow()));
                        } catch (SQLException sqle) {
                            log.error(sqle.getMessage(), sqle);
                        }
                    }
                }
            } catch (SQLException sqle) {
                log.error(sqle.getMessage(), sqle);
            }
        }
        return super.load();
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public Icon getIcon() {
        return new ImageIcon(getClass().getResource("/procedure.png"));
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public Type getObjectType() {
        return Procedure;
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public int getColumnCount() {
        return 3;
    }
    
    /**
     * 
     * @param index
     * @return 
     */
    @Override
    public String getColumnName(int index) {
        switch (index) {
            case 0: return "Name";
            case 1: return "Columns";
            case 2: return "Description";
            default: return null;
        }
    }
    
    /**
     * 
     * @param index
     * @return 
     */
    @Override
    public Object getColumn(int index) {
        switch (index) {
            case 0: return getName();
            case 1: return size();
            case 2: return this.description == null ? "" : this.description;
            default: return null;
        }
    }
    
    /**
     * 
     * @param index
     * @return 
     */
    @Override
    public boolean isColumnEditable(int index) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    /**
     * 
     * @param index
     * @param object 
     */
    @Override
    public void setColumn(int index, Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
