/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.jmsql.jdbc;

import java.io.Reader;
import java.math.BigDecimal;
import java.sql.Array;
import java.sql.Ref;
import java.sql.Struct;
import java.sql.Types;
import java.text.Format;
import java.util.Date;

/**
 *
 * @author Marquema
 */
public enum JDBCType {

    Char("CHAR", Types.CHAR, String.class),
    VarChar("VARCHAR", Types.VARCHAR, String.class),
    LongNVarChar("LONGNVARCHAR", Types.LONGNVARCHAR, String.class),
    Numeric("NUMERIC", Types.NUMERIC, BigDecimal.class),
    Decimal("DECIMAL", Types.DECIMAL, BigDecimal.class),
    Boolean("BOOLEAN", Types.BOOLEAN, Boolean.class),
    Integer("INT", Types.INTEGER, Integer.class),
    BigInteger("BIGINT", Types.BIGINT, Long.class),
    SmallInteger("SMALLINT", Types.SMALLINT, Short.class),
    Real("REAL", Types.REAL, Float.class),
    Float("FLOAT", Types.FLOAT, Float.class),
    Double("DOUBLE", Types.DOUBLE, Double.class),
    Clob("CLOB", Types.CLOB, Reader.class),
    Blob("BLOB", Types.BLOB, Byte[].class),
    Binary("BINARY", Types.BINARY, Byte[].class),
    VarBinary("VARBINARY", Types.VARBINARY, Byte[].class),
    LongVarBinary("LONGVARBINARY", Types.LONGVARBINARY, Byte[].class),
    Date("DATE", Types.DATE, Date.class),
    Time("TIME", Types.TIME, Date.class),
    TimeStamp("TIMESTAMP", Types.TIMESTAMP, Date.class),
    Array("ARRAY", Types.ARRAY, Array.class),
    Struct("STRUCT", Types.STRUCT, Struct.class),
    Ref("REF", Types.REF, Ref.class),
    Class("CLASS", Types.JAVA_OBJECT, Class.class),
    Object("OBJECT", Types.OTHER, Object.class);
    
    private final String name;
    private final int jdbc;
    private final Class type;
    private final Format format;

    /**
     *
     * @param name
     * @param jdbc
     * @param type
     * @param format
     */
    JDBCType(String name, int jdbc, Class type) {
        this.name = name;
        this.jdbc = jdbc;
        this.type = type;
        this.format = null;
    }
    
    /**
     * 
     * @return 
     */
    public String getName() {
        return this.name;
    }

    /**
     *
     * @return
     */
    public int getJdbcType() {
        return this.jdbc;
    }

    /**
     *
     * @return
     */
    public Class getType() {
        return this.type;
    }

    /**
     * 
     * @return
     */
    @Deprecated
    public Format getFormat() {
        return this.format;
    }

    /**
     * 
     * @return
     */
    @Deprecated
    public boolean isFormattable() {
        return this.format != null;
    }
    
    /**
     * 
     * @param name
     * @return 
     */
    public static JDBCType get(String name) {
        for (JDBCType type : JDBCType.values()) {
            if (type.name.equals(name)) {
                return type;
            }
        }
        return Object;
    }

    /**
     *
     * @param jdbc
     * @return
     */
    public static JDBCType get(int jdbc) {
        for (JDBCType type : JDBCType.values()) {
            if (type.jdbc == jdbc) {
                return type;
            }
        }
        return Object;
    }

    /**
     *
     * @param type
     * @return
     */
    public static JDBCType get(Class type) {
        for (JDBCType jdbc : JDBCType.values()) {
            if (jdbc.type == type) {
                return jdbc;
            }
        }
        return Object;
    }

}
