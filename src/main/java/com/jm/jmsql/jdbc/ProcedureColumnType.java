/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.jmsql.jdbc;

/**
 *
 * @author Marquema
 */
public enum ProcedureColumnType {

    Unknown("Unknown", 1),
    ColumnIn("Column In", 2),
    ColumnInOut("Column In/Out", 3),
    ColumnOut("Column Out", 4),
    Return("Return", 5),
    Result("Result", 6);
    
    private final String name;
    private final int type;

    /**
     *
     * @param jdbc
     * @param type
     */
    private ProcedureColumnType(String name, int type) {
        this.name = name;
        this.type = type;
    }
    
    /**
     * 
     * @return 
     */
    public String getName() {
        return this.name;
    }

    /**
     *
     * @return
     */
    public int getType() {
        return this.type;
    }
    
    /**
     * 
     * @param name
     * @return 
     */
    public static ProcedureColumnType get(String name) {
        for (ProcedureColumnType type : ProcedureColumnType.values()) {
            if (type.name.equals(name)) {
                return type;
            }
        }
        return Unknown;
    }

    /**
     *
     * @param type
     * @return
     */
    public static ProcedureColumnType get(int type) {
        for (ProcedureColumnType pct : ProcedureColumnType.values()) {
            if (pct.type == type) {
                return pct;
            }
        }
        return Unknown;
    }

}
