/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.jmsql.listener;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import javax.swing.event.EventListenerList;
import org.apache.log4j.Logger;

/**
 *
 * @author Michael L.R. Marques
 */
public class InteractiveListener implements Runnable {

    private static final Logger log = Logger.getLogger(InteractiveListener.class);
    
    public static final String OK = "OK";
    public static final String DEFAULT_IP = "localhost";
    public static final int DEFAULT_PORT = 6576;

    private final int port;
    private final EventListenerList listeners;
    private ServerSocket serverSocket;
    private boolean running;

    /**
     * 
     */
    public InteractiveListener() {
        this(DEFAULT_PORT);
    }

    /**
     * 
     * @param port
     */
    public InteractiveListener(int port) {
        this.port = port;
        this.listeners = new EventListenerList();
    }

    /**
     * 
     * @return boolean
     */
    public boolean isRunning() {
        return this.running;
    }

    /**
     * 
     */
    @Override
    public void run() {
        log.debug("Entering InteractiveListener run");
        try {
            this.serverSocket = new ServerSocket(this.port);
            this.running = true;
            while (this.running) {
                try (Socket socket = this.serverSocket.accept()) {
                    try (PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
                            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()))) {
                        writer.println(OK);
                        String line = reader.readLine();
                        if (line != null &&
                                !line.isEmpty() &&
                                    !line.equals(OK)) {
                            fireOpenFile(new File(line));
                            break;
                        }
                    }
                } catch (IOException ioe) {
                    log.error(ioe.getMessage(), ioe);
                }
            }
        } catch (IOException ioe) {
            log.error(ioe.getMessage(), ioe);
        } finally {
            log.debug("Exiting InteractiveListener run");
        }
    }
    
    /**
     * 
     * @param l 
     */
    public void addFileOpenListener(FileOpenListener l) {
        this.listeners.add(FileOpenListener.class, l);
    }
    
    /**
     * 
     * @param l 
     */
    public void removeFileOpenListener(FileOpenListener l) {
        this.listeners.remove(FileOpenListener.class, l);
    }
    
    /**
     * 
     * @return 
     */
    public FileOpenListener[] getFileOpenListeners() {
        return this.listeners.getListeners(FileOpenListener.class);
    }

    /**
     *
     * @param file
     */
    protected void fireOpenFile(File file) {
        for (FileOpenListener listener : getFileOpenListeners()) {
            listener.openFile(new FileOpenEvent(this, file));
        }
    }

}
