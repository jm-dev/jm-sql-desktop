/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.jmsql.listener;

import java.io.File;
import java.util.EventObject;

/**
 *
 * @author Marquema
 */
public class FileOpenEvent extends EventObject {

    private final File file;

    /**
     * 
     * @param source
     * @param file
     */
    public FileOpenEvent(Object source, String file) {
        this(source, new File(file));
    }

    /**
     *
     * @param source
     * @param file
     */
    public FileOpenEvent(Object source, File file) {
        super(source);
        this.file = file;
    }

    /**
     * @return the file
     */
    public File getFile() {
        return file;
    }

}
