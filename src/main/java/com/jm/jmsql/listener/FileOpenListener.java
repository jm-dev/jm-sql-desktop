/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.jmsql.listener;

import java.util.EventListener;

/**
 *
 * @author Michael L.R. Marques
 */
public interface FileOpenListener extends EventListener {

    /**
     * 
     * @param event
     */
    void openFile(FileOpenEvent event);

}
