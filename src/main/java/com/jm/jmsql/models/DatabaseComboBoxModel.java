/*
 * Copyright (C) 2013 Michael
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.jm.jmsql.models;

import com.jm.jmsql.xplora.Database;
import com.jm.jmsql.xplora.Databases;
import com.jm.jmsql.utils.Settings;
import javax.swing.ComboBoxModel;
import javax.swing.event.EventListenerList;
import javax.swing.event.ListDataListener;
import javax.xml.bind.JAXBException;

/**
 *
 * @created Nov 23, 2012
 * @author Michael L.R. Marques
 */
public class DatabaseComboBoxModel implements ComboBoxModel {
    
    /**
     * 
     */
    private EventListenerList listeners;
    private Databases databases;
    private Database selectedDatabase;
    
    /**
     * 
     * @throws javax.xml.bind.JAXBException
     */
    public DatabaseComboBoxModel() throws JAXBException {
        this(Databases.generate(Settings.getDatabasesFile()));
    }
    
    /**
     * 
     * @param database 
     */
    public DatabaseComboBoxModel(Database database) {
        this((Databases) database.getParent());
        this.selectedDatabase = database;
    }
    
    /**
     * 
     * @param databases 
     */
    public DatabaseComboBoxModel(Databases databases) {
        this.databases = databases;
        this.listeners = new EventListenerList();
        
        if (!this.databases.isEmpty()) {
            this.selectedDatabase = (Database) this.databases.get(0);
        }
    }
    
    /**
     * 
     * @param anItem 
     */
    @Override
    public void setSelectedItem(Object anItem) {
        this.selectedDatabase = (Database) this.databases.get(this.databases.indexOf(anItem));
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public Object getSelectedItem() {
        return this.selectedDatabase;
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public int getSize() {
        return this.databases.size();
    }
    
    /**
     * 
     * @param index
     * @return 
     */
    @Override
    public Object getElementAt(int index) {
        return this.databases.get(index);
    }
    
    /**
     * 
     * @param l 
     */
    @Override
    public void addListDataListener(ListDataListener l) {
        this.listeners.add(ListDataListener.class, l);
    }
    
    /**
     * 
     * @param l 
     */
    @Override 
    public void removeListDataListener(ListDataListener l) {
        this.listeners.remove(ListDataListener.class, l);
    }

}
