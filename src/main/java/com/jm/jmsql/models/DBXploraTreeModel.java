/*
 * Copyright (C) 2012-2013 Michael L.R. Marques
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Contact: michaellrmarques@gmail.com
 */
package com.jm.jmsql.models;

import com.jm.jmsql.xplora.Database;
import com.jm.jmsql.xplora.Databases;
import com.jm.jmsql.xplora.Definition;
import com.jm.jmsql.xplora.Item;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.EventListener;
import java.util.EventObject;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.EventListenerList;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeCellEditor;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreePath;

/**
 *
 * @author Michael L.R. Marques
 */
public class DBXploraTreeModel implements TreeModel<Item> {
    
    private final EventListenerList listeners;
    private final Databases databases;
    
    /**
     * 
     * @param databases 
     */
    public DBXploraTreeModel(Databases databases) {
        this.listeners = new EventListenerList();
        this.databases = databases;
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public Item getRoot() {
        return this.databases;
    }
    
    /**
     * 
     * @param parent
     * @param index
     * @return 
     */
    @Override
    public Item getChild(Object parent, int index) {
        if (((Item) parent).isEmpty()) {
            ((Item) parent).load();
        }
        return ((Item) parent).get(index);
    }
    
    /**
     * 
     * @param parent
     * @return 
     */
    @Override
    public int getChildCount(Object parent) {
        if (((Item) parent).isLoading()) {
            return 0;
        } else if (((Item) parent).isEmpty()) {
            ((Item) parent).load();
        }
        return ((Item) parent).size();
    }
    
    /**
     * 
     * @param node
     * @return 
     */
    @Override
    public boolean isLeaf(Object node) {
        if (((Item) node).isLeaf()) {
            return true;
        }
        return ((Item) node).isLoading() ||
                    (((Item) node).isEmpty() &&
                        ((Item) node).isLoaded());
    }
    
    /**
     * 
     * @param path
     * @param newValue 
     */
    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {
        fireTreeNodesChanged((Item) newValue);
    }
    
    /**
     * 
     * @param parent
     * @param child
     * @return 
     */
    @Override
    public int getIndexOfChild(Object parent, Object child) {
        return ((Item) parent).indexOf((Item) child);
    }
    
    /**
     * 
     * @param l 
     */
    @Override
    public void addTreeModelListener(TreeModelListener l) {
        this.listeners.add(TreeModelListener.class, l);
    }
    
    /**
     * 
     * @param l 
     */
    @Override
    public void removeTreeModelListener(TreeModelListener l) {
        this.listeners.remove(TreeModelListener.class, l);
    }
    
    /**
     * 
     * @return 
     */
    public TreeModelListener[] getTreeModelListeners() {
        return this.listeners.getListeners(TreeModelListener.class);
    }
    
    /**
     * 
     * @param <T>
     * @param listenerType
     * @return 
     */
    public <T extends EventListener> T[] getListeners(Class<T> listenerType) {
        return this.listeners.getListeners(listenerType);
    }
    
    /**
     * 
     * @param object 
     */
    public void fireTreeNodesChanged(Item object) {
        // Loop through the listeners
        for (TreeModelListener listener : getTreeModelListeners()) {
            // Lazily create the event:
            listener.treeNodesChanged(new TreeModelEvent(this, object.getPath(), object.getIndices(), object.toArray()));
        }
    }
    
    /**
     * 
     * @param object 
     */
    public void fireTreeNodesInserted(Item object) {
        // Loop through the listeners
        for (TreeModelListener listener : getTreeModelListeners()) {
            // Lazily create the event:
            listener.treeNodesInserted(new TreeModelEvent(this, object.getPath(), object.getIndices(), object.toArray()));
        }
    }
    
    /**
     * 
     * @param object 
     */
    public void fireTreeNodesRemoved(Item object) {
        // Loop through the listeners
        for (TreeModelListener listener : getTreeModelListeners()) {
            // Lazily create the event:
            listener.treeNodesRemoved(new TreeModelEvent(this, object.getPath(), object.getIndices(), object.toArray()));
        }
    }
    
    /**
     * 
     * @param object 
     */
    public void fireStructureChanged(Item object) {
        // Loop through the listeners
        for (TreeModelListener listener : getTreeModelListeners()) {
            // Lazily create the event:
            listener.treeStructureChanged(new TreeModelEvent(this, object.getPath(), object.getIndices(), object.toArray()));
        }
    }
    
    /**
     * 
     * @author Michael L.R. Marques
     */
    public static final class DBXploraTreeCellRenderer extends JLabel implements TreeCellRenderer {
                
        /**
         * 
         */
        public DBXploraTreeCellRenderer() {
            setHorizontalAlignment(JLabel.LEADING);
        }
        
        /**
         * 
         * @param tree
         * @param value
         * @param selected
         * @param expanded
         * @param leaf
         * @param row
         * @param hasFocus
         * @return 
         */
        @Override
        public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
            final Item item = (Item) value;
            setIcon(item.getIcon());
            setText(item.getText());
            setFont(item.getFont());
            if (selected) {
                tree.setToolTipText(item.getToolTipText());
                setBackground(item.getSelectedBackground());
                setForeground(item.getSelectedForeground());
            } else {
                setBackground(item.getBackground());
                setForeground(item.getForeground());
            }
            return this;
        }
        
    }

    /**
     *
     * @author Michael L.R. Marques
     */
    public static class DBXploraTreeCellEditor extends JTextField implements TreeCellEditor {

        private final JTree tree;

        /**
         * 
         * @param tree
         */
        public DBXploraTreeCellEditor(JTree tree) {
            this.tree = tree;

            addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (stopCellEditing()) {
                        fireEditingStopped();
                    }
                }
            });
        }

        /**
         *
         * @param tree
         * @param value
         * @param isSelected
         * @param expanded
         * @param leaf
         * @param row
         * @return
         */
        @Override
        public Component getTreeCellEditorComponent(JTree tree, Object value, boolean isSelected, boolean expanded, boolean leaf, int row) {
            if (value instanceof Item) {
                Item item = (Item) value;
                setText(item.getName());
                return this;
            }
            return null;
        }

        /**
         *
         * @param e
         * @return
         */
        @Override
        public boolean isCellEditable(EventObject e) {
            return e == null &&
                        (this.tree.getLastSelectedPathComponent() instanceof Definition ||
                            this.tree.getLastSelectedPathComponent() instanceof Database);
        }

        /**
         *
         * @return
         */
        @Override
        public Object getCellEditorValue() {
            Item item = (Item) this.tree.getLastSelectedPathComponent();
            item.setName(getText());
            return item;
        }

        /**
         *
         * @param event
         * @return
         */
        @Override
        public boolean shouldSelectCell(EventObject event) {
            return true;
        }

        /**
         * 
         * @return
         */
        @Override
        public boolean stopCellEditing() {
            return !getText().isEmpty();
        }

        /**
         *
         */
        @Override
        public void cancelCellEditing() {
            
        }

        /**
         *
         * @param l
         */
        @Override
        public void addCellEditorListener(CellEditorListener l) {
            this.listenerList.add(CellEditorListener.class, l);
        }

        /**
         * 
         * @param l
         */
        @Override
        public void removeCellEditorListener(CellEditorListener l) {
            this.listenerList.remove(CellEditorListener.class, l);
        }

        /**
         *
         */
        protected void fireEditingStopped() {
            for (CellEditorListener listener : getListeners(CellEditorListener.class)) {
                listener.editingStopped(new ChangeEvent(this));
            }
        }

    }
    
}
