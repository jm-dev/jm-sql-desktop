/*
 * Copyright (C) 2013 Michael
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.jm.jmsql.models;

import com.jm.jmsql.xplora.Column;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.ComboBoxModel;
import javax.swing.event.ListDataListener;

/**
 *
 * @created Mar 15, 2013
 * @author Michael L.R. Marques
 */
public class JDBCTypeComboBoxModel implements ComboBoxModel<JDBCTypeComboBoxModel.JDBCType> {
    
    private final List<JDBCType> types;
    private JDBCType selectedType;

    /**
     *
     */
    public JDBCTypeComboBoxModel() {
        this.types = new ArrayList(14);
        this.types.add(0, new JDBCType("Boolean", java.sql.Types.BOOLEAN, "BOOLEAN"));
        this.types.add(1, new JDBCType("Character", java.sql.Types.CHAR, "CHAR"));
        this.types.add(2, new JDBCType("Variable Character", java.sql.Types.VARCHAR, "VARCHAR"));
        this.types.add(3, new JDBCType("Date", java.sql.Types.DATE, "DATE"));
        this.types.add(4, new JDBCType("Time", java.sql.Types.TIME, "TIME"));
        this.types.add(5, new JDBCType("Time Stamp", java.sql.Types.TIMESTAMP, "TIMESTAMP"));
        this.types.add(6, new JDBCType("Decimal", java.sql.Types.DECIMAL, "DECIMAL"));
        this.types.add(7, new JDBCType("Integer", java.sql.Types.INTEGER, "INTEGER"));
        this.types.add(8, new JDBCType("Small Integer", java.sql.Types.SMALLINT, "SMALLINT"));
        this.types.add(9, new JDBCType("Big Integer", java.sql.Types.BIGINT, "BIGINT"));
        this.types.add(10, new JDBCType("Numeric", java.sql.Types.NUMERIC, "NUMERIC"));
        this.types.add(11, new JDBCType("Double", java.sql.Types.DOUBLE, "DOUBLE"));
        this.types.add(12, new JDBCType("Floar", java.sql.Types.FLOAT, "FLOAT"));
        this.types.add(13, new JDBCType("Blob", java.sql.Types.BLOB, "BLOB"));
        this.selectedType = this.types.get(0);
    }

    /**
     *
     * @param anItem
     */
    @Override
    public void setSelectedItem(Object anItem) {
        if (this.types.contains((JDBCType) anItem)) {
            this.selectedType = (JDBCType) anItem;
        }
    }

    /**
     *
     * @return
     */
    @Override
    public Object getSelectedItem() {
        return this.selectedType;
    }

    /**
     *
     * @return
     */
    @Override
    public int getSize() {
        return this.types.size();
    }

    /**
     *
     * @param index
     * @return
     */
    @Override
    public JDBCType getElementAt(int index) {
        return this.types.get(index);
    }

    /**
     *
     * @param l
     */
    @Override
    public void addListDataListener(ListDataListener l) {
    }

    /**
     *
     * @param l
     */
    @Override
    public void removeListDataListener(ListDataListener l) {}

    /**
     *
     * @author Michael L.R. Marques
     */
    public class JDBCType {
        
        private final String name;
        private final int type;
        private final String typeName;

        /**
         *
         * @param name
         * @param type
         * @param typeName
         */
        public JDBCType(String name, int type, String typeName) {
            this.name = name;
            this.type = type;
            this.typeName = typeName;
        }

        /**
         *
         * @return
         */
        public String getName() {
            return this.name;
        }

        /**
         *
         * @return
         */
        public int getType() {
            return this.type;
        }
        
        /**
         * 
         * @return 
         */
        public String getTypeName() {
            return this.typeName;
        }
        
        /**
         * 
         * @return 
         */
        public boolean hasNoLength() {
            return Arrays.asList(Column.NO_LENGTH_TYPES).contains(this.type);
        }
        
        /**
         * 
         * @return 
         */
        public boolean hasDecimals() {
            return Arrays.asList(Column.DECIMAL_TYPES).contains(this.type);
        }

        /**
         *
         * @return
         */
        @Override
        public String toString() {
            return this.name;
        }

        /**
         *
         * @param o
         * @return
         */
        @Override
        public boolean equals(Object o) {
            if (o instanceof JDBCType) {
                return ((JDBCType) o).getName().equals(this.name) &&
                            ((JDBCType) o).getType() == this.type;
            }
            return false;
        }

        /**
         *
         * @return
         */
        @Override
        public int hashCode() {
            return super.hashCode();
        }
        
    }
    
}
