/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.jmsql.models;

import com.jm.jmsql.xplora.Definition;
import com.jm.jmsql.xplora.Item;
import java.sql.SQLException;
import javax.swing.ComboBoxModel;
import javax.swing.event.ListDataListener;

/**
 *
 * @author Michael L.R. Marques
 */
public class LibraryComboBoxModel implements ComboBoxModel<Item> {

    private final Definition definition;
    private Item selected;

    /**
     *
     * @param definition
     * @throws java.sql.SQLException
     */
    public LibraryComboBoxModel(Definition definition) throws SQLException {
        this.definition = definition;
        if (definition.getDefaultLibrary() != null) {
            for (Item item : definition) {
                if (item.getName().equals(definition.getDefaultLibrary())) {
                    this.selected = item;
                    break;
                }
            }
        }
        if (this.selected == null &&
                !definition.isEmpty()) {
            this.selected = definition.get(0);
        }
    }

    /**
     *
     * @param anItem
     */
    @Override
    public void setSelectedItem(Object anItem) {
        this.selected = (Item) anItem;
    }

    /**
     *
     * @return
     */
    @Override
    public Object getSelectedItem() {
        return this.selected;
    }

    /**
     *
     * @return
     */
    @Override
    public int getSize() {
        return this.definition.size();
    }

    /**
     *
     * @param index
     * @return
     */
    @Override
    public Item getElementAt(int index) {
        return this.definition.get(index);
    }

    /**
     *
     * @param l
     */
    @Override
    public void addListDataListener(ListDataListener l) {
        
    }

    /**
     *
     * @param l
     */
    @Override
    public void removeListDataListener(ListDataListener l) {
        
    }

}
