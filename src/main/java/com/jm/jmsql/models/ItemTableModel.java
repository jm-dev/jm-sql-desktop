/*
 * Copyright (C) 2013 Michael
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.jm.jmsql.models;

import com.jm.jmsql.xplora.Definition;
import com.jm.jmsql.xplora.Item;
import com.jm.jmsql.xplora.Library;
import javax.swing.event.EventListenerList;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

/**
 * 
 * @param <T>
 * @created Dec 12, 2012
 * @author Michael L.R. Marques
 */
public class ItemTableModel<T extends Item<? extends Item, ? extends Item>> implements TableModel<Object>, Runnable {

    private final EventListenerList listeners;
    private T item;
    
    /**
     * 
     * @param item 
     */
    public ItemTableModel(final T item) {
        this(item, false);
    }

    /**
     *
     * @param item
     * @param loadChildren
     */
    public ItemTableModel(final T item, boolean loadChildren) {
        this.listeners = new EventListenerList();
        setItem(item, loadChildren);
    }
    
    /**
     * 
     * @return 
     */
    public T getItem() {
        return this.item;
    }
    
    /**
     * 
     * @param item
     */
    public void setItem(T item) {
        setItem(item, false);
    }

    /**
     *
     * @param item
     * @param loadChildren
     */
    public void setItem(T item, boolean loadChildren) {
        this.item = item;
        fireTableChanged();
        if (loadChildren) {
            new Thread(this).start();
        }
    }

    /**
     *
     * @return
     */
    @Override
    public int getRowCount() {
        return this.item.size();
    }

    /**
     *
     * @return
     */
    @Override
    public int getColumnCount() {
        return this.item.size() > 0 ? this.item.get(0).getColumnCount() : 0;
    }

    /**
     *
     * @param columnIndex
     * @return
     */
    @Override
    public String getColumnName(int columnIndex) {
        return (columnIndex == 1 && this.item instanceof Library && !(this.item instanceof Definition)) ? "Total" : this.item.get(0).getColumnName(columnIndex);
    }

    /**
     *
     * @param columnIndex
     * @return
     */
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return this.item.get(0).getColumnType(columnIndex);
    }

    /**
     *
     * @param rowIndex
     * @param columnIndex
     * @return
     */
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return this.item.get(rowIndex).isInEditMode() &&
                    this.item.get(rowIndex).isColumnEditable(columnIndex);
    }

    /**
     *
     * @param rowIndex
     * @param columnIndex
     * @return
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return this.item.get(rowIndex).getColumn(columnIndex);
    }

    /**
     *
     * @param value
     * @param rowIndex
     * @param columnIndex
     */
    @Override
    public void setValueAt(Object value, int rowIndex, int columnIndex) {
        this.item.get(rowIndex).setColumn(columnIndex, value);
        fireTableChanged();
    }

    /**
     *
     * @param l
     */
    @Override
    public void addTableModelListener(TableModelListener l) {
        this.listeners.add(TableModelListener.class, l);
    }

    /**
     *
     * @param l
     */
    @Override
    public void removeTableModelListener(TableModelListener l) {
        this.listeners.remove(TableModelListener.class, l);
    }
    
    /**
     * 
     */
    public void fireTableChanged() {
        // Loop through the listeners
        for (TableModelListener listener : this.listeners.getListeners(TableModelListener.class)) {
            // Lazily create the event:
            listener.tableChanged(new TableModelEvent(this, 0, getRowCount(), TableModelEvent.ALL_COLUMNS));
        }
    }
    
    /**
     * 
     */
    @Override
    public void run() {
        for (Item childItem : this.item) {
            childItem.load();
        }
    }
    
}
