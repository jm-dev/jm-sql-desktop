/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.jmsql.models;

/**
 *
 * @author Marquema
 */
public class SQLHistoryTableModel extends AbstractTableModel<String> {


    /**
     *
     */
    public SQLHistoryTableModel() {
        super();
    }

    /**
     *
     * @return
     */
    @Override
    public int getRowCount() {
        return this.list.size();
    }

    /**
     *
     * @return
     */
    @Override
    public int getColumnCount() {
        return 1;
    }

    /**
     *
     * @param rowIndex
     * @param columnIndex
     * @return
     */
    @Override
    public String getValueAt(int rowIndex, int columnIndex) {
        return this.list.get(rowIndex);
    }

    /**
     *
     * @param columnIndex
     * @return
     */
    @Override
    public String getColumnName(int columnIndex) {
        return "SQL Query";
    }

}
