/*
 * Copyright (C) 2013 Michael
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.jm.jmsql.models;

import com.jm.commons.lookup.Lookup;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ComboBoxModel;
import javax.swing.event.ListDataListener;

/**
 *
 * @created Nov 22, 2012
 * @author Michael L.R. Marques
 */
public class DriverComboBoxModel implements ComboBoxModel<String> {
    
    private List<String> drivers;
    
    private String selectedDriver;
    
    /**
     * 
     */
    public DriverComboBoxModel() {
        this.drivers = new ArrayList();
    }
    
    /**
     * 
     * @param drivers 
     */
    public DriverComboBoxModel(List<String> drivers) {
        this.drivers = drivers;
        
        if (!this.drivers.isEmpty()) {
            this.selectedDriver = this.drivers.get(0);
        }
    }
    
    /**
     * 
     * @param file
     * @throws IOException 
     */
    public DriverComboBoxModel(String file) throws IOException {
        this(new File(file));
    }
    
    /**
     * 
     * @param file
     * @throws IOException 
     */
    public DriverComboBoxModel(File file) throws IOException {
        //
        this.drivers = new ArrayList();
        //
        for (Class clazz : Lookup.byInterface(file, java.sql.Driver.class)) {
            this.drivers.add(clazz.getName());
            for (Class clazz2 : Lookup.byInterface(file, clazz.getName())) {
                this.drivers.add(clazz2.getName());
            }
        }
        // 
        if (!this.drivers.isEmpty()) {
            this.selectedDriver = this.drivers.get(0);
        }
    }
    
    /**
     * 
     * @param anItem 
     */
    @Override
    public void setSelectedItem(Object anItem) {
        this.selectedDriver = anItem.toString();
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public Object getSelectedItem() {
        return this.selectedDriver;
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public int getSize() {
        return this.drivers.size();
    }
    
    /**
     * 
     * @param index
     * @return 
     */
    @Override 
    public String getElementAt(int index) {
        return this.drivers.get(index);
    }
    
    /**
     * 
     * @param l 
     */
    @Override
    public void addListDataListener(ListDataListener l) {}
    
    /**
     * 
     * @param l 
     */
    @Override
    public void removeListDataListener(ListDataListener l) {}

}
