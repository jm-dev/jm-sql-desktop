/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jm.jmsql.models;

/**
 *
 * @author Marquema
 * @param <T>
 */
public interface TableModel<T extends Object> extends javax.swing.table.TableModel {

    /**
     * 
     * @param rowIndex
     * @param columnIndex
     * @return 
     */
    @Override
    public abstract T getValueAt(int rowIndex, int columnIndex);
    
}
