/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.jmsql.models;

import com.jm.jmsql.sql.var.VariableException;
import com.jm.jmsql.sql.var.Variables;
import com.jm.jmsql.sql.var.event.VariableEvent;
import com.jm.jmsql.sql.var.event.VariableListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ComboBoxModel;
import javax.swing.event.EventListenerList;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

/**
 *
 * @author Marquema
 */
public class VariablesComboBoxModel implements ComboBoxModel<String>, VariableListener {

    private final EventListenerList listeners;
    private final List<String> variables;
    private String selectedVariable;

    public VariablesComboBoxModel() throws VariableException {
        this.listeners = new EventListenerList();
        this.variables = new ArrayList<>(Variables.getInstance().getVariables().keySet());
        if (!this.variables.isEmpty()) {
            this.selectedVariable = this.variables.get(0);
        }
        Variables.getInstance().addVariableListener(this);
    }

    @Override
    public void setSelectedItem(Object anItem) {
        if (anItem != null) {
            this.selectedVariable = anItem.toString();
        }
    }

    @Override
    public Object getSelectedItem() {
        return this.selectedVariable;
    }

    @Override
    public int getSize() {
        return this.variables.size();
    }

    @Override
    public String getElementAt(int index) {
        return this.variables.get(index);
    }

    @Override
    public void addListDataListener(ListDataListener l) {
        this.listeners.add(ListDataListener.class, l);
    }

    @Override
    public void removeListDataListener(ListDataListener l) {
        this.listeners.remove(ListDataListener.class, l);
    }

    @Override
    public void variableAdded(VariableEvent ve) {
        this.variables.add(ve.getVariable());
        if (!this.variables.isEmpty()) {
            this.selectedVariable = this.variables.get(0);
        }
        for (ListDataListener listener : this.listeners.getListeners(ListDataListener.class)) {
            listener.intervalAdded(new ListDataEvent(ve, ListDataEvent.CONTENTS_CHANGED, 0, this.variables.size()));
        }
    }

    @Override
    public void variableDeleted(VariableEvent ve) {
        this.variables.remove(ve.getVariable());
        if (this.selectedVariable.equals(ve.getVariable()) &&
                !this.variables.isEmpty()) {
            this.selectedVariable = this.variables.get(0);
        }
        for (ListDataListener listener : this.listeners.getListeners(ListDataListener.class)) {
            listener.intervalRemoved(new ListDataEvent(ve, ListDataEvent.CONTENTS_CHANGED, 0, this.variables.size() - 2));
        }
    }

    @Override
    public void variableChanged(VariableEvent ve) {
        for (ListDataListener listener : this.listeners.getListeners(ListDataListener.class)) {
            listener.contentsChanged(new ListDataEvent(ve, ListDataEvent.CONTENTS_CHANGED, 0, this.variables.size() - 1));
        }
    }

}
