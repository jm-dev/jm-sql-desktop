/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jm.jmsql.models;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreePath;

/**
 *
 * @author Marquema
 */
public interface TreeModel<T> extends javax.swing.tree.TreeModel {

    @Override
    public T getRoot();

    @Override
    public Object getChild(Object parent, int index);

    @Override
    public int getChildCount(Object parent);

    @Override
    public boolean isLeaf(Object node);

    @Override
    public void valueForPathChanged(TreePath path, Object newValue);

    @Override
    public int getIndexOfChild(Object parent, Object child);

    @Override
    public void addTreeModelListener(TreeModelListener l);

    @Override
    public void removeTreeModelListener(TreeModelListener l);
    
}
