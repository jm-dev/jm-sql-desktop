/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.jmsql.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author Marquema
 * @param <T>
 */
public abstract class AbstractTableModel<T extends Object> extends javax.swing.table.AbstractTableModel implements TableModel<T>, List<T> {

    protected final List<T> list;

    /**
     *
     */
    public AbstractTableModel() {
        this(new ArrayList(0));
    }

    /**
     *
     * @param list
     */
    public AbstractTableModel(T... list) {
        this(new ArrayList(Arrays.asList(list)));
    }

    /**
     *
     * @param list
     */
    public AbstractTableModel(List<T> list) {
        super();
        this.list = list;
    }

    /**
     *
     * @return
     */
    @Override
    public abstract int getRowCount();
    
    /**
     *
     * @param rowIndex
     * @param columnIndex
     * @return
     */
    @Override
    public abstract T getValueAt(int rowIndex, int columnIndex);

    /**
     *
     * @return
     */
    @Override
    public abstract int getColumnCount();

    /**
     * 
     * @param columnIndex
     * @return
     */
    @Override
    public abstract String getColumnName(int columnIndex);
    
    /**
     * 
     * @return 
     */
    @Override
    public int size() {
        return list.size();
    }

    /**
     *
     * @return
     */
    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }

    /**
     *
     * @param o
     * @return
     */
    @Override
    public boolean contains(Object o) {
        return list.contains(o);
    }

    /**
     *
     * @return
     */
    @Override
    public Iterator<T> iterator() {
        return list.iterator();
    }

    /**
     *
     * @return
     */
    @Override
    public Object[] toArray() {
        return list.toArray();
    }

    /**
     *
     * @param <T>
     * @param a
     * @return
     */
    @Override
    public <T> T[] toArray(T[] a) {
        return list.toArray(a);
    }

    /**
     *
     * @param e
     * @return
     */
    @Override
    public boolean add(T e) {
        if (list.add(e)) {
            int index = list.indexOf(e);
            fireTableRowsInserted(index, index);
            return true;
        }
        return false;
    }

    /**
     *
     * @param o
     * @return
     */
    @Override
    public boolean remove(Object o) {
        int index = list.indexOf(o);
        if (list.remove(o)) {
            fireTableRowsDeleted(index, index);
            return true;
        }
        return false;
    }

    /**
     *
     * @param c
     * @return
     */
    @Override
    public boolean containsAll(Collection<?> c) {
        return list.containsAll(c);
    }

    /**
     *
     * @param c
     * @return
     */
    @Override
    public boolean addAll(Collection<? extends T> c) {
        if (list.addAll(c)) {
            int index = list.indexOf(c.iterator().next());
            fireTableRowsInserted(index, index + c.size() - 1);
            return true;
        }
        return false;
    }

    /**
     *
     * @param index
     * @param c
     * @return
     */
    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        if (list.addAll(index, c)) {
            fireTableRowsInserted(index, index + c.size() - 1);
            return true;
        }
        return false;
    }

    /**
     *
     * @param c
     * @return
     */
    @Override
    public boolean removeAll(Collection<?> c) {
        if (list.removeAll(c)) {
            fireTableDataChanged();
            return true;
        }
        return false;
    }

    /**
     *
     * @param c
     * @return
     */
    @Override
    public boolean retainAll(Collection<?> c) {
        if (list.retainAll(c)) {
            fireTableDataChanged();
            return true;
        }
        return false;
    }

    /**
     *
     */
    @Override
    public void clear() {
        list.clear();
    }

    /**
     *
     * @param index
     * @return
     */
    @Override
    public T get(int index) {
        return list.get(index);
    }

    /**
     *
     * @param index
     * @param element
     * @return
     */
    @Override
    public T set(int index, T element) {
        if (list.set(index, element) != null) {
            fireTableRowsUpdated(index, index);
            return element;
        }
        return null;
    }

    /**
     *
     * @param index
     * @param element
     */
    @Override
    public void add(int index, T element) {
        list.add(index, element);
        fireTableRowsInserted(index, index);
    }

    /**
     *
     * @param index
     * @return
     */
    @Override
    public T remove(int index) {
        T element = list.remove(index);
        if (element != null) {
            fireTableRowsDeleted(index, index);
            return element;
        }
        return null;
    }

    /**
     *
     * @param o
     * @return
     */
    @Override
    public int indexOf(Object o) {
        return list.indexOf(o);
    }

    /**
     *
     * @param o
     * @return
     */
    @Override
    public int lastIndexOf(Object o) {
        return list.lastIndexOf(o);
    }

    /**
     *
     * @return
     */
    @Override
    public ListIterator<T> listIterator() {
        return list.listIterator();
    }

    /**
     *
     * @param index
     * @return
     */
    @Override
    public ListIterator<T> listIterator(int index) {
        return list.listIterator(index);
    }

    /**
     *
     * @param fromIndex
     * @param toIndex
     * @return
     */
    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        return list.subList(fromIndex, toIndex);
    }
    
}
