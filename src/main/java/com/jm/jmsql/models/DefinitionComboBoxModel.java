/*
 * Copyright (C) 2013 Michael
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.jm.jmsql.models;

import com.jm.jmsql.xplora.Databases;
import com.jm.jmsql.xplora.Definition;
import com.jm.jmsql.xplora.events.DefinitionAdapter;
import com.jm.jmsql.xplora.events.DefinitionEvent;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.util.Collections;
import java.util.List;
import javax.swing.ComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.event.EventListenerList;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

/**
 *
 * @created Nov 23, 2012
 * @author Michael L.R. Marques
 */
public class DefinitionComboBoxModel implements ComboBoxModel<Definition> {
    
    private EventListenerList listeners;
    private List<Definition> definitions;
    private Definition selectedDefinition;
    
    /**
     * 
     * @param databases 
     */
    public DefinitionComboBoxModel(Databases databases) {
        this(databases, null);
    }
    
    /**
     * 
     * @param databases 
     * @param selectedDefinition 
     */
    public DefinitionComboBoxModel(Databases databases, Definition selectedDefinition) {
        this.definitions = databases.getDefinitions();
        this.listeners = new EventListenerList();
        
        Collections.sort(this.definitions);
        
        this.selectedDefinition = selectedDefinition == null ? (!this.definitions.isEmpty() ? (Definition) this.definitions.get(0) : null) : selectedDefinition;
        // Implement connect and disconnect events
        for (Definition definition : this.definitions) {
            definition.addDefinitionListener(new DefinitionAdapter() {
                @Override
                public void connected(DefinitionEvent e) {
                    Collections.sort(definitions);
                    fireDefinitionsChanged();
                }
                @Override
                public void disconnected(DefinitionEvent e) {
                    Collections.sort(definitions);
                    fireDefinitionsChanged();
                }
            });
        }
    }
    
    /**
     * 
     * @param anItem 
     */
    @Override
    public void setSelectedItem(Object anItem) {
        this.selectedDefinition = (Definition) anItem;
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public Object getSelectedItem() {
        return this.selectedDefinition;
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public int getSize() {
        return this.definitions.size();
    }
    
    /**
     * 
     * @param index
     * @return 
     */
    @Override
    public Definition getElementAt(int index) {
        return this.definitions.get(index);
    }
    
    /**
     * 
     * @param l 
     */
    @Override
    public void addListDataListener(ListDataListener l) {
        this.listeners.add(ListDataListener.class, l);
    }
    
    /**
     * 
     * @param l 
     */
    @Override
    public void removeListDataListener(ListDataListener l) {
        this.listeners.remove(ListDataListener.class, l);
    }

    /**
     *
     * @param index
     */
    public void fireDefinitionAdded(int index) {
        for (ListDataListener listener : this.listeners.getListeners(ListDataListener.class)) {
            listener.intervalAdded(new ListDataEvent(this, ListDataEvent.INTERVAL_REMOVED, index, index));
        }
    }

    /**
     *
     * @param index
     */
    public void fireDefinitionRemoved(int index) {
        for (ListDataListener listener : this.listeners.getListeners(ListDataListener.class)) {
            listener.intervalRemoved(new ListDataEvent(this, ListDataEvent.INTERVAL_REMOVED, index, index));
        }
    }
    
    /**
     * 
     */
    public void fireDefinitionsChanged() {
        for (ListDataListener listener : this.listeners.getListeners(ListDataListener.class)) {
            listener.contentsChanged(new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED, 0, this.definitions.size()));
        }
    }
    
    /**
     * 
     */
    public static class DefinitionComboBoxCellRenderer extends JLabel implements ListCellRenderer {
        
        /**
         * 
         * @param list
         * @param value
         * @param index
         * @param isSelected
         * @param cellHasFocus
         * @return 
         */
        @Override
        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            if (value instanceof Definition) {
                Definition definition = (Definition) value;
                setText(definition.getName() + " - " + definition.getDatasource());
                setIcon(definition.getIcon());
                setFont(new Font("Arial", Font.PLAIN, 11));
                setBackground(Color.white);
                setForeground(Color.black);
                return this;
            }
            return this;
        }
        
    }
    
}
