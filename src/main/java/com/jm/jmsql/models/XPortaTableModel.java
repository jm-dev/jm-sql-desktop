/*
 * Copyright (C) 2013 Michael
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */



package com.jm.jmsql.models;

import com.jm.jmsql.plugins.PluginClassLoader;
import com.jm.jmsql.plugins.PluginException;
import com.jm.jmsql.plugins.PluginFactory;
import com.jm.jmsql.utils.Settings;
import com.jm.xporta.plugin.XPort;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.filechooser.FileFilter;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @created Feb 12, 2013
 * @author Michael L.R. Marques
 */
public class XPortaTableModel extends AbstractTableModel {
    
    private static final Logger logger = LogManager.getLogger(XPortaTableModel.class);

    /**
     *
     */
    private List<XPort<?, ?>> xports;

    /**
     * 
     */
    public XPortaTableModel() {
        loadPlugins();
    }
    
    /**
     * 
     */
    public void reload() {
        loadPlugins();
    }

    /**
     *
     * @return
     */
    @Override
    public int getRowCount() {
        return this.xports.size();
    }

    /**
     *
     * @return
     */
    @Override
    public int getColumnCount() {
        return 2;
    }

    /**
     *
     * @param columnIndex
     * @return
     */
    @Override
    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 1: return "Description";
            default: return "Name";
        }
    }

    /**
     *
     * @param columnIndex
     * @return
     */
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            default: return String.class;
        }
    }

    /**
     *
     * @param rowIndex
     * @param columnIndex
     * @return
     */
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    /**
     *
     * @param rowIndex
     * @param columnIndex
     * @return
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 1: return this.xports.get(rowIndex).getDescription();
            default: return this.xports.get(rowIndex).getName();
        }
    }

    /**
     *
     * @param aValue
     * @param rowIndex
     * @param columnIndex
     */
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {}

    /**
     *
     * @param index
     * @return
     */
    public XPort getXPort(int index) {
        return this.xports.get(index);
    }
    
    /**
     * 
     */
    private void loadPlugins() {
        this.xports = new ArrayList();
        try {
            for (PluginClassLoader<XPort> pcl : PluginFactory.getInstance(XPort.class, Settings.getXPortPluginsLocation()).getPlugins(XPort.class)) {
                try {
                    XPort<?, ?> xport = pcl.newInstance();
                    if (!xport.isExcluded()) {
                        this.xports.add(xport);
                    }
                } catch (InstantiationException | IllegalAccessException e) {
                    logger.error(e.getMessage(), e);
                }
            }
        } catch (PluginException pe) {
            logger.error(pe.getMessage(), pe);
        }
    }

    /**
     *
     */
    public static class XPortaFileTypeCellEditor extends AbstractCellEditor implements TableCellEditor {

        /**
         *
         */
        private JComboBox cell;

        /**
         *
         * @param table
         * @param value
         * @param isSelected
         * @param row
         * @param column
         * @return
         */
        @Override
        public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
            this.cell = new JComboBox();
            if (value instanceof FileFilter[]) {
                for (FileFilter filter : (FileFilter[]) value) {
                    this.cell.addItem(filter.getDescription());
                }
            }
            return this.cell;
        }

        /**
         *
         * @return
         */
        @Override
        public Object getCellEditorValue() {
            return this.cell.getSelectedItem();
        }
    }


    /**
     *
     */
    public final static class XPortaFileTypeCellRenderer extends JComboBox<String> implements TableCellRenderer {

        /**
         *
         * @param table
         * @param value
         * @param isSelected
         * @param hasFocus
         * @param row
         * @param column
         * @return
         */
        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            // Add the file filter objects
            if (value instanceof FileFilter[]) {
                removeAllItems();

                for (FileFilter filter : (FileFilter[]) value) {
                    addItem(filter.getDescription());
                }

                if (getItemCount() > 0) {
                    setSelectedIndex(0);
                }
            }

            // Check if the row is selected
            if (isSelected) {
                setForeground(table.getSelectionForeground());
                super.setBackground(table.getSelectionBackground());
            } else {
                setForeground(table.getForeground());
                setBackground(table.getBackground());
            }

            return this;
        }
    }
}
