/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.jmsql.plugins;

import com.jm.jmsql.plugins.PluginClassLoader;
import com.jm.xporta.plugin.XPort;
import java.io.File;
import java.net.MalformedURLException;

/**
 *
 * @author Michael L.R. Marques
 * @param <T>
 */
public class XPortPluginClassLoader<T extends XPort> extends PluginClassLoader<T> {

    /**
     *
     * @param clazz
     * @param file
     * @throws MalformedURLException
     * @throws ClassNotFoundException
     */
    public XPortPluginClassLoader(Class<T> clazz, File file) throws MalformedURLException, ClassNotFoundException {
        super(clazz, file);
    }

}
