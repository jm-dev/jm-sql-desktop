/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jm.jmsql.plugins;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

/**
 *
 * @author Michael L.R. Marques
 * @param <T>
 */
public class PluginClassLoader<T> extends URLClassLoader {
    
    private final Class<T> type;
    private final File file;
    
    /**
     * 
     * @param clazz
     * @param file
     * @throws MalformedURLException 
     * @throws java.lang.ClassNotFoundException 
     */
    public PluginClassLoader(Class<T> clazz, File file) throws MalformedURLException, ClassNotFoundException {
        super(new URL[] { new URL("jar:file:" + file.getAbsolutePath() + "!/") });
        
        this.type = (Class<T>) Class.forName(clazz.getName().replace(".class", ""), true, this);
        this.file = file;
    }
    
    /**
     * 
     * @return 
     */
    public Class<T> getType() {
        return this.type;
    }
    
    /**
     * 
     * @return 
     */
    public File getFile() {
        return this.file;
    }
    
    /**
     * 
     * @return
     * @throws InstantiationException
     * @throws IllegalAccessException 
     */
    public T newInstance() throws InstantiationException, IllegalAccessException {
        return this.type.newInstance();
    }
    
}
