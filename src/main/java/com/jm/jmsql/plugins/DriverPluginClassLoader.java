/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.jmsql.plugins;

import java.io.File;
import java.net.MalformedURLException;
import java.sql.Driver;

/**
 *
 * @author Michael L.R. Marques
 * @param <T>
 */
public class DriverPluginClassLoader<T extends Driver> extends PluginClassLoader<T> {
    
    /**
     * 
     * @param clazz
     * @param file
     * @throws MalformedURLException
     * @throws ClassNotFoundException 
     */
    public DriverPluginClassLoader(Class<T> clazz, File file) throws MalformedURLException, ClassNotFoundException {
        super(clazz, file);
    }

}
