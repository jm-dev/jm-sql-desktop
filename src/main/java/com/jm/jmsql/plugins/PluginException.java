/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.jmsql.plugins;

/**
 *
 * @author Michael L.R. Marques
 */
public class PluginException extends Exception {

    /**
     * 
     */
    public PluginException() {
        super();
    }

    /**
     *
     * @param message
     */
    public PluginException(String message) {
        super(message);
    }

    /**
     *
     * @param message
     * @param cause
     */
    public PluginException(String message, Throwable cause) {
        super(message, cause);
    }

}
