/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jm.jmsql.plugins;

import com.jm.commons.fio.FileSystem;
import com.jm.commons.lookup.Lookup;
import com.jm.xporta.plugin.XPort;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.sql.Driver;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Michael L.R. Marques
 */
public class PluginFactory {
    
    private static final Logger logger = Logger.getLogger(PluginFactory.class);
    
    private static PluginFactory factory;
    
    private List<PluginClassLoader> plugins;
    
    /**
     * 
     * @throws FileNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException 
     */
    private PluginFactory() {
        if (this.plugins == null) {
            this.plugins = new ArrayList();
        }
    }
    
    /**
     * 
     * @param type
     * @param pluginLocation
     * @return PluginFactory
     * @throws PluginException
     */
    public static PluginFactory getInstance(Class<?> type, File pluginLocation) throws PluginException {
        return getInstance(type, pluginLocation, false);
    }
    
    /**
     * 
     * @param type
     * @param reload
     * @param pluginLocation
     * @return PluginFactory
     * @throws PluginException
     */
    public static PluginFactory getInstance(Class<?> type, File pluginLocation, boolean reload) throws PluginException {
        if (factory == null) {
            factory = new PluginFactory();
            factory.loadPlugins(type, pluginLocation);
        } else if (reload) {
            factory.getPlugins().removeAll(factory.getPlugins(type));
            factory.loadPlugins(type, pluginLocation);
        }
        return factory;
    }
    
    /**
     * 
     * @return 
     */
    public List<PluginClassLoader> getPlugins() {
        return this.plugins;
    }
    
    /**
     * 
     * @param <T>
     * @param type
     * @return 
     */
    public <T> List<PluginClassLoader<T>> getPlugins(Class<T> type) {
        List<PluginClassLoader<T>> pcls = new ArrayList();
        for (PluginClassLoader pcl : this.plugins) {
            if (type.isAssignableFrom(pcl.getType())) {
                pcls.add(pcl);
            }
        }
        return pcls;
    }

    /**
     * 
     * @param <T>
     * @param type
     * @param pluginLocation
     * @throws FileNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws MalformedURLException
     * @throws ClassNotFoundException
     */
    private <T> void loadPlugins(Class<T> type, File pluginLocation) throws PluginException {
        try {
            if (!pluginLocation.exists()) {
                throw new FileNotFoundException(pluginLocation.getAbsolutePath());
            }

            for (File file : FileSystem.listFiles(null, pluginLocation, new String[] { "jar" })) {
                for (Class<T> clazz : Lookup.byInterface(file, type)) {
                    try {
                        this.plugins.add(getCustomClassLoader(clazz, file));
                    } catch (NoClassDefFoundError ncde) {
                        logger.error(ncde.getMessage(), ncde);
                    }
                }
            }
        } catch (FileNotFoundException fnfe) {
            throw new PluginException(fnfe.getMessage(), fnfe);
        }
    }

    /**
     *
     * @return
     */
    private <T> PluginClassLoader<T> getCustomClassLoader(Class<T> type, File pluginLocation) throws PluginException {
        try {
            if (XPort.class.isAssignableFrom(type)) {
                return new XPortPluginClassLoader(type, pluginLocation);
            } else if (Driver.class.isAssignableFrom(type)) {
                return new DriverPluginClassLoader(type, pluginLocation);
            } else {
                throw new PluginException("Plugin type does not exist");
            }
        } catch (MalformedURLException | ClassNotFoundException e) {
            throw new PluginException(e.getMessage(), e);
        }
    }

}
