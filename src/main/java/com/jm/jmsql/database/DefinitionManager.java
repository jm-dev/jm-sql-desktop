/*
 * Copyright (C) 2012-2013 Michael L.R. Marques
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Contact: michaellrmarques@gmail.com
 */
package com.jm.jmsql.database;

import com.jm.commons.components.JMDialog;
import com.jm.commons.ui.UIBounds;
import com.jm.jmsql.models.DatabaseComboBoxModel;
import com.jm.jmsql.utils.Constants;
import com.jm.jmsql.xplora.Database;
import com.jm.jmsql.xplora.Definition;import com.jm.jmsql.xplora.exceptions.LoadDriverException;
import java.awt.Frame;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.sql.SQLException;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Michael L.R. Marques
 */
public class DefinitionManager extends JMDialog implements DocumentListener {
    
    private static final Logger logger = LogManager.getLogger(DefinitionManager.class);
    
    public static final int SUCCESSFUL = 1;
    public static final int UN_SUCCESSFUL = 0;
    private int option;
    
    /**
     * Creates new form DefinitionManager
     * @param parent
     * @param database 
     */
    public DefinitionManager(Frame parent, Database database) {
        super(parent);
        initComponents();
        this.mainPanel.setImage(new java.io.File("./images/optionslogo.png"));
        UIBounds.setBounds(parent, this);
        // Set the icon image
        try {
            setIconImage(ImageIO.read(getClass().getResource("/connected_definition.png")));
        } catch (IOException ioe) {
            logger.error(ioe.getMessage(), ioe);
        }
        // 
        this.txtDefinitionName.getDocument().addDocumentListener(this);
        this.txtDatasource.getDocument().addDocumentListener(this);
        this.txtUsername.getDocument().addDocumentListener(this);
        this.txtPassword.getDocument().addDocumentListener(this);
        this.txtPassword.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if (e.isControlDown()) {
                    switch (e.getKeyCode()) {
                        case KeyEvent.VK_C: e.consume(); break;
                        case KeyEvent.VK_X: e.consume(); break;
                    }
                }
            }
        });
        // 
        this.cbxDatabases.setModel(new DatabaseComboBoxModel(database));
    }
    
    /**
     * 
     * @param definition
     */
    public void setDefinition(Definition definition) {
        this.txtDefinitionName.setText(definition.getName());
        this.txtDatasource.setText(definition.getDatasource());
        this.txtUsername.setText(definition.getUsername());
        this.txtPassword.setText(definition.getPassword());
        this.chbAutoConnect.setSelected(definition.isAutoConnect());
        this.cbxDatabases.setSelectedItem(definition.getParent());
        
        this.cbxDatabases.setEnabled(false);
        this.btnTestDatasource.setEnabled(true);
        this.btnDone.setEnabled(true);
    }
    
    /**
     * 
     * @return 
     */
    public Definition getDefinition() {
        return this.option == SUCCESSFUL ? new Definition((Database) this.cbxDatabases.getSelectedItem(),
                                                            this.txtDefinitionName.getText(),
                                                                this.txtDatasource.getText(),
                                                                    this.txtUsername.getText(),
                                                                        new String(this.txtPassword.getPassword()),
                                                                            this.chbAutoConnect.isSelected()) : null;
    }
    
    /**
     * 
     * @return 
     */
    public int showDialog() {
        this.option = UN_SUCCESSFUL;
        setVisible(true);
        return this.option;
    }
    
    /**
     * 
     * @param e 
     */
    @Override
    public void insertUpdate(DocumentEvent e) {
        this.btnTestDatasource.setEnabled(!this.txtDefinitionName.getText().isEmpty() &&
                                    !this.txtDatasource.getText().isEmpty() &&
//                                        !this.txtUsername.getText().isEmpty() &&
//                                            !(new String(this.txtPassword.getPassword())).isEmpty() &&
                                                this.cbxDatabases.getSelectedItem() != null);
        this.btnDone.setEnabled(!this.txtDefinitionName.getText().isEmpty() &&
                                    !this.txtDatasource.getText().isEmpty() &&
//                                        !this.txtUsername.getText().isEmpty() &&
//                                            !(new String(this.txtPassword.getPassword())).isEmpty() &&
                                                this.cbxDatabases.getSelectedItem() != null);
    }
    
    /**
     * 
     * @param e 
     */
    @Override
    public void removeUpdate(DocumentEvent e) {
        this.btnTestDatasource.setEnabled(!this.txtDefinitionName.getText().isEmpty() &&
                                    !this.txtDatasource.getText().isEmpty() &&
//                                        !this.txtUsername.getText().isEmpty() &&
//                                            !(new String(this.txtPassword.getPassword())).isEmpty() &&
                                                this.cbxDatabases.getSelectedItem() != null);
        this.btnDone.setEnabled(!this.txtDefinitionName.getText().isEmpty() &&
                                    !this.txtDatasource.getText().isEmpty() &&
//                                        !this.txtUsername.getText().isEmpty() &&
//                                            !(new String(this.txtPassword.getPassword())).isEmpty() &&
                                                this.cbxDatabases.getSelectedItem() != null);
    }
    
    /**
     * 
     * @param e 
     */
    @Override
    public void changedUpdate(DocumentEvent e) {
        this.btnTestDatasource.setEnabled(!this.txtDefinitionName.getText().isEmpty() &&
                                    !this.txtDatasource.getText().isEmpty() &&
                                        !this.txtUsername.getText().isEmpty() &&
                                            !(new String(this.txtPassword.getPassword())).isEmpty() &&
                                                this.cbxDatabases.getSelectedItem() != null);
        this.btnDone.setEnabled(!this.txtDefinitionName.getText().isEmpty() &&
                                    !this.txtDatasource.getText().isEmpty() &&
//                                        !this.txtUsername.getText().isEmpty() &&
//                                            !(new String(this.txtPassword.getPassword())).isEmpty() &&
                                                this.cbxDatabases.getSelectedItem() != null);
        
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainPanel = new com.jm.commons.components.panel.ImagePanel();
        btnDone = new javax.swing.JButton();
        txtUsername = new com.jm.commons.components.textfield.HintJTextField();
        txtDatasource = new com.jm.commons.components.textfield.HintJTextField();
        cbxDatabases = new javax.swing.JComboBox();
        txtPassword = new javax.swing.JPasswordField();
        txtDefinitionName = new com.jm.commons.components.textfield.HintJTextField();
        btnTestDatasource = new javax.swing.JButton();
        chbAutoConnect = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Definition Manager");
        setModal(true);
        setResizable(false);

        mainPanel.setImage(new java.io.File("C:\\Users\\Michael\\Documents\\NetBeansProjects\\JMSql\\images\\optionslogo.png"));
        mainPanel.setPreferredSize(new java.awt.Dimension(549, 227));

        btnDone.setText("Done");
        btnDone.setEnabled(false);
        btnDone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                done(evt);
            }
        });

        txtUsername.setHintText("Username...");
        txtUsername.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                done(evt);
            }
        });

        txtDatasource.setHintText("Datasource...");
        txtDatasource.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                done(evt);
            }
        });

        txtPassword.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                done(evt);
            }
        });

        txtDefinitionName.setHintText("Definition Name...");
        txtDefinitionName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                done(evt);
            }
        });

        btnTestDatasource.setText("Test Datasource");
        btnTestDatasource.setEnabled(false);
        btnTestDatasource.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                testDatasource(evt);
            }
        });

        chbAutoConnect.setText("Auto-Connect");

        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtDefinitionName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cbxDatabases, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtDatasource, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(mainPanelLayout.createSequentialGroup()
                        .addComponent(btnTestDatasource)
                        .addGap(52, 52, 52)
                        .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(mainPanelLayout.createSequentialGroup()
                                .addComponent(chbAutoConnect)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnDone))
                            .addGroup(mainPanelLayout.createSequentialGroup()
                                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtPassword)
                                    .addComponent(txtUsername, javax.swing.GroupLayout.DEFAULT_SIZE, 218, Short.MAX_VALUE))
                                .addGap(163, 163, 163)))))
                .addContainerGap())
        );
        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addGap(103, 103, 103)
                .addComponent(txtDefinitionName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cbxDatabases, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(txtDatasource, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtUsername, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnDone)
                    .addComponent(btnTestDatasource)
                    .addComponent(chbAutoConnect))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 564, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 302, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     *
     * @param evt
     */
    private void done(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_done
        // Done button must be enabled
        if (!this.btnDone.isEnabled()) {
            return;
        }
        // Set the option as successful
        this.option = SUCCESSFUL;
        // Hide the definition manager
        setVisible(false);
    }//GEN-LAST:event_done

    /**
     *
     * @param evt
     */
    private void testDatasource(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_testDatasource
        Definition definition = new Definition((Database) this.cbxDatabases.getSelectedItem(),
                                                    this.txtDefinitionName.getText(),
                                                        this.txtDatasource.getText(),
                                                            this.txtUsername.getText(),
                                                                new String(this.txtPassword.getPassword()),
                                                                    this.chbAutoConnect.isSelected());
        try {
            if (definition.testConnection()) {
                JOptionPane.showMessageDialog(this, "The connection to the datasource was successful", "Test Datasource Successful", JOptionPane.INFORMATION_MESSAGE);
            } else {
                SQLException sqle = definition.getException();
                String exceptionMessage = "SQL Error Code: " + sqle.getErrorCode() + Constants.NEW_LINE + "SQL State: " + sqle.getSQLState() + Constants.NEW_LINE + "Message: " + sqle.getMessage();
                JOptionPane.showMessageDialog(this, exceptionMessage, "Test Datasource Unsuccessful", JOptionPane.ERROR_MESSAGE);
            }
        } catch (LoadDriverException e) {
            JOptionPane.showMessageDialog(this, e.getMessage(), "Test Datasource Unsuccessful", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_testDatasource
    
   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnDone;
    private javax.swing.JButton btnTestDatasource;
    private javax.swing.JComboBox cbxDatabases;
    private javax.swing.JCheckBox chbAutoConnect;
    private com.jm.commons.components.panel.ImagePanel mainPanel;
    private com.jm.commons.components.textfield.HintJTextField txtDatasource;
    private com.jm.commons.components.textfield.HintJTextField txtDefinitionName;
    private javax.swing.JPasswordField txtPassword;
    private com.jm.commons.components.textfield.HintJTextField txtUsername;
    // End of variables declaration//GEN-END:variables
    
}
