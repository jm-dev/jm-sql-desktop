/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jm.jmsql.database.popup.items;

import com.jm.jmsql.JMSqlApp;
import com.jm.jmsql.database.popup.DBXPloraPopUpMenu;
import com.jm.jmsql.xplora.Item;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JMenuItem;

/**
 *
 * @author Michael L.R. Marques
 * @param <T>
 */
public abstract class DBXPloraMenuItem<T extends Item> extends JMenuItem {
    
    private final DBXPloraPopUpMenu menu;
    private final T item;
    
    /**
     * 
     * @param menu
     * @param name
     * @param item 
     */
    public DBXPloraMenuItem(DBXPloraPopUpMenu menu, String name, T item) {
        this(menu, name, item, item.getIcon());
    }
    
    /**
     * 
     * @param menu
     * @param name
     * @param item
     * @param iconResource 
     */
    public DBXPloraMenuItem(DBXPloraPopUpMenu menu, String name, T item, String iconResource) {
        this(menu, name, item, new ImageIcon(DBXPloraMenuItem.class.getResource(iconResource)));
    }
    
    /**
     * 
     * @param menu
     * @param name
     * @param item
     * @param icon 
     */
    public DBXPloraMenuItem(DBXPloraPopUpMenu menu, String name, T item, Icon icon) {
        super(name);
        this.menu = menu;
        this.item = item;
        setIcon(icon);
        addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                action(getItem());
            }
        });
    }
    
    /**
     * 
     * @return 
     */
    public JMSqlApp getApplication() {
        return this.menu.getApplication();
    }

    /**
     * @return the menu
     */
    public DBXPloraPopUpMenu getMenu() {
        return this.menu;
    }

    /**
     * @return the item
     */
    public T getItem() {
        return item;
    }
    
    /**
     * 
     * @param item
     * @return
     */
    public abstract boolean action(T item);
    
}
