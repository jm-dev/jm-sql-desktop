/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jm.jmsql.database.popup;

import com.jm.commons.utils.Constants;
import com.jm.jmsql.JMSqlApp;
import com.jm.jmsql.database.DefinitionManager;
import com.jm.jmsql.database.popup.items.DBXPloraCheckBoxMenuItem;
import com.jm.jmsql.database.popup.items.DBXPloraMenuItem;
import com.jm.jmsql.sql.SQLEditor;
import com.jm.jmsql.xplora.Database;
import com.jm.jmsql.xplora.Definition;
import com.jm.jmsql.xplora.events.DefinitionEvent;
import com.jm.jmsql.xplora.events.DefinitionTestConnectionAdapter;
import com.jm.jmsql.xplora.exceptions.LoadDriverException;
import javax.swing.JOptionPane;
import javax.swing.JTree;
import org.apache.log4j.Logger;

/**
 *
 * @author Michael L.R. Marques
 * @param <T>
 */
public class DefinitionPopUpMenu<T extends Definition> extends LibraryPopUpMenu<T> {
    
    private static final Logger logger = Logger.getLogger(DefinitionPopUpMenu.class);
    
    /**
     * 
     * @param application
     * @param tree 
     */
    public DefinitionPopUpMenu(JMSqlApp application, JTree tree) {
        super(application, tree);
    }
    
    /**
     * 
     */
    @Override
    protected void initializeComponents() {
        if (!getSelectedTreeComponent().getParent().isDriverLoaded()) {
            try {
                getSelectedTreeComponent().getParent().getDriver();
            } catch (LoadDriverException lde) {
                logger.error(lde.getMessage(), lde);
            }
        }
        if (getSelectedTreeComponent().getParent().isDriverLoaded()) {
            DBXPloraMenuItem connect = new DBXPloraMenuItem<T>(this, "Connect", getSelectedTreeComponent(), "/connected_definition.png") {
                @Override
                public boolean action(T item) {
                    getItem().addDefinitionListener(new DefinitionTestConnectionAdapter() {
                        @Override
                        public void connectionSuccessful(DefinitionEvent e) {}
                        @Override
                        public void connectionFailed(DefinitionEvent e) {
                            JOptionPane.showMessageDialog(getApplication(), "Datasource: " + getItem().getDatasource() + Constants.NEW_LINE + "Username: " + getItem().getUsername() + Constants.NEW_LINE + getItem().getException().getMessage(), "Connection Error", JOptionPane.ERROR_MESSAGE);
                        }
                    });
                    getItem().asyncConnect();
                    return true;
                }
            };
            connect.setEnabled(!getSelectedTreeComponent().isConnected());
            add(connect);
            DBXPloraMenuItem disconnect = new DBXPloraMenuItem<T>(this, "Disconnect", getSelectedTreeComponent(), "/disconnected_definition.png") {
                @Override
                public boolean action(T item) {
                    if (!getItem().disconnect()) {
                        JOptionPane.showMessageDialog(getApplication(), getItem().getException().getMessage(), "Disconnect Error", JOptionPane.ERROR_MESSAGE);
                    }
                    return true;
                }
            };
            disconnect.setEnabled(getSelectedTreeComponent().isConnected());
            add(disconnect);
            addSeparator();
        }
        
        add(new DBXPloraMenuItem<T>(this, "Customize Definition...", getSelectedTreeComponent(), "/connected_definition.png") {
            @Override
            public boolean action(T item) {
                DefinitionManager definitionManager = new DefinitionManager(getMenu().getApplication(), getItem().getParent());
                definitionManager.setDefinition(getItem());
                if (DefinitionManager.SUCCESSFUL == definitionManager.showDialog()) {
                    Definition newDefinition = definitionManager.getDefinition();
                    if (newDefinition != null) {
                        if (getItem().update(newDefinition)) {
                            getTreeModel().fireTreeNodesChanged(getItem());
                            JOptionPane.showMessageDialog(getApplication(), "Successfully changed the selected definition", "Customize Definition", JOptionPane.INFORMATION_MESSAGE);
                        }
                    }
                }
                return true;
            }
        });
        
        add(new DBXPloraMenuItem<T>(this, "Remove Definition", getSelectedTreeComponent(), "/delete_definition.png") {
            @Override
            public boolean action(T item) {
                Database database = getItem().getParent();
                if (database.remove(getItem())) {
                    getTreeModel().fireStructureChanged(database);
                    JOptionPane.showMessageDialog(getApplication(), "Successfully removed the selected definition", "Remove Definition", JOptionPane.INFORMATION_MESSAGE);
                }
                return true;
            }
        });

        addSeparator();

        add(new DBXPloraCheckBoxMenuItem<T>(this, "Auto-Connect", getSelectedTreeComponent(), getSelectedTreeComponent().isAutoConnect()) {
            @Override
            public boolean action(T item) {
                getItem().setAutoConnect(isSelected());
                getTreeModel().fireTreeNodesChanged(getItem());
                return true;
            }
        });
        
        addSeparator();
        
        add(new DBXPloraMenuItem<T>(this, "Execute Query", getSelectedTreeComponent(), "/play.png") {
            @Override
            public boolean action(T item) {
                SQLEditor editor = new SQLEditor(getApplication(), getItem().getParent().getParent(), getItem());
                getMenu().getApplication().newSQLEditor("Execute Query.sql", editor);
                return true;
            }
        });
        
        super.initializeComponents();
    }
    
}
