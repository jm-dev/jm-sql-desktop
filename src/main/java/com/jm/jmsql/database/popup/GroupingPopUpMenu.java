/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jm.jmsql.database.popup;

import com.jm.jmsql.JMSqlApp;
import com.jm.jmsql.database.popup.items.RefreshMenuItem;
import com.jm.jmsql.xplora.Grouping;
import javax.swing.JTree;

/**
 *
 * @author Michael L.R. Marques
 * @param <T>
 */
public class GroupingPopUpMenu<T extends Grouping> extends DBXPloraPopUpMenu<T> {
    
    /**
     * 
     * @param application
     * @param tree 
     */
    public GroupingPopUpMenu(JMSqlApp application, JTree tree) {
        super(application, tree);
    }
    
    /**
     * 
     */
    @Override
    protected void initializeComponents() {
        if (getComponentCount() > 0) {
            addSeparator();
        }
        add(new RefreshMenuItem(this, getSelectedTreeComponent()));
    }
    
}
