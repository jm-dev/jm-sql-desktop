/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jm.jmsql.database.popup;

import com.jm.jmsql.JMSqlApp;
import com.jm.jmsql.database.popup.items.DBXPloraMenuItem;
import com.jm.jmsql.database.popup.items.RefreshMenuItem;
import com.jm.jmsql.xplora.Definition;
import com.jm.jmsql.xplora.Library;
import javax.swing.JTree;

/**
 *
 * @author Michael L.R. Marques
 * @param <T>
 */
public class LibraryPopUpMenu<T extends Library> extends DBXPloraPopUpMenu<T> {
    
    /**
     * 
     * @param application
     * @param tree 
     */
    public LibraryPopUpMenu(JMSqlApp application, JTree tree) {
        super(application, tree);
    }
    
    /**
     * 
     */
    @Override
    protected void initializeComponents() {
        if (getComponentCount() > 0) {
            addSeparator();
        }
        add(new RefreshMenuItem(this, getSelectedTreeComponent()));
        addSeparator();
        add(new DBXPloraMenuItem<T>(this, "Set as Default...", getSelectedTreeComponent()) {
            @Override
            public boolean action(T item) {
                if (item.getParent() instanceof Definition) {
                    Definition definition = (Definition) item.getParent();
                    definition.setDefaultLibrary(item.getName());
                }
                return true;
            }
        });
    }
    
}
