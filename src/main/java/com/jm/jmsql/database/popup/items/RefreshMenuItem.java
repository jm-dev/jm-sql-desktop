/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jm.jmsql.database.popup.items;

import com.jm.jmsql.database.popup.DBXPloraPopUpMenu;
import com.jm.jmsql.xplora.Item;
import javax.swing.ImageIcon;

/**
 *
 * @author Marquema
 */
public class RefreshMenuItem extends DBXPloraMenuItem<Item> {
    
    
    /**
     * 
     * @param menu
     * @param item 
     */
    public RefreshMenuItem(DBXPloraPopUpMenu menu, Item item) {
        super(menu, "Refresh", item, new ImageIcon(RefreshMenuItem.class.getResource("/refresh.png")));
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public boolean action(Item item) {
        if (getItem() == null) {
            return false;
        }
        getItem().reload();
        getMenu().getTreeModel().fireStructureChanged(getItem());
        return true;
    }
    
}
