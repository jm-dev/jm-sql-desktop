/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jm.jmsql.database.popup.items;

import com.jm.jmsql.JMSqlApp;
import com.jm.jmsql.database.popup.DBXPloraPopUpMenu;
import com.jm.jmsql.xplora.Item;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.JCheckBoxMenuItem;

/**
 *
 * @author Michael L.R. Marques
 * @param <T>
 */
public abstract class DBXPloraCheckBoxMenuItem<T extends Item> extends JCheckBoxMenuItem {
    
    private final DBXPloraPopUpMenu menu;
    private final T item;

    /**
     * 
     * @param menu
     * @param name
     * @param item
     */
    public DBXPloraCheckBoxMenuItem(DBXPloraPopUpMenu menu, String name, T item) {
        this(menu, name, item, false);
    }

    /**
     * 
     * @param menu
     * @param name
     * @param item
     * @param selected
     */
    public DBXPloraCheckBoxMenuItem(DBXPloraPopUpMenu menu, String name, T item, boolean selected) {
        super(name);
        this.menu = menu;
        this.item = item;
        setSelected(selected);
        addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                action(getItem());
            }
        });
    }
    
    /**
     * 
     * @return 
     */
    public JMSqlApp getApplication() {
        return this.menu.getApplication();
    }

    /**
     * @return the menu
     */
    public DBXPloraPopUpMenu getMenu() {
        return this.menu;
    }

    /**
     * @return the item
     */
    public T getItem() {
        return item;
    }
    
    /**
     * 
     * @param item
     * @return 
     */
    public abstract boolean action(T item);
    
}
