/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jm.jmsql.database.popup;

import com.jm.jmsql.JMSqlApp;
import com.jm.jmsql.database.DatabaseManager;
import com.jm.jmsql.database.DefinitionManager;
import com.jm.jmsql.database.popup.items.DBXPloraMenuItem;
import com.jm.jmsql.database.popup.items.RemoveChildMenuItem;
import com.jm.jmsql.xplora.Database;
import com.jm.jmsql.xplora.Definition;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JTree;

/**
 *
 * @author Michael L.R. Maurqes
 * @param <T>
 */
public class DatabasePopUpMenu<T extends Database> extends DBXPloraPopUpMenu<T> {
    
    /**
     * 
     * @param application
     * @param tree 
     */
    public DatabasePopUpMenu(JMSqlApp application, JTree tree) {
        super(application, tree);
    }
    
    /**
     * 
     */
    @Override
    protected void initializeComponents() {
        JMenuItem customize = new DBXPloraMenuItem<T>(this, "Customize Database...", getSelectedTreeComponent()) {
            @Override
            public boolean action(T item) {
                DatabaseManager driverManager = new DatabaseManager(getApplication());
                driverManager.setDatabase(getSelectedTreeComponent());
                if (DatabaseManager.SUCCESSFUL == driverManager.showDialog()) {
                    Database newDatabase = driverManager.getDatabase();
                    if (newDatabase != null) {
                        if (getSelectedTreeComponent().update(newDatabase)) {
                            getTreeModel().fireTreeNodesChanged(newDatabase);
                            JOptionPane.showMessageDialog(getApplication(), "Successfully changed the selected database", "Customize Database", JOptionPane.INFORMATION_MESSAGE);
                        }
                    }
                }
                return true;
            }
        };
        customize.setEnabled(!getSelectedTreeComponent().isSystemDatabase());
        
        JMenuItem remove = new RemoveChildMenuItem<T>(this, "Remove Database", getSelectedTreeComponent(), "/delete_database.png") {
            @Override
            public boolean action(T item) {
                if (super.action(item)) {
                    JOptionPane.showMessageDialog(getApplication(), "Successfully removed the selected database", "Remove Database", JOptionPane.INFORMATION_MESSAGE);
                }
                return true;
            }
        };
        remove.setEnabled(!getSelectedTreeComponent().isSystemDatabase());
        
        add(customize);
        add(remove);
        addSeparator();
        
        add(new DBXPloraMenuItem<T>(this, "Create Definition...", getSelectedTreeComponent(), "/create_definition.png") {
            @Override
            public boolean action(T item) {
                DefinitionManager definitionManager = new DefinitionManager(getApplication(), getSelectedTreeComponent());
                if (DefinitionManager.SUCCESSFUL == definitionManager.showDialog()) {
                    Definition definition = definitionManager.getDefinition();
                    if (definition != null) {
                        if (getSelectedTreeComponent().add(definitionManager.getDefinition())) {
                            getTreeModel().fireStructureChanged(definition.getParent());
                            JOptionPane.showMessageDialog(getApplication(), "Successfully added a definition", "Add Definition", JOptionPane.INFORMATION_MESSAGE);
                        }
                    }
                }
                return true;
            }
        });
    }
    
}
