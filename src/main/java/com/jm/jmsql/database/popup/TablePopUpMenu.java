/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jm.jmsql.database.popup;

import com.jm.jmsql.JMSqlApp;
import com.jm.jmsql.database.popup.items.DBXPloraMenuItem;
import com.jm.jmsql.database.popup.items.RefreshMenuItem;
import com.jm.jmsql.xplora.Databases;
import com.jm.jmsql.xplora.Definition;
import com.jm.jmsql.xplora.Table;
import com.jm.jmsql.sql.SQLEditor;
import javax.swing.JOptionPane;
import javax.swing.JTree;

/**
 *
 * @author Marquema
 * @param <T>
 */
public class TablePopUpMenu<T extends Table> extends DBXPloraPopUpMenu<T> {
    
    /**
     * 
     * @param application
     * @param tree 
     */
    public TablePopUpMenu(JMSqlApp application, JTree tree) {
        super(application, tree);
    }
    
    /**
     * 
     */
    @Override
    protected void initializeComponents() {
        add(new DBXPloraMenuItem<T>(this, "View Table Data", getSelectedTreeComponent()) {
            @Override
            public boolean action(T item) {
                getApplication().newSQLEditor("Select " + getItem().getName() + ".sql",
                        new SQLEditor(getApplication(),
                                        getItem().getParent(Databases.class),
                                            getItem().getParent(Definition.class),
                                                getItem().selectSQLQuery(), true));
                return true;
            }
        });
        add(new DBXPloraMenuItem<T>(this, "Delete Table", getSelectedTreeComponent(), "/delete_table.png") {
            @Override
            public boolean action(T item) {
                if (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(getApplication(), "Are you sure you would like to delete this table?", "Delete Table " + getItem().getName(), JOptionPane.YES_NO_OPTION)) {
                        getApplication().newSQLEditor("Delete " + getItem().getName() + ".sql",
                                new SQLEditor(getApplication(), 
                                                getItem().getParent(Databases.class),
                                                    getItem().getParent(Definition.class),
                                                        "drop table " + getItem().getName(), true));
                        getItem().reload();
                        getTreeModel().fireStructureChanged(getItem());
                    }
                return true;
            }
        });
        addSeparator();
        add(new DBXPloraMenuItem<T>(this, "Select SQL Query", getSelectedTreeComponent()) {
            @Override
            public boolean action(T item) {
                getApplication().newSQLEditor("Select " + getItem().getName() + ".sql",
                        new SQLEditor(getApplication(),
                                            getItem().getParent(Databases.class),
                                                getItem().getParent(Definition.class),
                                                    getItem().selectSQLQuery(), false));
                return true;
            }
        });
        add(new DBXPloraMenuItem<T>(this, "Insert SQL Query", getSelectedTreeComponent()) {
            @Override
            public boolean action(T item) {
                getApplication().newSQLEditor("Insert " + getItem().getName() + ".sql",
                                new SQLEditor(getApplication(), 
                                                getItem().getParent(Databases.class),
                                                    getItem().getParent(Definition.class),
                                                        getItem().insertSQLQuery(), false));
                return true;
            }
        });
        add(new DBXPloraMenuItem<T>(this, "Update SQL Query", getSelectedTreeComponent()) {
            @Override
            public boolean action(T item) {
                getApplication().newSQLEditor("Update " + getItem().getName() + ".sql",
                                new SQLEditor(getApplication(),
                                                getItem().getParent(Databases.class),
                                                    getItem().getParent(Definition.class),
                                                        getItem().updateSQLQuery(), false));
                return true;
            }
        });
        add(new DBXPloraMenuItem<T>(this, "Delete SQL Query", getSelectedTreeComponent()) {
            @Override
            public boolean action(T item) {
                getApplication().newSQLEditor("Delete " + getItem().getName() + ".sql",
                                new SQLEditor(getApplication(), 
                                                getItem().getParent(Databases.class),
                                                    getItem().getParent(Definition.class),
                                                        getItem().deleteSQLQuery(), false));
                return true;
            }
        });
        add(new DBXPloraMenuItem<T>(this, "Create Table SQL Query", getSelectedTreeComponent(), "/table_structure.png") {
            @Override
            public boolean action(T item) {
                getApplication().newSQLEditor("Create " + getItem().getName() + ".sql",
                                new SQLEditor(getApplication(), 
                                                getItem().getParent(Databases.class),
                                                    getItem().getParent(Definition.class),
                                                        getItem().createSQLQuery(), false));
                return true;
            }
        });
        addSeparator();
        add(new RefreshMenuItem(this, null));
    }
    
}
