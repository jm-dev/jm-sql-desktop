/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jm.jmsql.database.popup;

import com.jm.jmsql.JMSqlApp;
import com.jm.jmsql.xplora.Column;
import com.jm.jmsql.xplora.Database;
import com.jm.jmsql.xplora.Databases;
import com.jm.jmsql.xplora.Definition;
import com.jm.jmsql.xplora.Procedure;
import com.jm.jmsql.xplora.ProcedureColumn;
import com.jm.jmsql.xplora.Table;
import com.jm.jmsql.xplora.Tables;
import com.jm.jmsql.xplora.View;
import com.jm.jmsql.xplora.Grouping;
import com.jm.jmsql.xplora.Item;
import com.jm.jmsql.xplora.Library;
import javax.swing.JTree;

/**
 *
 * @author Michael L.R. Marques
 */
public class DBXPloraPopupFactory {
    
    private static DBXPloraPopupFactory factory;
    
    /**
     * 
     */
    private DBXPloraPopupFactory() {
       super(); 
    }
    
    /**
     * 
     * @return 
     */
    public static DBXPloraPopupFactory getInstance() {
        if (factory == null ) {
           factory = new DBXPloraPopupFactory();
       }
        return factory;
    }
    
    /**
     * 
     * @param app
     * @param tree
     * @return 
     */
    public DBXPloraPopUpMenu getMenu(JMSqlApp app, JTree tree) {
        if (tree.getLastSelectedPathComponent() instanceof Item) {
            Item item = (Item) tree.getLastSelectedPathComponent();
            if (item instanceof Databases) {
                return new DatabasesPopUpMenu(app, tree);
            } else if (item instanceof Database) {
                return new DatabasePopUpMenu(app, tree);
            } else if (item instanceof Definition) {
                return new DefinitionPopUpMenu(app, tree);
            } else if (item instanceof Library) {
                return new LibraryPopUpMenu(app, tree);
            } else if (item instanceof Tables) {
                return new TablesPopUpMenu(app, tree);
            } else if (item instanceof Grouping) {
                return new GroupingPopUpMenu(app, tree);
            } else if (item instanceof View) {
                return new ViewPopUpMenu(app, tree);
            } else if (item instanceof Procedure) {
                return new DefaultPopUpMenu(app, tree);
            } else if (item instanceof Table) {
                return new TablePopUpMenu(app, tree);
            } else if (item instanceof ProcedureColumn) {
                return new DefaultPopUpMenu(app, tree);
            } else if (item instanceof Column) {
                return new ColumnPopUpMenu(app, tree);
            } else {
                return new DefaultPopUpMenu(app, tree);
            }
        }
        return null;
    }
    
    class DefaultPopUpMenu extends DBXPloraPopUpMenu<Item> {
        
        public DefaultPopUpMenu(JMSqlApp application, JTree tree) {
           super(application, tree);
        }
        
        @Override
        protected void initializeComponents() {

        }

    }
    
}
