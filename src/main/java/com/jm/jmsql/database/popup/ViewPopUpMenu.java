/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jm.jmsql.database.popup;

import com.jm.jmsql.JMSqlApp;
import com.jm.jmsql.database.popup.items.DBXPloraMenuItem;
import com.jm.jmsql.database.popup.items.RefreshMenuItem;
import com.jm.jmsql.sql.SQLEditor;
import com.jm.jmsql.xplora.Databases;
import com.jm.jmsql.xplora.Definition;
import com.jm.jmsql.xplora.Library;
import com.jm.jmsql.xplora.View;
import javax.swing.JTree;

/**
 *
 * @author Michael L.R. Marques
 * @param <T>
 */
public class ViewPopUpMenu<T extends View> extends DBXPloraPopUpMenu<T> {
    
    /**
     * 
     * @param application
     * @param tree 
     */
    public ViewPopUpMenu(JMSqlApp application, JTree tree) {
        super(application, tree);
    }
    
    /**
     * 
     */
    @Override
    protected void initializeComponents() {
        add(new DBXPloraMenuItem<T>(this, "View Data", getSelectedTreeComponent()) {
            @Override
            public boolean action(T item) {
                StringBuilder builder = new StringBuilder("SELECT *\nFROM ");
                if (!(getItem().getParent(Library.class) instanceof Definition)) {
                    builder.append(getItem().getParent().getName());
                    builder.append(".");
                }
                builder.append(getItem().getName());
                
                Databases databases = (Databases) getItem().getParent(Databases.class);
                getApplication().newSQLEditor("View " + getItem().getName() + ".sql", new SQLEditor(getApplication(), databases, (Definition) getItem().getParent(Definition.class), builder.toString(), true));
                return true;
            }
        });
        addSeparator();
        add(new RefreshMenuItem(this, getSelectedTreeComponent()));
    }
    
}
