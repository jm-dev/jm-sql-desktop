/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jm.jmsql.database.popup;

import com.jm.jmsql.JMSqlApp;
import com.jm.jmsql.database.CreateTable;
import com.jm.jmsql.database.popup.items.DBXPloraMenuItem;
import com.jm.jmsql.xplora.Tables;
import javax.swing.JTree;

/**
 *
 * @author Michael L.R. Marques
 * @param <T>
 */
public class TablesPopUpMenu<T extends Tables> extends GroupingPopUpMenu<T> {
    
    /**
     * 
     * @param application
     * @param tree 
     */
    public TablesPopUpMenu(JMSqlApp application, JTree tree) {
        super(application, tree);
    }
    
    /**
     * 
     */
    @Override
    protected void initializeComponents() {
        add(new DBXPloraMenuItem<T>(this, "Create Table", getSelectedTreeComponent(), "/create_table.png") {
            @Override
            public boolean action(T item) {
                CreateTable create = new CreateTable(getApplication(), getSelectedTreeComponent());
                create.setVisible(true);
                getSelectedTreeComponent().reload();
                getTreeModel().fireStructureChanged(getSelectedTreeComponent());
                return true;
            }
        });
        super.initializeComponents();
    }
    
}
