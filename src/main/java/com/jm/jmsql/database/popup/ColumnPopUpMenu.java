/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jm.jmsql.database.popup;

import com.jm.jmsql.JMSqlApp;
import com.jm.jmsql.database.popup.items.DBXPloraMenuItem;
import com.jm.jmsql.sql.SQLEditor;
import com.jm.jmsql.xplora.Column;
import com.jm.jmsql.xplora.Databases;
import com.jm.jmsql.xplora.Definition;
import com.jm.jmsql.xplora.Library;
import com.jm.jmsql.xplora.Table;
import javax.swing.JTree;

/**
 *
 * @author Marquema
 * @param <T>
 */
public class ColumnPopUpMenu<T extends Column<Table>> extends DBXPloraPopUpMenu<T> {
    
    /**
     * 
     * @param application
     * @param tree 
     */
    public ColumnPopUpMenu(JMSqlApp application, JTree tree) {
        super(application, tree);
    }
    
    /**
     * 
     */
    @Override
    protected void initializeComponents() {
        add(new DBXPloraMenuItem<T>(this, "View Column Data", getSelectedTreeComponent()) {
            @Override
            public boolean action(T item) {
                StringBuilder builder = new StringBuilder("SELECT ");
                builder.append(getItem().getName());
                builder.append(" FROM ");
                if (!(getItem().getParent(Library.class) instanceof Definition)) {
                    builder.append(getItem().getParent().getName());
                    builder.append(".");
                }
                builder.append(getItem().getParent().getName());
                
                getApplication().newSQLEditor("Select " + getItem().getName() + ".sql",
                        new SQLEditor(getApplication(), getItem().getParent(Databases.class), getItem().getParent(Definition.class), builder.toString(), true));
                return true;
            }
        });
    }
    
}
