/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jm.jmsql.database.popup.items;

import com.jm.jmsql.database.popup.DBXPloraPopUpMenu;
import com.jm.jmsql.xplora.Item;
import java.util.Collections;

/**
 *
 * @author Michael L.R. Marques
 * @param <T>
 */
public class SortMenuItem<T extends Item> extends DBXPloraMenuItem<T> {
    
    /**
     * 
     * @param menu
     * @param item 
     */
    public SortMenuItem(DBXPloraPopUpMenu menu, T item) {
        super(menu, "Sort...", item, "/sort.png");
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public boolean action(T item) {
        Collections.sort(getItem());
        getMenu().getTreeModel().fireTreeNodesChanged(getItem());
        return true;
    }
    
}
