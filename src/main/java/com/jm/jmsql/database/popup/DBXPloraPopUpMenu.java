/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jm.jmsql.database.popup;

import com.jm.jmsql.JMSqlApp;
import com.jm.jmsql.database.popup.items.PropertiesMenuItem;
import com.jm.jmsql.database.popup.items.SortMenuItem;
import com.jm.jmsql.models.DBXploraTreeModel;
import com.jm.jmsql.xplora.Item;
import java.awt.Component;
import javax.swing.JPopupMenu;
import javax.swing.JTree;

/**
 *
 * @author Marquema
 * @param <T>
 */
public abstract class DBXPloraPopUpMenu<T extends Item> extends JPopupMenu {
    
    private final JMSqlApp application;
    private final JTree tree;
    
    /**
     * 
     * @param application
     * @param tree 
     */
    public DBXPloraPopUpMenu(final JMSqlApp application, final JTree tree) {
        this.application = application;
        this.tree = tree;
    }
    
    /**
     * @return the application
     */
    public JMSqlApp getApplication() {
        return this.application;
    }

    /**
     * @return the tree
     */
    public JTree getTree() {
        return this.tree;
    }
    
    /**
     * 
     * @return 
     */
    public DBXploraTreeModel getTreeModel() {
        return (DBXploraTreeModel) this.tree.getModel();
    }

    /**
     * @return 
     */
    protected T getSelectedTreeComponent() {
        return (T) this.tree.getLastSelectedPathComponent();
    }
    
    /**
     * 
     */
    protected abstract void initializeComponents();
    
    /**
     * 
     */
    private void defaultComponents() {
        if (getSelectedTreeComponent().isSortable()) {
            if (getComponentCount() > 0) {
                addSeparator();
            }
            add(new SortMenuItem<>(this, getSelectedTreeComponent()));
        }
        if (getComponentCount() > 0) {
            addSeparator();
        }
        add(new PropertiesMenuItem<>(this, getSelectedTreeComponent()));
    }
    
    /**
     * 
     */
    @Override
    @SuppressWarnings("deprecated")
    public void show() {
        show(0, 0);
    }
    
    /**
     * 
     * @param x
     * @param y 
     */
    public void show(int x, int y) {
        show(getTree(), x, y);
    }
    
    /**
     * 
     * @param x
     * @param y 
     */
    @Override
    public void show(Component invoker, int x, int y) {
        initializeComponents();
        defaultComponents();
        super.show(invoker, x, y);
    }
    
}
