/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jm.jmsql.database.popup.items;

import com.jm.jmsql.database.popup.DBXPloraPopUpMenu;
import com.jm.jmsql.xplora.Item;

/**
 *
 * @author Marquema
 * @param <T>
 */
public class RemoveChildMenuItem<T extends Item> extends DBXPloraMenuItem<T> {
    
    /**
     * 
     * @param menu
     * @param name
     * @param item 
     */
    public RemoveChildMenuItem(DBXPloraPopUpMenu menu, String name, T item) {
        super(menu, name, item);
    }
    
    /**
     * 
     * @param menu
     * @param name
     * @param item
     * @param iconResource 
     */
    public RemoveChildMenuItem(DBXPloraPopUpMenu menu, String name, T item, String iconResource) {
        super(menu, name, item, iconResource);
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public boolean action(T item) {
        if (getItem().getParent().remove(getItem())) {
            getMenu().getTreeModel().fireTreeNodesRemoved(getItem());
            return true;
        }
        return false;
    }
    
}
