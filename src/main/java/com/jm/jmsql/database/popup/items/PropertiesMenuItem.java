/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jm.jmsql.database.popup.items;

import com.jm.jmsql.database.Properties;
import com.jm.jmsql.database.popup.DBXPloraPopUpMenu;
import com.jm.jmsql.xplora.Item;

/**
 *
 * @author Michael L.R. Marques
 * @param <T>
 */
public class PropertiesMenuItem<T extends Item> extends DBXPloraMenuItem<T> {
    
    /**
     * 
     * @param menu
     * @param item 
     */
    public PropertiesMenuItem(DBXPloraPopUpMenu menu, T item) {
        super(menu, "Properties", item, "/database_properties.png");
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public boolean action(T item) {
        Properties.showDialog(getMenu().getApplication(), item);
        return true;
    }
    
}
