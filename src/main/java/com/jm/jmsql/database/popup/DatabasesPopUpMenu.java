/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jm.jmsql.database.popup;

import com.jm.jmsql.database.popup.items.DBXPloraMenuItem;
import com.jm.jmsql.JMSqlApp;
import com.jm.jmsql.database.DatabaseManager;
import com.jm.jmsql.xplora.Database;
import com.jm.jmsql.xplora.Databases;
import javax.swing.JOptionPane;
import javax.swing.JTree;

/**
 *
 * @author Marquema
 * @param <T>
 */
public class DatabasesPopUpMenu<T extends Databases> extends DBXPloraPopUpMenu<T> {

    /**
     * 
     * @param application
     * @param tree 
     */
    public DatabasesPopUpMenu(JMSqlApp application, JTree tree) {
        super(application, tree);
    }
    
    /**
     * 
     */
    @Override
    protected void initializeComponents() {
        add(new DBXPloraMenuItem<T>(this, "Create Database...", getSelectedTreeComponent()) {
            @Override
            public boolean action(T item) {
                DatabaseManager driverManager = new DatabaseManager(getApplication());
                if (DatabaseManager.SUCCESSFUL == driverManager.showDialog()) {
                    Database database = driverManager.getDatabase();
                    if (database != null) {
                        if (getSelectedTreeComponent().add(database)) {
                            getTreeModel().fireStructureChanged(database.getParent());
                            JOptionPane.showMessageDialog(getApplication(), "Successfully added a database", "Add Database", JOptionPane.INFORMATION_MESSAGE);
                        }
                    }
                }
                return true;
            }
        });
    }
    
}
