/*
 * Copyright (C) 2012-2013 Michael L.R. Marques
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Contact: michaellrmarques@gmail.com
 */
package com.jm.jmsql;

import com.jm.commons.components.JMFrame;
import com.jm.commons.settings.SettingsException;
import com.jm.jmsql.about.About;
import com.jm.jmsql.about.HelpSystem;
import com.jm.jmsql.about.Welcome;
import com.jm.jmsql.cloud.CloudLogin;
import com.jm.jmsql.cloud.CloudRegister;
import com.jm.jmsql.database.popup.DBXPloraPopUpMenu;
import com.jm.jmsql.database.popup.DBXPloraPopupFactory;
import com.jm.jmsql.listener.FileOpenEvent;
import com.jm.jmsql.listener.FileOpenListener;
import com.jm.jmsql.listener.InteractiveListener;
import static com.jm.jmsql.listener.InteractiveListener.DEFAULT_IP;
import static com.jm.jmsql.listener.InteractiveListener.DEFAULT_PORT;
import static com.jm.jmsql.listener.InteractiveListener.OK;
import com.jm.jmsql.models.DBXploraTreeModel;
import com.jm.jmsql.options.Options;
import com.jm.jmsql.sql.SQLEditor;
import com.jm.jmsql.sql.SQLFileDropTarget;
import com.jm.jmsql.sql.SQLFileOperations;
import com.jm.jmsql.tabulation.TabPageHeader;
import static com.jm.jmsql.utils.Constants.Colours.LIGHT_BLUE;
import static com.jm.jmsql.utils.Constants.DEFAULT_FONT;
import com.jm.jmsql.utils.Pages;
import com.jm.jmsql.utils.Settings;
import com.jm.jmsql.xplora.Database;
import com.jm.jmsql.xplora.Databases;
import com.jm.jmsql.xplora.Definition;
import com.jm.jmsql.xplora.events.DefinitionAdapter;
import com.jm.jmsql.xplora.events.DefinitionEvent;
import static java.awt.AlphaComposite.Clear;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Graphics2D;
import static java.awt.RenderingHints.KEY_TEXT_ANTIALIASING;
import static java.awt.RenderingHints.VALUE_TEXT_ANTIALIAS_ON;
import java.awt.SplashScreen;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import javax.imageio.ImageIO;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.ERROR_MESSAGE;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreePath;
import javax.xml.bind.JAXBException;
import jsyntaxpane.syntaxkits.SqlSyntaxKit;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 * 
 * @author Michael L.R. Marques
 */
public class JMSqlApp extends JMFrame implements FileOpenListener {
    
    /**
     * Logging Framework
     */
    private static final Logger log = Logger.getLogger(Settings.class);
    
    // Splash screen objectes
    private static SplashScreen splashScreen;
    private static Graphics2D splashScreenGraphics;
    private static BufferedImage splashScreenImage;
    
    private Databases databases;
    private boolean loggedIn;
    private String user;
    private String password;
    
    /**
     * Creates new form JMSql.
     */
    public JMSqlApp() {
        // Initialize components
        renderSplashScreen("Loading JMSql Components...", 30);
        initComponents();
        // Set application icon
        try {
            setIconImage(ImageIO.read(getClass().getResource("/icon.png")));
        } catch (IOException ioe) {
            log.error(ioe.getMessage(), ioe);
        }
        renderSplashScreen("Starting JMSql...", 35);
        // Set the applications title
        setTitle(Settings.getApplicationName() + " " + Settings.getApplicationVersion());
        setBounds(Settings.getApplicationBounds() == null ? Settings.getApplicationBoundsDefault() : Settings.getApplicationBounds());
        renderSplashScreen("Starting JMSql...", 40);
        // Set the welcome pate
        if (Settings.showStartupPage()) {
            startupPage(null);
        }
        // 
        this.mainSplitPanel.setDividerLocation(Settings.getDBXploraWidth());
        renderSplashScreen("Starting JMSql...", 45);
        // Set the Syntax kit
        SqlSyntaxKit.initKit();
        renderSplashScreen("Starting JMSql...", 50);
        this.jMenu5.setVisible(false); // TODO: Testing
        setDropTarget(new SQLFileDropTarget(this));
        log.debug("Auto-login");
        if (Settings.isRememberLogin()) {
            renderSplashScreen("Logging into JMSql Cloud...", 60);
            CloudLogin login = CloudLogin.login(this);
            if (login.isLoggedIn()) {
                setLoggedIn(login.isLoggedIn());
                setUser(login.getUser());
                setPassword(login.getPassword());

//                CloudOperations cloudOperations = new CloudOperations();
//                this.databases = cloudOperations.getDBXplora(Databases.class, getUser());
            }
        } else {
            log.debug("Loading Cached DB Xplora Data");
            renderSplashScreen("Loading DB Xplora Data...", 60);
            try {
                this.databases = Databases.generate(Settings.getDatabasesFile());
            } catch (JAXBException jaxbe) {
                log.error(jaxbe.getMessage(), jaxbe);
            }
        }
        renderSplashScreen("Initialize the DBXplora Tree...", 70);
        this.treeDBXplora.setModel(new DBXploraTreeModel(this.databases));
        this.treeDBXplora.setCellRenderer(new DBXploraTreeModel.DBXploraTreeCellRenderer());
        this.treeDBXplora.setCellEditor(new DBXploraTreeModel.DBXploraTreeCellEditor(this.treeDBXplora));
        for (Database database : this.databases) {
            this.treeDBXplora.expandPath(new TreePath(database.getPath()));
            for (Definition definition : database) {
                if (definition.isAutoConnect()) {
                    definition.asyncConnect();
                }
            }
        }
        renderSplashScreen("Finalizing...", 80);
        // Set the definition connection events
        for (Definition definition : this.databases.getDefinitions()) {
            definition.addDefinitionListener(new DefinitionAdapter() {
                @Override
                public void connectionSuccessful(DefinitionEvent e) {
                    if (e.getDefinition().isConnected()) {
                        e.getDefinition().reload();
                        ((DBXploraTreeModel) treeDBXplora.getModel()).fireStructureChanged(e.getDefinition());
                    }
                }
                @Override
                public void disconnected(DefinitionEvent e) {
                    if (!e.getDefinition().isConnected()) {
                        e.getDefinition().clear();
                        ((DBXploraTreeModel) treeDBXplora.getModel()).fireStructureChanged(e.getDefinition());
                    }
                }
                @Override
                public void connected(DefinitionEvent e) {
                    if (e.getDefinition().isConnected()) {
                        if (!treeDBXplora.isExpanded(new TreePath(e.getDefinition().getPath()))) {
                            treeDBXplora.expandPath(new TreePath(e.getDefinition().getPath()));
                        }
                    }
                }
            });
        }
        this.treeDBXplora.getModel().addTreeModelListener(new TreeModelListener() {
            @Override
            public void treeNodesChanged(TreeModelEvent e) {
                databases.save();
            }
            @Override
            public void treeNodesInserted(TreeModelEvent e) {
                databases.save();
            }
            @Override
            public void treeNodesRemoved(TreeModelEvent e) {
                databases.save();
            }
            @Override
            public void treeStructureChanged(TreeModelEvent e) {
                databases.save();
            }
        });
        renderSplashScreen("Loading Editors...", 90);
        for (File page : Pages.getInstance().getPages()) {
            newSQLEditor(page);
        }

        InteractiveListener interactiveListener = new InteractiveListener();
        interactiveListener.addFileOpenListener(this);
        Thread interactiveListenerThread = new Thread(interactiveListener);
        interactiveListenerThread.setDaemon(true);
        interactiveListenerThread.start();
    }
    
    /**
     *
     * @return
     */
    public Databases getDatabases() {
        return this.databases;
    }

    /**
     * @return the loggedIn
     */
    public boolean isLoggedIn() {
        return loggedIn;
    }

    /**
     * @param loggedIn the loggedIn to set
     */
    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    /**
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }
    
    /**
     * Is the editor selected.
     * 
     * @return 
     */
    public boolean isEditorSelected() {
        return this.tabPages.getSelectedComponent() != null &&
                    this.tabPages.getSelectedComponent() instanceof SQLEditor;
    }
    
    /**
     * 
     * @return 
     */
    public boolean containsEditor() {
        return getEditors().size() > 0;
    }
    
    /**
     * 
     * @param index 
     */
    public void setSelectedEditor(int index) {
        this.tabPages.setSelectedIndex(index);
    }
    
    /**
     * 
     * @param editor 
     */
    public void setSelectedEditor(SQLEditor editor) {
        this.tabPages.setSelectedComponent(editor);
    }
    
    /**
     * 
     * @return 
     */
    public SQLEditor getSelectedEditor() {
        return (SQLEditor) this.tabPages.getSelectedComponent();
    }
    
    /**
     * 
     * @return 
     */
    public List<SQLEditor> getEditors() {
        List<SQLEditor> editors = new ArrayList();
        for (int i = 0; i < this.tabPages.getTabCount(); i++) {
            if (((TabPageHeader) this.tabPages.getTabComponentAt(i)).getComponent() instanceof SQLEditor) {
                editors.add((SQLEditor) ((TabPageHeader) this.tabPages.getTabComponentAt(i)).getComponent());
            }
        }
        return editors;
    }
    
    /**
     * 
     */
    public void newSQLEditor() {
        newSQLEditor(new SQLEditor(this, this.databases));
    }

    /**
     * 
     * @param sql
     */
    public void newSQLEditor(String sql) {
        SQLEditor editor = new SQLEditor(this, this.databases, sql);
        this.tabPages.setSelectedComponent(this.tabPages.add(editor));
        this.tabPages.setTabComponentAt(this.tabPages.getSelectedIndex(), new TabPageHeader(this, this.tabPages, editor));
    }

    /**
     * 
     * @param file
     */
    public void newSQLEditor(File file) {
        try {
            SQLEditor editor = new SQLEditor(this, this.databases, file);
            this.tabPages.setSelectedComponent(this.tabPages.add(editor));
            this.tabPages.setTabComponentAt(this.tabPages.getSelectedIndex(), new TabPageHeader(this, this.tabPages, editor, file.getName(), file.getAbsolutePath()));
        } catch (IOException ioe) {
            log.error(ioe.getMessage());
        }
    }
    
    /**
     * 
     * @param editor 
     */
    public void newSQLEditor(SQLEditor editor) {
        newSQLEditor(SQLEditor.NEW_SQL_EDITOR, editor);
    }
    
    /**
     * 
     * @param tabText
     * @param editor 
     */
    public void newSQLEditor(String tabText, SQLEditor editor) {
        this.tabPages.setSelectedComponent(this.tabPages.add(editor));
        this.tabPages.setTabComponentAt(this.tabPages.getSelectedIndex(), new TabPageHeader(this, this.tabPages, editor, tabText));
    }
    
    /**
     * 
     */
    public void openSQLEditor() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Open SQL File...");
        fileChooser.setCurrentDirectory(Settings.getSQLLocation());
        fileChooser.setFileFilter(SQLFileOperations.SQLFileFilter.getInstance());
        fileChooser.setMultiSelectionEnabled(true);
        if (JFileChooser.APPROVE_OPTION == fileChooser.showOpenDialog(this)) {
            for (File file : fileChooser.getSelectedFiles()) {
                newSQLEditor(file);
            }
            Settings.setSQLLocation(fileChooser.getCurrentDirectory());
        }
    }
    
    /**
     * 
     */
    public void saveSQL() {
        if (isEditorSelected()) {
            getSelectedEditor().saveSQLFile();
        }
    }
    
    /**
     * 
     */
    public void saveAsSQL() {
        if (isEditorSelected()) {
            getSelectedEditor().saveSQLFile(true);
        }
    }
    
    /**
     * 
     */
    public void saveAllSQL() {                            
        for (SQLEditor editor : getEditors()) {
            editor.saveSQLFile();
        }
    }

    /**
     *
     * @param event
     */
    @Override
    public void openFile(FileOpenEvent event) {
        newSQLEditor(event.getFile());
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jToolBar1 = new javax.swing.JToolBar();
        btnNew = new javax.swing.JButton();
        btnOpen = new javax.swing.JButton();
        btnSaveAll = new javax.swing.JButton();
        mainSplitPanel = new javax.swing.JSplitPane();
        scpDBXploraModel = new javax.swing.JScrollPane();
        treeDBXplora = new javax.swing.JTree();
        tabPages = new javax.swing.JTabbedPane();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        jMenuItem5 = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenuItem8 = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        jMenuItem9 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        mnuUndo = new javax.swing.JMenuItem();
        mnuRedo = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem11 = new javax.swing.JMenuItem();
        jMenuItem10 = new javax.swing.JMenuItem();
        jSeparator5 = new javax.swing.JPopupMenu.Separator();
        jMenuItem12 = new javax.swing.JMenuItem();
        jSeparator6 = new javax.swing.JPopupMenu.Separator();
        jMenuItem15 = new javax.swing.JMenuItem();
        jSeparator7 = new javax.swing.JPopupMenu.Separator();
        jMenuItem2 = new javax.swing.JMenuItem();
        jSeparator12 = new javax.swing.JPopupMenu.Separator();
        jMenuItem16 = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        mnuExecute = new javax.swing.JMenuItem();
        jSeparator10 = new javax.swing.JPopupMenu.Separator();
        mnuXPort = new javax.swing.JMenuItem();
        jSeparator9 = new javax.swing.JPopupMenu.Separator();
        jMenuItem14 = new javax.swing.JMenuItem();
        jMenu5 = new javax.swing.JMenu();
        mnuLogin = new javax.swing.JMenuItem();
        jSeparator11 = new javax.swing.JPopupMenu.Separator();
        mnuRegister = new javax.swing.JMenuItem();
        jMenu4 = new javax.swing.JMenu();
        mnuHelp = new javax.swing.JMenuItem();
        jSeparator8 = new javax.swing.JPopupMenu.Separator();
        mnuStartupPage = new javax.swing.JMenuItem();
        jMenuItem18 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("JMSql Scripting Tool");
        setLocationByPlatform(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                applicationClosing(evt);
            }
        });

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);

        btnNew.setIcon(new javax.swing.ImageIcon(getClass().getResource("/new.png"))); // NOI18N
        btnNew.setToolTipText("New (Ctrl+Enter)");
        btnNew.setFocusable(false);
        btnNew.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnNew.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newSQLEditor(evt);
            }
        });
        jToolBar1.add(btnNew);

        btnOpen.setIcon(new javax.swing.ImageIcon(getClass().getResource("/open.png"))); // NOI18N
        btnOpen.setToolTipText("Open... (Ctrl+O)");
        btnOpen.setFocusable(false);
        btnOpen.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnOpen.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnOpen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openSQLEditor(evt);
            }
        });
        jToolBar1.add(btnOpen);

        btnSaveAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/save_all.png"))); // NOI18N
        btnSaveAll.setToolTipText("Save All (Ctrl+Shift+S)");
        btnSaveAll.setFocusable(false);
        btnSaveAll.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnSaveAll.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnSaveAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveAllSQL(evt);
            }
        });
        jToolBar1.add(btnSaveAll);

        mainSplitPanel.setDividerLocation(205);
        mainSplitPanel.setOneTouchExpandable(true);

        javax.swing.tree.DefaultMutableTreeNode treeNode1 = new javax.swing.tree.DefaultMutableTreeNode("root");
        treeDBXplora.setModel(new javax.swing.tree.DefaultTreeModel(treeNode1));
        treeDBXplora.setDoubleBuffered(true);
        treeDBXplora.setEditable(true);
        treeDBXplora.setLargeModel(true);
        treeDBXplora.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                treeDBXploraMouseClicked(evt);
            }
        });
        treeDBXplora.addTreeWillExpandListener(new javax.swing.event.TreeWillExpandListener() {
            public void treeWillCollapse(javax.swing.event.TreeExpansionEvent evt)throws javax.swing.tree.ExpandVetoException {
            }
            public void treeWillExpand(javax.swing.event.TreeExpansionEvent evt)throws javax.swing.tree.ExpandVetoException {
                treeDBXploraTreeWillExpand(evt);
            }
        });
        scpDBXploraModel.setViewportView(treeDBXplora);

        mainSplitPanel.setLeftComponent(scpDBXploraModel);

        tabPages.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        tabPages.setOpaque(true);
        mainSplitPanel.setRightComponent(tabPages);

        jMenu1.setText("File");

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_ENTER, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/new.png"))); // NOI18N
        jMenuItem1.setText("New");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newSQLEditor(evt);
            }
        });
        jMenu1.add(jMenuItem1);
        jMenu1.add(jSeparator1);

        jMenuItem5.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/open.png"))); // NOI18N
        jMenuItem5.setText("Open...");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openSQLEditor(evt);
            }
        });
        jMenu1.add(jMenuItem5);
        jMenu1.add(jSeparator2);

        jMenuItem6.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/save.png"))); // NOI18N
        jMenuItem6.setText("Save");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveSQL(evt);
            }
        });
        jMenu1.add(jMenuItem6);

        jMenuItem7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/save.png"))); // NOI18N
        jMenuItem7.setText("Save As...");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveAsSQL(evt);
            }
        });
        jMenu1.add(jMenuItem7);

        jMenuItem8.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/save_all.png"))); // NOI18N
        jMenuItem8.setText("Save All");
        jMenuItem8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveAllSQL(evt);
            }
        });
        jMenu1.add(jMenuItem8);
        jMenu1.add(jSeparator3);

        jMenuItem9.setText("Exit");
        jMenuItem9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exit(evt);
            }
        });
        jMenu1.add(jMenuItem9);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");
        jMenu2.addMenuListener(new javax.swing.event.MenuListener() {
            public void menuCanceled(javax.swing.event.MenuEvent evt) {
            }
            public void menuDeselected(javax.swing.event.MenuEvent evt) {
                editMenuClosed(evt);
            }
            public void menuSelected(javax.swing.event.MenuEvent evt) {
                editMenuOpened(evt);
            }
        });

        mnuUndo.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Z, java.awt.event.InputEvent.CTRL_MASK));
        mnuUndo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/undo.png"))); // NOI18N
        mnuUndo.setText("Undo");
        mnuUndo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                undo(evt);
            }
        });
        jMenu2.add(mnuUndo);

        mnuRedo.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Y, java.awt.event.InputEvent.CTRL_MASK));
        mnuRedo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/redo.png"))); // NOI18N
        mnuRedo.setText("Redo");
        mnuRedo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                redo(evt);
            }
        });
        jMenu2.add(mnuRedo);
        jMenu2.add(jSeparator4);

        jMenuItem4.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_X, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cut.png"))); // NOI18N
        jMenuItem4.setText("Cut");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cut(evt);
            }
        });
        jMenu2.add(jMenuItem4);

        jMenuItem11.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/copy.png"))); // NOI18N
        jMenuItem11.setText("Copy");
        jMenuItem11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                copy(evt);
            }
        });
        jMenu2.add(jMenuItem11);

        jMenuItem10.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_V, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/paste.png"))); // NOI18N
        jMenuItem10.setText("Paste");
        jMenuItem10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                paste(evt);
            }
        });
        jMenu2.add(jMenuItem10);
        jMenu2.add(jSeparator5);

        jMenuItem12.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/search.png"))); // NOI18N
        jMenuItem12.setText("Find...");
        jMenuItem12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                find(evt);
            }
        });
        jMenu2.add(jMenuItem12);
        jMenu2.add(jSeparator6);

        jMenuItem15.setText("Clone");
        jMenuItem15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clonePage(evt);
            }
        });
        jMenu2.add(jMenuItem15);
        jMenu2.add(jSeparator7);

        jMenuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem2.setText("Comment");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comment(evt);
            }
        });
        jMenu2.add(jMenuItem2);
        jMenu2.add(jSeparator12);

        jMenuItem16.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem16.setText("Select All");
        jMenuItem16.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selectAll(evt);
            }
        });
        jMenu2.add(jMenuItem16);

        jMenuBar1.add(jMenu2);

        jMenu3.setText("Tools");
        jMenu3.addMenuListener(new javax.swing.event.MenuListener() {
            public void menuCanceled(javax.swing.event.MenuEvent evt) {
            }
            public void menuDeselected(javax.swing.event.MenuEvent evt) {
                toolsMenuClosed(evt);
            }
            public void menuSelected(javax.swing.event.MenuEvent evt) {
                toolsMenuOpened(evt);
            }
        });

        mnuExecute.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F5, 0));
        mnuExecute.setIcon(new javax.swing.ImageIcon(getClass().getResource("/play.png"))); // NOI18N
        mnuExecute.setText("Execute SQL Query");
        mnuExecute.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuExecuteActionPerformed(evt);
            }
        });
        jMenu3.add(mnuExecute);
        jMenu3.add(jSeparator10);

        mnuXPort.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_X, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        mnuXPort.setIcon(new javax.swing.ImageIcon(getClass().getResource("/xport.png"))); // NOI18N
        mnuXPort.setText("XPort Results...");
        mnuXPort.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuXPortActionPerformed(evt);
            }
        });
        jMenu3.add(mnuXPort);
        jMenu3.add(jSeparator9);

        jMenuItem14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/options.png"))); // NOI18N
        jMenuItem14.setText("Options...");
        jMenuItem14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                options(evt);
            }
        });
        jMenu3.add(jMenuItem14);

        jMenuBar1.add(jMenu3);

        jMenu5.setText("Cloud");

        mnuLogin.setText("Login...");
        mnuLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                login(evt);
            }
        });
        jMenu5.add(mnuLogin);
        jMenu5.add(jSeparator11);

        mnuRegister.setText("Register...");
        mnuRegister.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                register(evt);
            }
        });
        jMenu5.add(mnuRegister);

        jMenuBar1.add(jMenu5);

        jMenu4.setText("Help");

        mnuHelp.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F1, 0));
        mnuHelp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/help.png"))); // NOI18N
        mnuHelp.setText("Help");
        mnuHelp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                help(evt);
            }
        });
        jMenu4.add(mnuHelp);
        jMenu4.add(jSeparator8);

        mnuStartupPage.setText("Startup Page");
        mnuStartupPage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startupPage(evt);
            }
        });
        jMenu4.add(mnuStartupPage);

        jMenuItem18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/about.png"))); // NOI18N
        jMenuItem18.setText("About");
        jMenuItem18.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                about(evt);
            }
        });
        jMenu4.add(jMenuItem18);

        jMenuBar1.add(jMenu4);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(mainSplitPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 870, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(mainSplitPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 426, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     *
     * @param evt
     */
    private void newSQLEditor(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newSQLEditor
        newSQLEditor();
    }//GEN-LAST:event_newSQLEditor

    /**
     *
     * @param evt
     */
    private void openSQLEditor(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openSQLEditor
        openSQLEditor();
    }//GEN-LAST:event_openSQLEditor

    /**
     *
     * @param evt
     */
    private void saveSQL(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveSQL
        saveSQL();
    }//GEN-LAST:event_saveSQL

    /**
     *
     * @param evt
     */
    private void saveAsSQL(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveAsSQL
        saveAsSQL();
    }//GEN-LAST:event_saveAsSQL

    /**
     *
     * @param evt
     */
    private void saveAllSQL(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveAllSQL
        saveAllSQL();
    }//GEN-LAST:event_saveAllSQL

    /**
     *
     * @param evt
     */
    private void undo(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_undo
        if (isEditorSelected() &&
                getSelectedEditor().canUndo()) {
            getSelectedEditor().undo();
        }
    }//GEN-LAST:event_undo

    /**
     *
     * @param evt
     */
    private void redo(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_redo
        if (isEditorSelected() &&
                getSelectedEditor().canRedo()) {
            getSelectedEditor().redo();
        }
    }//GEN-LAST:event_redo

    /**
     *
     * @param evt
     */
    private void cut(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cut
        if (isEditorSelected()) {
            getSelectedEditor().cut();
        }
    }//GEN-LAST:event_cut

    /**
     *
     * @param evt
     */
    private void copy(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_copy
        if (isEditorSelected()) {
            getSelectedEditor().copy();
        }
    }//GEN-LAST:event_copy

    /**
     *
     * @param evt
     */
    private void paste(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_paste
        if (isEditorSelected()) {
            getSelectedEditor().paste();
        }
    }//GEN-LAST:event_paste

    /**
     *
     * @param evt
     */
    private void find(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_find
        if (this.treeDBXplora.hasFocus()) {
            
        } else if (isEditorSelected()) {
            getSelectedEditor().find();
        }
    }//GEN-LAST:event_find

    /**
     *
     * @param evt
     */
    private void clonePage(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clonePage
        if (isEditorSelected()) {
            try {
                SQLEditor editor = getSelectedEditor().clone();
                Component component = this.tabPages.getTabComponentAt(this.tabPages.getSelectedIndex());
                this.tabPages.setSelectedComponent(this.tabPages.add(editor));
                this.tabPages.setTabComponentAt(this.tabPages.getSelectedIndex(), component);
            } catch (CloneNotSupportedException cnse) {
                System.out.println(cnse.getMessage());
            }
        }
    }//GEN-LAST:event_clonePage

    /**
     *
     * @param evt
     */
    private void selectAll(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_selectAll
        if (isEditorSelected()) {
            getSelectedEditor().selectAllText();
        }
    }//GEN-LAST:event_selectAll

    /**
     *
     * @param evt
     */
    private void options(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_options
        final Options options = new Options(this);
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override public void run() {
                options.setVisible(true);
            }
        });
    }//GEN-LAST:event_options

    /**
     *
     * @param evt
     */
    private void help(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_help
        final HelpSystem help = new HelpSystem(this);
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override public void run() {
                help.setVisible(true);
            }
        });
    }//GEN-LAST:event_help

    /**
     *
     * @param evt
     */
    private void startupPage(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startupPage
        // Loop through tab pages to see if a welcome page is available
        for (Component component : this.tabPages.getComponents()) {
            if (component instanceof Welcome) {
                this.tabPages.setSelectedComponent(component);
                return;
            }
        }
        // Show a new Welcome page
        log.debug("Show the welcome page");
        this.tabPages.setSelectedComponent(this.tabPages.add(new Welcome(this)));
        this.tabPages.setTabComponentAt(this.tabPages.getSelectedIndex(), new TabPageHeader(this, this.tabPages, (JComponent) this.tabPages.getSelectedComponent(), "Welcome"));
    }//GEN-LAST:event_startupPage

    /**
     *
     * @param evt
     */
    private void about(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_about
        final About about = new About(this);
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override public void run() {
                about.setVisible(true);
            }
        });
    }//GEN-LAST:event_about

    /**
     *
     * @param evt
     */
    private void editMenuOpened(javax.swing.event.MenuEvent evt) {//GEN-FIRST:event_editMenuOpened
        this.mnuUndo.setEnabled(isEditorSelected() &&
                                    getSelectedEditor().canUndo());
        this.mnuRedo.setEnabled(isEditorSelected() &&
                                    getSelectedEditor().canRedo());
    }//GEN-LAST:event_editMenuOpened

    /**
     *
     * @param evt
     */
    private void treeDBXploraMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_treeDBXploraMouseClicked
        if (SwingUtilities.isRightMouseButton(evt)) {
            this.treeDBXplora.setSelectionRow(this.treeDBXplora.getClosestRowForLocation(evt.getX(), evt.getY()));
            DBXPloraPopUpMenu menu = DBXPloraPopupFactory.getInstance().getMenu(this, this.treeDBXplora);
            if (menu != null) {
                menu.show(evt.getX(), evt.getY());
            }
        }
    }//GEN-LAST:event_treeDBXploraMouseClicked

    /**
     *
     * @param evt
     */
    private void applicationClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_applicationClosing
        exit(null);
    }//GEN-LAST:event_applicationClosing

    /**
     *
     * @param evt
     */
    private void mnuXPortActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuXPortActionPerformed
        if (isEditorSelected()) {
            getSelectedEditor().export();
        }
    }//GEN-LAST:event_mnuXPortActionPerformed

    /**
     *
     * @param evt
     */
    private void mnuExecuteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuExecuteActionPerformed
        if (isEditorSelected()) {
            getSelectedEditor().execute();
        }
    }//GEN-LAST:event_mnuExecuteActionPerformed

    /**
     *
     * @param evt
     */
    private void toolsMenuOpened(javax.swing.event.MenuEvent evt) {//GEN-FIRST:event_toolsMenuOpened
        this.mnuExecute.setEnabled(isEditorSelected() &&
                                        getSelectedEditor().canExecute());
        this.mnuXPort.setEnabled(isEditorSelected() &&
                                        getSelectedEditor().canExport());
    }//GEN-LAST:event_toolsMenuOpened

    /**
     *
     * @param evt
     */
    private void editMenuClosed(javax.swing.event.MenuEvent evt) {//GEN-FIRST:event_editMenuClosed
        this.mnuUndo.setEnabled(true);
        this.mnuRedo.setEnabled(true);
    }//GEN-LAST:event_editMenuClosed

    /**
     *
     * @param evt
     */
    private void toolsMenuClosed(javax.swing.event.MenuEvent evt) {//GEN-FIRST:event_toolsMenuClosed
        this.mnuExecute.setEnabled(true);
        this.mnuXPort.setEnabled(true);
    }//GEN-LAST:event_toolsMenuClosed

    /**
     *
     * @param evt
     * @throws javax.swing.tree.ExpandVetoException
     */
    private void treeDBXploraTreeWillExpand(javax.swing.event.TreeExpansionEvent evt)throws javax.swing.tree.ExpandVetoException {//GEN-FIRST:event_treeDBXploraTreeWillExpand
        if (evt.getPath().getLastPathComponent() instanceof Definition) {
            Definition definition = (Definition) evt.getPath().getLastPathComponent();
            if (!definition.isConnected()) {
                definition.asyncConnect();
            }
        }
    }//GEN-LAST:event_treeDBXploraTreeWillExpand

    /**
     *
     * @param evt
     */
    private void login(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_login
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                CloudLogin login = new CloudLogin(getContentPane());
                login.setVisible(true);
                
                if (login.isLoggedIn()) {
                    setLoggedIn(login.isLoggedIn());
                    setUser(login.getUser());
                    setPassword(login.getPassword());
                }
            }
        });
    }//GEN-LAST:event_login

    /**
     *
     * @param evt
     */
    private void register(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_register
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                CloudRegister register = new CloudRegister(getParent());
                register.setVisible(true);
            }
        });
    }//GEN-LAST:event_register

    /**
     *
     * @param evt
     */
    private void comment(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comment
        if (isEditorSelected()) {
            getSelectedEditor().commentText();
        }
    }//GEN-LAST:event_comment

    /**
     * 
     * @param evt
     */
    private void exit(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exit
        if (this.databases != null) {
            for (Definition definition : this.databases.getConnectedDefinitions()) {
                definition.disconnect();
            }
        }
        Settings.setApplicationBounds(this);
        Settings.setDBXPloraWidth(this.mainSplitPanel.getDividerLocation());
        List<File> files = new ArrayList();
        for (SQLEditor sqlEditor : getEditors()) {
            if (sqlEditor.isFileSet()) {
                File file = sqlEditor.getFile();
                if (file.exists()) {
                    files.add(file);
                }
            }
        }
        Pages.getInstance().setPages(files);
        try {
            Pages.getInstance().save();
        } catch (IOException ioe) {
            log.error(ioe.getMessage(), ioe);
        }
        try {
            Settings.save();
        } catch (SettingsException se) {
            log.error(se.getMessage(), se);
        }

        if (isLoggedIn()) {
//            CloudOperations cloudOperations = new CloudOperations();
//            cloudOperations.setDBXplora(Databases.class, getUser(), this.databases.saveToString());
        }
        System.exit(0);
    }//GEN-LAST:event_exit
    
    /**
     * 
     * @param image
     * @return 
     */
    private static BufferedImage getSplashScreenImage() {
        if (splashScreen != null &&
                splashScreenGraphics != null) {
            if (splashScreenImage == null) {
                try {
                    splashScreenImage = ImageIO.read(splashScreen.getImageURL());
                } catch (IOException ioe) {
                    log.error(ioe.getMessage(), ioe);
                }
            }
        }
        return splashScreenImage;
    }
    
    /**
     * 
     * @param process 
     */
    private static void renderSplashScreen(String message, int position) {
        if (splashScreen == null) {
            splashScreen = SplashScreen.getSplashScreen();
        }
        if (splashScreenGraphics == null &&
                splashScreen != null) {
            splashScreenGraphics = splashScreen.createGraphics();
        }
        
        if (splashScreen != null &&
                splashScreen.isVisible() &&
                    splashScreenGraphics != null &&
                        getSplashScreenImage() != null) {
            int width = (splashScreenImage.getWidth() / 100) * position;

            splashScreenGraphics.setComposite(Clear);
            splashScreenGraphics.clipRect(0, 0, splashScreenImage.getWidth(), splashScreenImage.getHeight());
            splashScreenGraphics.drawImage(splashScreenImage, 0, 0, splashScreenImage.getWidth(), splashScreenImage.getHeight(), null);
            splashScreenGraphics.setPaintMode();
            splashScreenGraphics.setColor(LIGHT_BLUE);
            splashScreenGraphics.setFont(DEFAULT_FONT);
            splashScreenGraphics.setRenderingHint(KEY_TEXT_ANTIALIASING, VALUE_TEXT_ANTIALIAS_ON);
            splashScreenGraphics.drawString(message, 10, splashScreenImage.getHeight() - 20);
            splashScreenGraphics.fillRect(0, splashScreenImage.getHeight() - 5, width, 5);
            splashScreen.update();
            log.debug("Message: " + message + ", Position: " + position + ", Full Width: " + splashScreenImage.getWidth() + ", Width: " + width);
        }
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(final String... args) {
        renderSplashScreen("Checking parameters...", 1);
        try (Socket socket = new Socket(DEFAULT_IP, DEFAULT_PORT)) {
            try (PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
                    BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()))) {
                String line = reader.readLine();
                if (line != null &&
                        line.equals(OK)) {
                    if (args.length > 0) {
                        renderSplashScreen("Open " + args[0], 5);
                        writer.println(args[0]);
                    } else {
                        writer.println(OK);
                        JOptionPane.showMessageDialog(null, "An instance of JMSql is already running", "JMSql Startup", ERROR_MESSAGE);
                    }
                    return;
                }
            }
        } catch (IOException ioe) {
            log.error(ioe.getMessage(), ioe);
        }
        
        renderSplashScreen("Loading Settings...", 10);
        try {
            Properties log4jProperties = new Properties();
            try (InputStream input = JMSqlApp.class.getClassLoader().getResourceAsStream("/log4j.properties")) {
                if (input != null) {
                    log4jProperties.load(input);
                }
            }
            PropertyConfigurator.configure(log4jProperties);
        } catch (IOException ioe) {
            log.error(ioe.getMessage(), ioe);
        }
        try {
            Settings.load();
        } catch (SettingsException se) {
            log.error(se.getMessage(), se);
        }
        // Initialize the settings file
        Settings.initializeSettings();
        renderSplashScreen("Initializing Look & Feel...", 15);
        /* Set the look and feel */
        try {
            UIManager.setLookAndFeel(Settings.getLookAndFeel());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            log.error(e.getMessage(), e);
        }
        renderSplashScreen("Starting JMSql...", 20);
        /* Create and display the form */
        EventQueue.invokeLater(new Runnable() {
            @Override 
            public void run() {
                JMSqlApp app = new JMSqlApp();
                if (args.length > 0 &&
                        args[0] != null &&
                            !args[0].isEmpty()) {
                    File file = new File(args[0]);
                    if (file.exists()) {
                        app.newSQLEditor(file);
                    }
                }
                renderSplashScreen("Finalizing JMSql...", 100);
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ie) {
                    log.error(ie.getMessage(), ie);
                }
                if (splashScreen != null &&
                        splashScreen.isVisible()) {
                    splashScreen.close();
                }
                app.setVisible(true);
            }
        });
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnNew;
    private javax.swing.JButton btnOpen;
    private javax.swing.JButton btnSaveAll;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem10;
    private javax.swing.JMenuItem jMenuItem11;
    private javax.swing.JMenuItem jMenuItem12;
    private javax.swing.JMenuItem jMenuItem14;
    private javax.swing.JMenuItem jMenuItem15;
    private javax.swing.JMenuItem jMenuItem16;
    private javax.swing.JMenuItem jMenuItem18;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JMenuItem jMenuItem8;
    private javax.swing.JMenuItem jMenuItem9;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator10;
    private javax.swing.JPopupMenu.Separator jSeparator11;
    private javax.swing.JPopupMenu.Separator jSeparator12;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JPopupMenu.Separator jSeparator5;
    private javax.swing.JPopupMenu.Separator jSeparator6;
    private javax.swing.JPopupMenu.Separator jSeparator7;
    private javax.swing.JPopupMenu.Separator jSeparator8;
    private javax.swing.JPopupMenu.Separator jSeparator9;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JSplitPane mainSplitPanel;
    private javax.swing.JMenuItem mnuExecute;
    private javax.swing.JMenuItem mnuHelp;
    private javax.swing.JMenuItem mnuLogin;
    private javax.swing.JMenuItem mnuRedo;
    private javax.swing.JMenuItem mnuRegister;
    private javax.swing.JMenuItem mnuStartupPage;
    private javax.swing.JMenuItem mnuUndo;
    private javax.swing.JMenuItem mnuXPort;
    private javax.swing.JScrollPane scpDBXploraModel;
    public javax.swing.JTabbedPane tabPages;
    private javax.swing.JTree treeDBXplora;
    // End of variables declaration//GEN-END:variables

}
