/*
 * Copyright (C) 2013 Michael
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.jm.jmsql.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @created Feb 25, 2013
 * @author Michael L.R. Marques
 */
public class Cache {
    
    /**
     * The location of the cache files
     */
    private static final File cacheLocation = new File("./cache");
    
    /**
     * The extensions of the cache file
     */
    private static final String cacheExtension = ".cache";
    
    /**
     * The caching limit
     */
    private static final int cacheLimit = 10;
    
    /**
     * 
     * @param name
     * @param text 
     */
    public static void write(String name, String text) {
        // Check if the cache folder exists, if not, create it
        if (!cacheLocation.exists()) {
            cacheLocation.mkdirs();
        }
        // Create the cache file
        File cacheFile = new File(cacheLocation, name + cacheExtension);
        // Write to cache
        try {
            // Get the currently cached objects
            List<String> cache = read(name);
            // Create a new file
            cacheFile.createNewFile();
            // Create the reader stream, read the contents
            try (FileWriter writer = new FileWriter(cacheFile)) {
                // Write the text
                writer.write(text + Constants.NEW_LINE);
                // Track the number offf lines
                int lines = 1;
                // Loop through the current cache strings
                for (String line : cache) {
                    // Append the cached strings
                    if (!line.equals(text) &&
                            lines <= cacheLimit) {
                        writer.append(line + Constants.NEW_LINE);
                    }
                    // If limit has been reached, break
                    if (lines > cacheLimit) {
                        break;
                    }
                }
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
    
    /**
     * 
     * @param name
     * @return 
     */
    public static List<String> read(String name) {
        // Create cache array
        List<String> cache = new ArrayList();
        // Check if the cache folder exists
        if (cacheLocation.exists()) {
            // Create the cache file
            File cacheFile = new File(cacheLocation, name + cacheExtension);
            // Check if the cache file exists
            if (cacheFile.exists()) {
                try {
                    // Create the reader stream, read the contents
                    try (BufferedReader reader = new BufferedReader(new FileReader(cacheFile))) {
                        String line;
                        while((line = reader.readLine()) != null) {
                            cache.add(line);
                        }
                    }
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            }
        }
        return cache;
    }

}
