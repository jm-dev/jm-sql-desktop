/*
 * Copyright (C) 2012-2013 Michael L.R. Marques
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Contact: michaellrmarques@gmail.com
 */

package com.jm.jmsql.utils;

import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;

/**
 *
 * @created Nov 22, 2012
 * @author Michael L.R. Marques
 */
public final class Constants extends com.jm.commons.utils.Constants {
    
    // Default application bounds constant
    public static final Rectangle DEFAULT_BOUNDS = new Rectangle(0, 0, 800, 600);
    
    // Application name constant
    public static final String APPLICATION_NAME = "JMSql";
    
    // Application version constant
    public static final String VERSION = "1.0.3";

    public static final Font DEFAULT_FONT = new Font("Font", Font.BOLD, 13);

    public static final String SPLASH_SCREEN_IMAGE = "./images/splashscreen.png";
    
    /**
     * 
     */
    public final static class SQL {
        
        //
        public static final String DELIMETER = ";";
        
    }
    
    /**
     * 
     */
    public final static class Colours {
        
        //
        public static final Color LIGHT_BLUE = new Color(100, 149, 237);
        
    }

}
