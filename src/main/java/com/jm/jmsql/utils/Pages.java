/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.jmsql.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 *
 * @author Michael L.R. Marques
 */
public class Pages {

    private static final String KEY_PAGES = "pages";

    private static final File PAGES_FILE = new File("./settings/variables.properties");
    
    private static Pages pages;

    private List<File> pageList;

    /**
     *
     */
    private Pages() {
        if (PAGES_FILE.exists()) {
            try (FileInputStream input = new FileInputStream(PAGES_FILE)) {
                Properties props = new Properties();
                props.load(input);

                int pageCount = props.get(KEY_PAGES) == null ? 0 : Integer.parseInt((String) props.get(KEY_PAGES));
                this.pageList = new ArrayList(pageCount);

                for (int i = 0; i < pageCount; i++) {
                    String page = props.getProperty(PAGES_FILE + "." + i);
                    if (page != null) {
                        this.pageList.add(new File(page));
                    }
                }
            } catch (IOException ioe) {
                System.out.println(ioe.getMessage());
            }
        } else {
            PAGES_FILE.getParentFile().mkdirs();
            try {
                PAGES_FILE.createNewFile();
            } catch (IOException ioe) {
                System.out.println(ioe.getMessage());
            }
        }

        if (this.pageList == null) {
            this.pageList = new ArrayList(0);
        }
    }

    /**
     * 
     * @return
     */
    public static Pages getInstance() {
        if (pages == null) {
            pages = new Pages();
        }
        return pages;
    }
    
    /**
     * 
     * @param pages 
     */
    public void setPages(List<File> pages) {
        this.pageList = pages;
    }
    
    /**
     * 
     * @return 
     */
    public List<File> getPages() {
        return this.pageList;
    }
    
    /**
     *
     * @throws java.io.IOException
     */
    public void save() throws IOException {
        try (FileOutputStream output = new FileOutputStream(PAGES_FILE)) {
            Properties props = new Properties();
            props.put(KEY_PAGES, String.valueOf(this.pageList.size()));

            for (int i = 0; i < this.pageList.size(); i++) {
                if (this.pageList.get(i) != null) {
                    props.setProperty(PAGES_FILE + "." + i, this.pageList.get(i).getAbsolutePath());
                }
            }
            props.store(output, "The last pages are saved");
        }
    }

}
