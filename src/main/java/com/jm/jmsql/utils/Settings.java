/*
 * Copyright (C) 2012-2013 Michael L.R. Marques
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Contact: michaellrmarques@gmail.com
 */

package com.jm.jmsql.utils;

import com.jm.commons.settings.SettingsException;
import static com.jm.jmsql.utils.Constants.DEFAULT_BOUNDS;
import java.awt.Frame;
import java.awt.Rectangle;
import java.io.File;
import java.text.SimpleDateFormat;
import javax.swing.UIManager.LookAndFeelInfo;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @created Nov 21, 2012
 * @author Michael L.R. Marques
 */
public class Settings extends com.jm.commons.settings.Settings {
    
    private static final Logger logger = LogManager.getLogger(Settings.class);
    
    public static final String APPLICATION_X = "application-x";
    public static final String APPLICATION_Y = "application-y";
    public static final String APPLICATION_WIDTH = "application-width";
    public static final String APPLICATION_HEIGHT = "application-height";
    
    public static final String PLUGINS_LOCATION = "xport-plugins-location";
    
    public static final String REMEMBER_LOGIN = "remember-login";
    public static final String LOGIN_USER = "login-user";
    public static final String LOGIN_PASSWORD = "login-password";

    public static final String LOOK_AND_FEEL = "look-and-feel";

    public static final String LAST_PAGES = "last-pages";

    public static final String DATE_FORMAT = "date-format";
    
    /**
     * 
     * @return 
     */
    public static Class getClazz() {
        return (new Settings()).getClass();
    }
    
    /**
     * 
     */
    public static void initializeSettings() {
        // Set the bounds
        if (Settings.getApplicationBounds() == null) {
            Settings.setApplicationBounds(DEFAULT_BOUNDS);
            Settings.setApplicationBoundsDefault(DEFAULT_BOUNDS);
            try {
                Settings.save();
            } catch (SettingsException se) {
                logger.error(se.getMessage(), se);
            }
        }
        // Set the database librarys directory
        if (Settings.getDatabaseLibrarysDirectory() == null ||
                !Settings.getDatabaseLibrarysDirectory().exists()) {
            Settings.setDatabaseLibrarysDirectory("./dblibs");
            Settings.setDatabaseLibrarysDirectoryDefault("./dblibs");
            try {
                Settings.save();
            } catch (SettingsException se) {
                logger.error(se.getMessage(), se);
            }
        }
        // Set the database xml file
        if (Settings.getDatabasesFile() == null ||
                !Settings.getDatabasesFile().exists()) {
            Settings.setDatabasesFile("./settings/databases.xml");
            Settings.setDatabasesFileDefault("./settings/databases.xml");
            try {
                Settings.save();
            } catch (SettingsException se) {
                logger.error(se.getMessage(), se);
            }
        }
        // Set the plugins location
        if (Settings.getXPortPluginsLocation() == null ||
                !Settings.getXPortPluginsLocation().exists()) {
            Settings.setXPortPluginsLocation("./plugins");
            Settings.setXPortPluginsLocationDefault("./plugins");
            try {
                Settings.save();
            } catch (SettingsException se) {
                logger.error(se.getMessage(), se);
            }
        }
        // Set DBXplora width
        if (Settings.getDBXploraWidth() == 0) {
            Settings.setDBXPloraWidth(200);
            Settings.setDBXPloraWidthDefault(200);
            try {
                Settings.save();
            } catch (SettingsException se) {
                logger.error(se.getMessage(), se);
            }
        }
        // Set Look and Feel
        if (Settings.getLookAndFeel() == null ||
                Settings.getLookAndFeel().isEmpty()) {
            Settings.setLookAndFeel(javax.swing.UIManager.getSystemLookAndFeelClassName());
            Settings.setLookAndFeelDefault(javax.swing.UIManager.getSystemLookAndFeelClassName());
            try {
                Settings.save();
            } catch (SettingsException se) {
                logger.error(se.getMessage(), se);
            }
        }

        if (isRememberLogin()) {
            
        }
    }
    
    /**
     * 
     * @param parent 
     */
    public static void setApplicationBounds(Frame parent) {
        setApplicationBounds(parent.getBounds());
    }
    
    /**
     * 
     * @param bounds 
     */
    public static void setApplicationBounds(Rectangle bounds) {
        try {
            setValue(APPLICATION_X, String.valueOf(bounds.x));
            setValue(APPLICATION_Y, String.valueOf(bounds.y));
            setValue(APPLICATION_WIDTH, String.valueOf(bounds.width));
            setValue(APPLICATION_HEIGHT, String.valueOf(bounds.height));
        } catch (SettingsException se) {
            logger.error(se.getMessage(), se);
        }
    }
    
    /**
     * 
     * @param x
     * @param y
     * @param width
     * @param height 
     */
    public static void setApplicationBounds(int x, int y, int width, int height) {
        try {
            setValue(APPLICATION_X, String.valueOf(x));
            setValue(APPLICATION_Y, String.valueOf(y));
            setValue(APPLICATION_WIDTH, String.valueOf(width));
            setValue(APPLICATION_HEIGHT, String.valueOf(height));
        } catch (SettingsException se) {
            logger.error(se.getMessage(), se);
        }
    }
    
    /**
     * 
     * @param x
     * @param y 
     */
    public static void setApplicationLocation(int x, int y) {
        try {
            setValue(APPLICATION_X, String.valueOf(x));
            setValue(APPLICATION_Y, String.valueOf(y));
        } catch (SettingsException se) {
            logger.error(se.getMessage(), se);
        }
    }
    
    /**
     * 
     * @param width
     * @param height 
     */
    public static void setApplicationSize(int width, int height) {
        try {
            setValue(APPLICATION_WIDTH, String.valueOf(width));
            setValue(APPLICATION_HEIGHT, String.valueOf(height));
        } catch (SettingsException se) {
            logger.error(se.getMessage(), se);
        }
    }
    
    /**
     * 
     * @return 
     */
    public static Rectangle getApplicationBounds() {
        try {
            return new Rectangle(Integer.parseInt(getValue(APPLICATION_X)),
                                        Integer.parseInt(getValue(APPLICATION_Y)),
                                            Integer.parseInt(getValue(APPLICATION_WIDTH)),
                                                Integer.parseInt(getValue(APPLICATION_HEIGHT)));
        } catch (SettingsException | NumberFormatException e) {
            logger.error(e.getMessage(), e);
            return getApplicationBoundsDefault();
        }
    }
    
    /**
     * 
     * @param parent 
     */
    public static void setApplicationBoundsDefault(Frame parent) {
        setApplicationBoundsDefault(parent.getBounds());
    }
    
    /**
     * 
     * @param bounds 
     */
    public static void setApplicationBoundsDefault(Rectangle bounds) {
        try {
            setDefault(APPLICATION_X, String.valueOf(bounds.x));
            setDefault(APPLICATION_Y, String.valueOf(bounds.y));
            setDefault(APPLICATION_WIDTH, String.valueOf(bounds.width));
            setDefault(APPLICATION_HEIGHT, String.valueOf(bounds.height));
        } catch (SettingsException se) {
            logger.error(se.getMessage(), se);
        }
    }
    
    /**
     * 
     * @return 
     */
    public static Rectangle getApplicationBoundsDefault() {
        try {
            return new Rectangle(Integer.parseInt(getDefault(APPLICATION_X)),
                                        Integer.parseInt(getDefault(APPLICATION_Y)),
                                            Integer.parseInt(getDefault(APPLICATION_WIDTH)),
                                                Integer.parseInt(getDefault(APPLICATION_HEIGHT)));
        } catch (SettingsException | NumberFormatException e) {
            logger.error(e.getMessage(), e);
            return Constants.DEFAULT_BOUNDS;
        }
    }
    
    /**
     * 
     * @return 
     */
    public static String getApplicationName() {
        return Constants.APPLICATION_NAME;
    }
    
    /**
     * 
     * @return 
     */
    public static String getApplicationVersion() {
        return Constants.VERSION;
    }
    
    /**
     * 
     * @param databaseLibrarysDirectory 
     */
    public static void setDatabaseLibrarysDirectoryDefault(File databaseLibrarysDirectory) {
        setDatabaseLibrarysDirectoryDefault(databaseLibrarysDirectory.getPath());
    }
    
    /**
     * 
     * @param databaseLibrarysDirectory
     */
    public static void setDatabaseLibrarysDirectoryDefault(String databaseLibrarysDirectory) {
        try {
            setDefault("database-librarys-directory", databaseLibrarysDirectory);
        } catch (SettingsException se) {
            logger.error(se.getMessage(), se);
        }
    }
    
    /**
     * 
     * @return 
     */
    public static File getDatabaseLibrarysDirectoryDefault() {
        try {
            return new File(getDefault("database-librarys-directory"));
        } catch (SettingsException se) {
            logger.error(se.getMessage(), se);
            return null;
        }
    }
    
    /**
     * 
     * @param databaseLibrarysDirectory 
     */
    public static void setDatabaseLibrarysDirectory(File databaseLibrarysDirectory) {
        setDatabaseLibrarysDirectory(databaseLibrarysDirectory.getPath());
    }
    
    /**
     * 
     * @param databaseLibrarysDirectory 
     */
    public static void setDatabaseLibrarysDirectory(String databaseLibrarysDirectory) {
        try {
            setValue("database-librarys-directory", databaseLibrarysDirectory);
        } catch (SettingsException se) {
            logger.error(se.getMessage(), se);
        }
    }
    
    /**
     * 
     * @return 
     */
    public static File getDatabaseLibrarysDirectory() {
        try {
            return new File(getValue("database-librarys-directory"));
        } catch (SettingsException se) {
            logger.error(se.getMessage(), se);
        }
        return null;
    }
    
    /**
     * 
     * @param databases 
     */
    public static void setDatabasesFileDefault(String databases) {
        try {
            setDefault("databases", databases);
        } catch (SettingsException se) {
            logger.error(se.getMessage(), se);
        }
    }
    
    /**
     * 
     * @param databases 
     */
    public static void setDatabasesFileDefault(File databases) {
        setDatabasesFileDefault(databases.getPath());
    }
    
    /**
     * 
     * @return 
     */
    public static File getDatabasesFileDefault() {
        try {
            return new File(getDefault("databases"));
        } catch (SettingsException se) {
            logger.error(se.getMessage(), se);
        }
        return null;
    }
    
    /**
     * 
     * @param databases 
     */
    public static void setDatabasesFile(String databases) {
        try {
            setValue("databases", databases);
        } catch (SettingsException se) {
            logger.error(se.getMessage(), se);
        }
    }
    
    /**
     * 
     * @param databases 
     */
    public static void setDatabasesFile(File databases) {
        setDatabasesFile(databases.getPath());
    }
    
    /**
     * 
     * @return 
     */
    public static File getDatabasesFile() {
        try {
            return new File(getValue("databases"));
        } catch (SettingsException se) {
            logger.error(se.getMessage(), se);
        }
        return null;
    }
    
    /**
     * 
     * @param logging 
     */
    public static void setLogging(boolean logging) {
        try {
            setValue("logging", Boolean.toString(logging));
        } catch (SettingsException se) {
            logger.error(se.getMessage(), se);
        }
    }
    
    /**
     * 
     * @return 
     */
    public static boolean isLogging() {
        try {
            return Boolean.parseBoolean(getValue("logging"));
        } catch (SettingsException se) {
            logger.error(se.getMessage(), se);
        }
        return false;
    }
    
    /**
     * 
     * @param showStartupPage 
     */
    public static void showStartupPage(boolean showStartupPage) {
        try {
            setValue("show-startup", Boolean.toString(showStartupPage));
        } catch (SettingsException se) {
            logger.error(se.getMessage(), se);
        }
    }
    
    /**
     * 
     * @return 
     */
    public static boolean showStartupPage() {
        try {
            return Boolean.parseBoolean(getValue("show-startup"));
        } catch (SettingsException se) {
            logger.error(se.getMessage(), se);
        }
        return true;
    }
    
    /**
     * 
     * @param width 
     */
    public static void setDBXPloraWidthDefault(int width) {
        try {
            setDefault("dbxplora-width", String.valueOf(width));
        } catch (SettingsException se) {
            logger.error(se.getMessage(), se);
        }
    }
    
    /**
     * 
     * @return 
     */
    public static int getDBXploraWidthDefault() {
        try {
            return Integer.parseInt(getDefault("dbxplora-width"));
        } catch (SettingsException | NumberFormatException e) {
            logger.error(e.getMessage(), e);
            return 200;
        }
    }
    
    /**
     * 
     * @param width 
     */
    public static void setDBXPloraWidth(int width) {
        try {
            setValue("dbxplora-width", String.valueOf(width));
        } catch (SettingsException se) {
            logger.error(se.getMessage(), se);
        }
    }
    
    /**
     * 
     * @return 
     */
    public static int getDBXploraWidth() {
        try {
            return Integer.parseInt(getValue("dbxplora-width"));
        } catch (SettingsException | NumberFormatException e) {
            logger.error(e.getMessage(), e);
            return getDBXploraWidthDefault();
        }
    }
    
    /**
     * 
     * @param laf 
     */
    public static void setLookAndFeelDefault(String laf) {
        try {
            setDefault(LOOK_AND_FEEL, laf);
        } catch (SettingsException se) {
            logger.error(se.getMessage(), se);
        }
    }
    
    /**
     * 
     * @return 
     */
    public static String getLookAndFeelDefault() {
        try {
            return getDefault(LOOK_AND_FEEL);
        } catch (SettingsException se) {
            logger.error(se.getMessage(), se);
            return javax.swing.UIManager.getSystemLookAndFeelClassName();
        }

    }
    
    /**
     * 
     * @param laf 
     */
    public static void setLookAndFeel(LookAndFeelInfo laf) {
        setLookAndFeel(laf.getClassName());
    }
    
    /**
     * 
     * @param laf 
     */
    public static void setLookAndFeel(String laf) {
        try {
            setValue(LOOK_AND_FEEL, laf);
        } catch (SettingsException se) {
            logger.error(se.getMessage(), se);
        }
    }
    
    /**
     * 
     * @return 
     */
    public static String getLookAndFeel() {
        try {
            return getValue(LOOK_AND_FEEL);
        } catch (SettingsException se) {
            logger.error(se.getMessage(), se);
            return getLookAndFeelDefault();
        }
    }
    
    /**
     * 
     * @param location 
     */
    public static void setSQLLocationDefault(File location) {
        try {
            setDefault("sql-location", location.getAbsolutePath());
        } catch (SettingsException se) {
            logger.error(se.getMessage(), se);
        }
    }
    
    /**
     * 
     * @return 
     */
    public static File getSQLLocationDefault() {
        try {
            return new File(getValue("sql-location"));
        } catch (SettingsException se) {
            logger.error(se.getMessage(), se);
            return new File(System.getProperty("./"));
        }
    }
    
    /**
     * 
     * @param location 
     */
    public static void setSQLLocation(File location) {
        try {
            setValue("sql-location", location.getAbsolutePath());
        } catch (SettingsException se) {
            logger.error(se.getMessage(), se);
        }
    }
    
    /**
     * 
     * @return 
     */
    public static File getSQLLocation() {
        try {
            return new File(getValue("sql-location"));
        } catch (SettingsException se) {
            logger.error(se.getMessage(), se);
            return getSQLLocationDefault();
        }
    }
    
    /**
     * 
     * @param location 
     */
    public static void setXPortPluginsLocationDefault(String location) {
        setXPortPluginsLocationDefault(new File(location));
    }
    
    /**
     * 
     * @param location 
     */
    public static void setXPortPluginsLocationDefault(File location) {
        try {
            setDefault(PLUGINS_LOCATION, location.getAbsolutePath());
        } catch (SettingsException se) {
            logger.error(se.getMessage(), se);
        }
    }
    
    /**
     * 
     * @return 
     */
    public static File getXPortPluginsLocationDefault() {
        try {
            File file = new File(getValue(PLUGINS_LOCATION));
            if (!file.exists()) {
                throw new SettingsException("Default plugins location does not exist");
            }
            return file;
        } catch (SettingsException se) {
            logger.error(se.getMessage(), se);
            File file = new File("./plugins");
            file.mkdirs();
            return file;
        }
    }
    
    /**
     * 
     * @param location 
     */
    public static void setXPortPluginsLocation(String location) {
        setXPortPluginsLocation(new File(location));
    }
    
    /**
     * 
     * @param location 
     */
    public static void setXPortPluginsLocation(File location) {
        try {
            setValue(PLUGINS_LOCATION, location.getAbsolutePath());
        } catch (SettingsException se) {
            logger.error(se.getMessage(), se);
        }
    }
    
    /**
     * 
     * @return 
     */
    public static File getXPortPluginsLocation() {
        try {
            File file = new File(getValue(PLUGINS_LOCATION));
            if (!file.exists()) {
                throw new SettingsException("Plugins location does not exist");
            }
            return file;
        } catch (SettingsException se) {
            logger.error(se.getMessage(), se);
            return getXPortPluginsLocationDefault();
        }
    }
    
    /**
     * 
     * @param predicting 
     */
    public static void setPredictingDefault(boolean predicting) {
        try {
            setDefault("predict", String.valueOf(predicting));
        } catch (SettingsException se) {
            logger.error(se.getMessage(), se);
        }
    }
    
    /**
     * 
     * @return 
     */
    public static boolean isPredictingDefault() {
        try {
            return Boolean.parseBoolean(getDefault("predict"));
        } catch (SettingsException se) {
            logger.error(se.getMessage(), se);
            return false;
        }
    }
       
    /**
     * 
     * @param predicting 
     */
    public static void setPredicting(boolean predicting) {
        try {
            setValue("predict", String.valueOf(predicting));
        } catch (SettingsException se) {
            logger.error(se.getMessage(), se);
        }
    }
    
    /**
     * 
     * @return 
     */
    public static boolean isPredicting() {
         try {
            return Boolean.parseBoolean(getValue("predict"));
        } catch (SettingsException se) {
            logger.error(se.getMessage(), se);
            return isPredictingDefault();
        }
    }
    
    /**
     * 
     * @param remember 
     */
    public static void setRememberLogin(boolean remember) {
        try {
            setValue(REMEMBER_LOGIN, String.valueOf(remember));
        } catch (SettingsException se) {
            logger.error(se.getMessage(), se);
        }
    }
    
    /**
     * 
     * @return 
     */
    public static boolean isRememberLogin() {
        try {
            return Boolean.parseBoolean(getValue(REMEMBER_LOGIN));
        } catch (SettingsException se) {
            logger.error(se.getMessage(), se);
            return false;
        }
    }
    
    /**
     * 
     * @param email 
     */
    public static void setLoginUserDefault(String email) {
        try {
            setDefault(LOGIN_USER, email);
        } catch (SettingsException se) {
            logger.error(se.getMessage(), se);
        }
    }
    
    /**
     * 
     * @return 
     */
    public static String getLoginUserDefault() {
        try {
            return getDefault(LOGIN_USER);
        } catch (SettingsException se) {
            logger.error(se.getMessage(), se);
            return "";
        }
    }
    
    /**
     * 
     * @param email 
     */
    public static void setLoginUser(String email) {
        try {
            setValue(LOGIN_USER, email);
        } catch (SettingsException se) {
            logger.error(se.getMessage(), se);
        }
    }
    
    /**
     * 
     * @return 
     */
    public static String getLoginUser() {
        try {
            return getValue(LOGIN_USER);
        } catch (SettingsException se) {
            logger.error(se.getMessage(), se);
            return getLoginUserDefault();
        }
    }
    
    /**
     * 
     * @param password 
     */
    public static void setLoginPasswordDefault(String password) {
        try {
            setDefault(LOGIN_PASSWORD, password);
        } catch (SettingsException se) {
            logger.error(se.getMessage(), se);
        }
    }
    
    /**
     * 
     * @return 
     */
    public static String getLoginPasswordDefault() {
        try {
            return getDefault(LOGIN_PASSWORD);
        } catch (SettingsException se) {
            logger.error(se.getMessage(), se);
            return "";
        }
    }
    
    /**
     * 
     * @param password 
     */
    public static void setLoginPassword(String password) {
        try {
            setValue(LOGIN_PASSWORD, password);
        } catch (SettingsException se) {
            logger.error(se.getMessage(), se);
        }
    }
    
    /**
     * 
     * @return 
     */
    public static String getLoginPassword() {
        try {
            return getValue(LOGIN_PASSWORD);
        } catch (SettingsException se) {
            logger.error(se.getMessage(), se);
            return getLoginUserDefault();
        }
    }

    /**
     *
     * @param format
     */
    public static void setDateFormat(SimpleDateFormat format) {
        setDateFormat(format.toPattern());
    }

    /**
     *
     * @param format
     */
    public static void setDateFormat(String format) {
        try {
            setValue(DATE_FORMAT, format);
        } catch (SettingsException se) {
            logger.error(se.getMessage(), se);
        }
    }

    /**
     *
     * @return
     */
    public static SimpleDateFormat getDateFormat() {
        try {
            return new SimpleDateFormat(getValue(DATE_FORMAT));
        } catch (SettingsException se) {
            logger.error(se.getMessage(), se);
        }
        return getDefaultDateFormat();
    }

    /**
     *
     * @return
     */
    public static SimpleDateFormat getDefaultDateFormat() {
        return new SimpleDateFormat();
    }

}
