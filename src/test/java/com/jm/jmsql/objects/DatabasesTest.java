/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.jmsql.objects;

import com.jm.jmsql.xplora.Databases;
import javax.xml.bind.JAXBException;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;

/**
 *
 * @author Michael L.R. Marques
 */
public class DatabasesTest {

    @Test
    public void loadEmptyDatabasesTest() throws JAXBException {
        Databases databases = Databases.generate("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><databases/>");

        assertNotNull(databases);
    }

}
