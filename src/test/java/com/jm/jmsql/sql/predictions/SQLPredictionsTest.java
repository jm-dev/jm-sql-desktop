/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.jmsql.sql.predictions;

import com.jm.jmsql.jdbc.JDBCType;
import com.jm.jmsql.xplora.Catalog;
import com.jm.jmsql.xplora.Column;
import com.jm.jmsql.xplora.Definition;
import com.jm.jmsql.xplora.Item;
import com.jm.jmsql.xplora.Procedures;
import com.jm.jmsql.xplora.Table;
import com.jm.jmsql.xplora.Tables;
import com.jm.jmsql.xplora.Views;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author Michael L.R. Marques
 */
@RunWith(MockitoJUnitRunner.class)
public class SQLPredictionsTest extends SQLPredictions {

    private static final String DEFAULT_PATTERN = "[.]";
    private static final String TESTING_SQL = "select *,  from hello. where ";

    @Mock
    Definition mockDefinition;
    @Mock
    Catalog mockCatalog;

    @Test
    public void testGetRequiredTable() {
        Class<? extends Item> required = getRequired(TESTING_SQL, TESTING_SQL.lastIndexOf("."));

        assertEquals(required, Table.class);
    }

    @Test
    public void testGetRequiredColumn() {
        Class<? extends Item> required = getRequired(TESTING_SQL, TESTING_SQL.lastIndexOf("*, ") + 3);

        assertEquals(required, Column.class);
    }

    @Test
    public void testGetRequiredColumnWhere() {
        Class<? extends Item> required = getRequired(TESTING_SQL, TESTING_SQL.length() - 1);

        assertEquals(required, Column.class);
    }

    @Test
    public void testSimpleBuildPredictionsTables() {
        setupDefinition();
        
        List<Item> items = buildPredictionList("", DEFAULT_PATTERN, TESTING_SQL, TESTING_SQL.lastIndexOf("."), this.mockDefinition, this.mockCatalog);

        assertTrue(items != null);
        assertTrue(items.size() > 0);
        assertEquals(items.get(0).getClass(), Table.class);
    }

    @Test
    public void testSimpleBuildPredictionsTablesByName() {
        setupDefinition();

        List<Item> items = buildPredictionList("test", DEFAULT_PATTERN, TESTING_SQL, TESTING_SQL.lastIndexOf("."), this.mockDefinition, this.mockCatalog);

        assertTrue(items != null);
        assertTrue(items.size() > 0);
        assertEquals(items.get(0).getClass(), Table.class);
    }

    @Test
    public void testSimpleBuildPredictionsColumnsWhere() {
        setupDefinition();

        List<Item> items = buildPredictionList("", DEFAULT_PATTERN, TESTING_SQL, TESTING_SQL.length() - 1, this.mockDefinition, this.mockCatalog);

        assertTrue(items != null);
        assertTrue(items.size() > 0);
        assertEquals(items.get(0).getClass(), Column.class);
    }
    
    private void setupDefinition() {
        when(this.mockDefinition.isConnected()).thenReturn(true);
        when(this.mockDefinition.isLibrary()).thenReturn(false);
        when(this.mockDefinition.isEmpty()).thenReturn(false);
        when(this.mockDefinition.isLoading()).thenReturn(false);
        when(this.mockDefinition.isLoaded()).thenReturn(true);

        when(this.mockCatalog.isEmpty()).thenReturn(false);
        when(this.mockCatalog.isLoading()).thenReturn(false);
        when(this.mockCatalog.isLoaded()).thenReturn(true);

        this.mockDefinition.add(this.mockCatalog);
        this.mockCatalog.setParent(this.mockDefinition);

        this.mockCatalog.add(new Tables(this.mockCatalog));
        this.mockCatalog.add(new Procedures(this.mockCatalog));
        this.mockCatalog.add(new Views(this.mockCatalog));

        List<Table> tables = new ArrayList();
        Table table = new Table(this.mockCatalog.getTables(), "test_table", "");
        table.add(new Column(table, "test_column_1", JDBCType.VarChar, "VARCHAR", 25, 0, "", 1, false, false, false));
        table.add(new Column(table, "test_column_2", JDBCType.VarChar, "VARCHAR", 25, 0, "", 1, false, false, false));
        Table table2 = new Table(this.mockCatalog.getTables(), "test_table_2", "");
        table2.add(new Column(table, "test_column_1", JDBCType.VarChar, "VARCHAR", 25, 0, "", 1, false, false, false));

        tables.add(table);
        tables.add(table2);

        when(this.mockCatalog.subList(Table.class)).thenReturn(tables);
        when(this.mockCatalog.subList(Column.class)).thenReturn(tables.get(0));
    }
    
}
