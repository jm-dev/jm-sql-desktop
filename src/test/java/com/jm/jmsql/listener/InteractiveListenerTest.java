/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.jmsql.listener;

import static com.jm.jmsql.listener.InteractiveListener.DEFAULT_IP;
import static com.jm.jmsql.listener.InteractiveListener.DEFAULT_PORT;
import static com.jm.jmsql.listener.InteractiveListener.OK;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.BindException;
import java.net.Socket;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Michael L.R. Marques
 */
public class InteractiveListenerTest {
    
    private static final String FILE = "./readme.txt";

    final InteractiveListener listener;
    final Thread listenerThread;

    /**
     * 
     * @throws IOException
     */
    public InteractiveListenerTest() throws IOException {
        this.listener = new InteractiveListener();
        this.listenerThread = new Thread(this.listener);
    }

    /**
     *
     */
    @Before
    public void startListener() {
        this.listenerThread.setDaemon(true);
        this.listenerThread.start();
    }

    /**
     * 
     * @throws IOException
     */
//    @Test
    public void testAcceptSocket() throws IOException {
        TestFileOpenListener fileOpenListener = new TestFileOpenListener();
        this.listener.addFileOpenListener(fileOpenListener);

        try (Socket socket = new Socket(DEFAULT_IP, DEFAULT_PORT)) {
            try (PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
                    BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()))) {
                String line = reader.readLine();
                if (line  != null &&
                        line.equals(OK)) {
                    writer.println(FILE);
                }
            }
            assertEquals(new File(FILE), fileOpenListener.getFile());
        } catch (BindException be) {
            
        }
    }

}

/**
 *
 * @author Michael L.R. Marques
 */
class TestFileOpenListener implements FileOpenListener {

    private File file;

    /**
     *
     * @return
     */
    protected File getFile() {
        return this.file;
    }

    /**
     * 
     * @param event
     */
    @Override
    public void openFile(FileOpenEvent event) {
        this.file = event.getFile();
    }

}
